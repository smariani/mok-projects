package tabella;

/**
 * Desktop front end main.
 * 
 * @author Giulio Crestani
 * @author Gianluca Spadazzi
 *
 */
public final class Main {

    @SuppressWarnings("unused")
    public static void main(final String[] args) {

        new Frame();

    }

    private Main() {
    }
}
