package tabella;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Desktop;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.Statement;
import java.sql.SQLException;
import java.util.ArrayList;
import javax.swing.DefaultCellEditor;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.UIManager;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableCellRenderer;

/**
 * Initializes GUI and bundles the various listeners to buttons.
 * 
 * @author Giulio Crestani
 * @author Gianluca Spadazzi
 * @version 1.0
 *
 */

public final class Frame {

    private static String serverString = "jdbc:mysql://sql2.freemysqlhosting.net:3306/sql288779";
    private static String dbName = "sql288779";
    private static String dbPass = "yX4*wU2%";

    private static ArrayList<String> columnNames;
    private static ArrayList<ArrayList<String>> rows;
    private static int row;
    private static Object[][] rowData;
    private static Object[] colNames;

    private static JTable table;
    private static DefaultTableModel tableModel;
    private static JFrame frame;



    /**
     * Constructor that takes no parameters input.
     */
    protected Frame() {

        frame = new JFrame("CrawlerCommander -- Front-End Desktop");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        loadAll();
    }
    
    
    /**
     * Reload all the table.
     */
    public static void loadAll(){
    	 initialize();
         DefaultTableModel def = initializeTable();
         table = new JTable(def) {

             private static final long serialVersionUID = 1L;

             @Override
             public boolean isCellEditable(final int row1, final int col) {
                 switch (col) {
                     case 0:
                     case 1:
                     case 2:
                     case 3:
                     case 4:
                         return false;
                     default:
                         return true;
                 }
             }
         };
         table.setAutoResizeMode(JTable.AUTO_RESIZE_ALL_COLUMNS);

         table.getColumn("ACTION").setCellRenderer(new ButtonRenderer());
         table.getColumn("ACTION")
                 .setCellEditor(new ButtonEditor(new JCheckBox()));

         JPanel subPanel = new JPanel();
         JButton buttonUpdate = new JButton("Update Data");

         final int dim1 = 150;
         final int dim2 = 50;
         buttonUpdate.setPreferredSize(new Dimension(dim1, dim2));

         JButton buttonLink = new JButton("Go to Link");
         buttonLink.addActionListener(new ActionListener() {

             @Override
             public void actionPerformed(final ActionEvent e) {
                 int x = table.getSelectedRow();                 
                 if (x == -1) {
                     JOptionPane.showMessageDialog(frame,
                             "A row must be selected!");
                 } else {
                     openWebpage(rowData[x][1].toString());
                 }
                 reload();

             }
         });
         buttonLink.setPreferredSize(new Dimension(dim1, dim2));

         JButton buttonDeleteAll = new JButton("Delete All");
         buttonDeleteAll.setPreferredSize(new Dimension(dim1, dim2));
         buttonDeleteAll.setBackground(Color.red);
         buttonDeleteAll.setForeground(Color.blue);

         subPanel.add(buttonLink);
         subPanel.add(buttonUpdate);
         subPanel.add(buttonDeleteAll);

         JScrollPane scrollPane = new JScrollPane(table);

         buttonDeleteAll.addActionListener(new ActionListener() {

             @Override
             public void actionPerformed(final ActionEvent e) {
                 int selectedOption = JOptionPane.showConfirmDialog(null,
                         "Are you sure??", "Choose", JOptionPane.YES_NO_OPTION);
                 if (selectedOption == JOptionPane.YES_OPTION) {
                     executeDeleteAll(scrollPane);
                 }
                 reload();
             }
         });

         frame.add(scrollPane, BorderLayout.CENTER);
         frame.add(subPanel, BorderLayout.SOUTH);
         final int d1 = 1000;
         final int d2 = 500;
         frame.setSize(d1, d2);
         frame.setVisible(true);

         buttonUpdate.addActionListener(new ActionListener() {

             @Override
             public void actionPerformed(final ActionEvent e) {
                reload();
             }
         });
    	
    }
    
    public static void reload(){
    	initialize();
    	 DefaultTableModel def = initializeTable();
    	 table.setModel(def);
    	 table.getColumn("ACTION").setCellRenderer(new ButtonRenderer());
         table.getColumn("ACTION")
                 .setCellEditor(new ButtonEditor(new JCheckBox()));
    }

    /**
     * Metodo che inizializza tutte le righe della tabella. Si connette al
     * database online e ne estrapola tutti i dati compresi i nomi delle
     * colonne.
     */
    private static void initialize() {
        Connection conn = null;
        Statement stmt = null;
        @SuppressWarnings("resource")
        ResultSet rs = null;

        try {
            Class.forName("com.mysql.jdbc.Driver");
            conn = DriverManager.getConnection(serverString, dbName, dbPass);
            // prepare query
            String query = "SELECT * from Search";
            // create a statement
            stmt = conn.createStatement();
            // execute query and return result as a ResultSet
            rs = stmt.executeQuery(query);
            // get the column names from the ResultSet
            columnNames = getColumnNames(rs);

            rows = getRows(rs);

        } catch (Exception e) {
            e.printStackTrace();
            System.exit(1);
        } finally {
            // release database resources
            try {
                rs.close();
                stmt.close();
                conn.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * Metodo che inizializza tutte e colonne e le righe con dati scaricati dal
     * database online.
     * 
     * @return Modello per la JTable.
     */
    private static DefaultTableModel initializeTable() {
        rowData = new Object[rows.size()][columnNames.size() + 1];
        colNames = new Object[columnNames.size() + 1];

        for (int i = 0; i < columnNames.size(); i++) {
            colNames[i] = columnNames.get(i);
        }
        colNames[columnNames.size()] = "ACTION";

        ArrayList<String> temp = new ArrayList<String>();
        for (int i = 0; i < rows.size(); i++) {
            temp = rows.get(i);
            for (int j = 0; j < columnNames.size(); j++) {
                rowData[i][j] = temp.get(j);
            }
            temp.clear();
        }

        tableModel = new DefaultTableModel(rowData, colNames);

        return tableModel;
    }

    /**
     * Questo metodo seleziona il link della riga specificata in modo da
     * passarlo al metodo executeDelete che provveder� a eliminarlo dal database
     * online.
     * 
     * @param r
     *            numero di riga
     */
    public static void setRowColumn(final int r) {
        row = r;
        // extract link to delete
        String link = rowData[row][1].toString();
        executeDelete(row, link);
    }

    /**
     * Questo metodo si connette al db online ed esegue una query di
     * eliminazione. Successivamente invoca un metodo per aggiornare i risultati
     * visibili nella tabella.
     * 
     * @param r
     *            numero di riga
     * @param s
     *            link specificato nella tabella
     */
    private static void executeDelete(final int r, final String s) {
        Connection conn = null;
        @SuppressWarnings("resource")
        Statement stmt = null;

        try {
            Class.forName("com.mysql.jdbc.Driver");
            conn = DriverManager.getConnection(serverString, dbName, dbPass);
            // eliminazione riga dal database
            String query = "DELETE FROM Search WHERE LINK = \"" + s + "\"";
            stmt = conn.createStatement();
            stmt.executeUpdate(query);
            // p(query);
            // eliminazione riga dal modello (cosi si aggiorna)
            tableModel.removeRow(r);
        } catch (Exception e) {
            e.printStackTrace();
            System.exit(1);
        } finally {
            try {
                stmt.close();
                conn.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }

    }

    /**
     * Metodo che ritorna un arrayList contenente il nome di tutti i campi del
     * database. Questi nomi sono estratti da una query fatta nela metodo
     * privato <em>initialize</em>.
     * 
     * @param rs
     *            ResultSet contenente tutti i metadati necessari al metodo.
     * @return ArrayList contenente il nome di tutte le colonne della tabella
     * @throws SQLException
     *             Eccezione SQL.
     */
    public static ArrayList<String> getColumnNames(final ResultSet rs)
            throws SQLException {
        ArrayList<String> col = new ArrayList<String>();
        if (rs == null) {
            return null;
        }
        ResultSetMetaData rsMetaData = rs.getMetaData();
        int numberOfColumns = rsMetaData.getColumnCount();

        // get the column names; column indexes start from 1
        for (int i = 1; i < numberOfColumns + 1; i++) {
            String columnName = rsMetaData.getColumnName(i);
            // System.out.println(columnName);
            col.add(columnName);
        }

        return col;
    }

    /**
     * Permette di avere tutte le righe del database.
     * 
     * @param rs
     *            ResultSet derivato dalla query.
     * @return ArrayList contenente tutti i dati delle righe del database.
     * @throws SQLException
     *             Eccezione SQL.
     */
    public static ArrayList<ArrayList<String>> getRows(final ResultSet rs)
            throws SQLException {
        ArrayList<ArrayList<String>> rows1 = new ArrayList<ArrayList<String>>();
        ArrayList<String> temp = new ArrayList<String>();

        while (rs.next()) {

            // System.out.println(rs.getString(1) + ", " + rs.getString(2) + ",
            // "
            // + rs.getString(3) + ", "
            // + rs.getString(4) + ", "
            // + rs.getString(5));

            for (int i = 0; i < columnNames.size(); i++) {
                temp.add(rs.getString(i + 1));
            }
            rows1.add(temp);
            temp = new ArrayList<String>();
        }
        return rows1;

    }

    /**
     * Semplice metodo per evitare di richiamare sempre System.out.println(...);
     * 
     * @param s
     *            Stringa da stampare a video
     */
    public static void p(final String s) {
        System.out.println(s);
    }

    /**
     * Apre nel browser la pagina web specificata.
     * 
     * @param uri
     *            URL da aprire nel browser predefinito nel sistema.
     */
    public static void openWebpage(final URI uri) {
        Desktop desktop = Desktop.isDesktopSupported() ? Desktop.getDesktop()
                : null;
        if (desktop != null && desktop.isSupported(Desktop.Action.BROWSE)) {
            try {
                desktop.browse(uri);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * Traduce la stringa in input in un oggetto URL.
     * 
     * @param u
     *            Stringa che verr� tradotta in URL per richimare il metodo
     *            <em>openWebpage(URI uri)</em>
     */
    public static void openWebpage(final String u) {
        URL url = null;
        try {
            url = new URL(u);
        } catch (MalformedURLException e1) {
            e1.printStackTrace();
        }

        try {
            openWebpage(url.toURI());
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }
    }

    /**
     * Esegue una query sul database che elimina tutte le tuple al suo interno.
     * 
     * @param scrollPane
     *            {@link JScrollPane} del Frame. Utilizzato per refreshare la
     *            tabella.
     */
    private static void executeDeleteAll(final JScrollPane scrollPane) {
        Connection conn = null;
        @SuppressWarnings("resource")
        Statement stmt = null;

        try {
            Class.forName("com.mysql.jdbc.Driver");
            conn = DriverManager.getConnection(serverString, dbName, dbPass);
            // eliminazione di tute le tuple dal database
            String query = "DELETE FROM Search";
            stmt = conn.createStatement();
            stmt.executeUpdate(query);

            // eliminazione riga dal modello (cosi si aggiorna)

        } catch (Exception e) {
            e.printStackTrace();
            System.exit(1);
        } finally {
            try {
                stmt.close();
                conn.close();

                reload();

            } catch (SQLException e) {
                e.printStackTrace();
            }
        }

    }

}

class ButtonRenderer extends JButton implements TableCellRenderer {

    private static final long serialVersionUID = 1L;

    public ButtonRenderer() {
        setOpaque(true);
    }

    @Override
    public Component getTableCellRendererComponent(final JTable table,
            final Object value, final boolean isSelected,
            final boolean hasFocus, final int row, final int column) {
        if (isSelected) {
            setForeground(table.getSelectionForeground());
            setBackground(table.getSelectionBackground());
        } else {
            setForeground(table.getForeground());
            setBackground(UIManager.getColor("Button.background"));
        }
        setText("Delete");
        return this;
    }
}

class ButtonEditor extends DefaultCellEditor {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    protected JButton button;

    private int row;

    public ButtonEditor(final JCheckBox checkBox) {
        super(checkBox);
        this.button = new JButton();
        this.button.setOpaque(true);
        this.button.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(final ActionEvent e) {
                 fireEditingStopped();
                Frame.setRowColumn(ButtonEditor.this.row);
            }

        });
    }

    @Override
    public Component getTableCellEditorComponent(final JTable table,
            final Object value, final boolean isSelected, final int row1,
            final int column) {
        if (isSelected) {
            this.button.setForeground(table.getSelectionForeground());
            this.button.setBackground(table.getSelectionBackground());

        } else {
            this.button.setForeground(table.getForeground());
            this.button.setBackground(table.getBackground());
        }
        this.button.setText("Delete");
        this.row = row1;
        return this.button;
    }

    @Override
    public boolean stopCellEditing() {
        return super.stopCellEditing();
    }

    @Override
    protected void fireEditingStopped() {
        super.fireEditingStopped();
    }

}
