package centralunit;

import java.util.List;

import interfaces.IRegistration;

/**
 * Saves IP address of the crawler that connect to Central Unit.
 * 
 * @author Giulio Crestani
 * @author Gianluca Spadazzi
 * @version 1.0
 *
 */

// riceve le registrazioni dei crawler
public class Registration implements IRegistration {

	private List<Object> c;

	/**
	 * Constructor that initialize internal list.
	 * 
	 * @param c1
	 *            Internal list that contains connected crawlers IP.
	 */
	public Registration(final List<Object> c1) {
		this.c = c1;
	}

	@Override
	public boolean register(final String ip) {
		System.out.println(ip + " aggiunto alla lista");
		return this.c.add(ip);
	}

	public void removeCrawler(final int id) {
		this.c.remove(this.c.get(id));
	}

	public void getNumElems() {
		System.out.println("dim: " + this.c.size());
	}
}
