package centralunit;

import interfaces.ISender;
import interfaces.IWebPage;
import interfaces.centralunit.IReceiver;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JButton;

/**
 * It receives and filters web pages. In addition to this, it sends the message
 * STOP to crawler when the search ends.
 * 
 * @author Giulio Crestani
 * @author Gianluca Spadazzi
 * @version 1.0
 *
 */

public class Receiver implements IReceiver {

	private List<Object> pag, connections;
	private long numberOfResults;
	private String keyword;
	private boolean searchSyn;
	private int index; // per dire a google da che indice cominciare a cercare
	private ISender send;
	private JButton btnStart; // mi serve perche' quando finisco la ricerca in
								// modo naturale (senza premere stop) devo
								// renderlo cliccabile

	// per i sinonimi
	private int countNormal;
	private int countSyn;
	private long maxNormal;
	private long maxSyn;

	private IWebPage currentPage;

	/**
	 * Constructor that initialize fields.
	 * 
	 * @param pag1
	 *            List of pages.
	 * @param send1
	 *            Sender that send pages to online database.
	 */
	public Receiver(final List<Object> pag1, final ISender send1) {
		this.pag = pag1;
		this.numberOfResults = 0;
		this.index = 0;
		this.send = send1;
		this.connections = null;
		this.countNormal = 0;
		this.countSyn = 0;

		this.maxNormal = 0;
		this.maxSyn = 0;
	}

	public synchronized void addWebPage(final IWebPage wp) {
		// se cerco con o senza sinonimi
		this.currentPage = wp;
		if (!this.searchSyn) {
			try {
				addWebPageNormal(wp);
			} catch (ClassNotFoundException | SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		} else {
			try {
				addWebPageSynonyms(wp);
			} catch (ClassNotFoundException | SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	/**
	 * Adds the page to the list in case the research is without synonyms.
	 * 
	 * @param wp
	 *            Web page.
	 * @throws ClassNotFoundException
	 * @throws SQLException
	 */
	// classica aggiunta di pagina normale come abbiamo sempre fatto
	private void addWebPageNormal(final IWebPage wp) throws ClassNotFoundException, SQLException {
		// controllo che non sia nella lista della ricerca corrente
		if (!isContained(wp) && SysKb.isRunning()) {
			// controllo anche che non sia gia' nel database
			if (!read(wp.getLink())) {
				this.pag.add(wp);
				this.index++;
				if (this.index == this.numberOfResults) {
					SysKb.setRunning(false);
					System.out.println("FERMI TUTTI: cont= " + this.index);
					// fermi tutti i crawler
					for (int i = 0; i < ButtonListener.getFinalList().size(); i++) {
						((ConnectionHandler) this.connections.get(i)).writeSocket("STOP");
					}

					// disabilito il bottone stop una volta terminata la ricerca
					ButtonListener.setButtonStop();

					// // invio al server
					this.send.setList(this.pag);
					this.send.insert();

					for (int i = 0; i < ButtonListener.getFinalList().size(); i++) {
						this.pag = new ArrayList<Object>();
					}
					this.numberOfResults = 0;
					this.index = 0;
					this.maxNormal = 0;
					this.maxSyn = 0;
					this.btnStart.setEnabled(true);
				}
			}
		}
	}

	/**
	 * Adds the page to the list in case the research is with synonyms.
	 * 
	 * @param wp
	 *            Web Page.
	 * @throws ClassNotFoundException
	 * @throws SQLException
	 */
	// continuo a aggiungere fino a che non ho il 50% di risultati della keyword
	// e il 50% di sinonimi
	private void addWebPageSynonyms(final IWebPage wp) throws ClassNotFoundException, SQLException {
		// controllo che non sia nella lista della ricerca corrente
		if (!isContained(wp) && SysKb.isRunning()) {
			// se la keyword e' la stessa tengo il conto nella rispettiva
			// variabile
			if (!read(wp.getLink()) && wp.getKeyword().equals(this.keyword)) { // no
																				// sinonimo
				this.countNormal++;
				if (this.countNormal <= this.maxNormal) {
					// controllo di non eccedere il contatore
					this.pag.add(wp);
					this.index++;
				}
			} else if (!read(wp.getLink()) && !wp.getKeyword().equals(this.keyword)) { // si'
																						// sinonimo
				this.countSyn++;
				if (this.countSyn <= this.maxSyn) {
					// controllo di non eccedere il contatore
					this.pag.add(wp);
					this.index++;
				}

			}
			System.out.println("index= " + this.index);
			if (this.index == this.numberOfResults) {
				System.out.println("FERMI TUTTI: cont= " + this.index);
				// fermi tutti i crawler
				for (int i = 0; i < ButtonListener.getFinalList().size(); i++) {
					((ConnectionHandler) this.connections.get(i)).writeSocket("STOP");
				}

				// // invio al server
				this.send.setList(this.pag);
				this.send.insert();

				for (int i = 0; i < ButtonListener.getFinalList().size(); i++) {
					this.pag = new ArrayList<Object>();
				}
				this.numberOfResults = 0;
				this.index = 0;
				this.btnStart.setEnabled(true);
				this.maxNormal = 0;
				this.maxSyn = 0;
			}
		}
	}

	/**
	 * Check that the page is not contained in the list of current research.
	 * 
	 * @param page
	 *            Web page.
	 * @return Boolean that indicates if the page is or isn't contained in the
	 *         current list.
	 */
	private boolean isContained(final IWebPage page) {
		for (int i = 0; i < this.pag.size(); i++) {
			if (((IWebPage) this.pag.get(i)).getLink().equals(page.getLink())) {
				return true;
			}
		}
		return false;
	}

	public synchronized void setNumberOfResults(final long numberOfResults1) {
		this.numberOfResults = numberOfResults1;
		
		long y = this.numberOfResults;
		float x = y;
		float per = SysKb.getPercentageSyn(); // Prendo percentuale di sinonimi
		this.maxSyn = (long) (x * (per / 100)); // Numero di risultati sinonimi
		this.maxNormal = y - this.maxSyn; // Numero di risultati normali
	}

	public synchronized void setKeyword(final String keyword1) {
		this.keyword = keyword1;
	}

	public synchronized void setSynonyms(final boolean b) {
		this.searchSyn = b;
	}

	/**
	 * Reads from the database online to check that a page is not contained in
	 * it. If it is included it is updated with the new data.
	 * 
	 * @param s
	 *            Page link.
	 * @return Boolean that indicates if the page is included it is updated with
	 *         the new data.
	 * @throws ClassNotFoundException
	 * @throws SQLException
	 */
	private boolean read(final String s) throws ClassNotFoundException, SQLException {
		boolean res;
		Class.forName("com.mysql.jdbc.Driver");
		@SuppressWarnings("resource")
		Connection conn = DriverManager.getConnection(SysKb.serverString(), SysKb.dbName(), SysKb.dbPass());
		Statement stmt = conn.createStatement();
		String query = "SELECT * FROM Search WHERE LINK= \"" + s + "\"";
		ResultSet rs = stmt.executeQuery(query);
		// nessun dato
		if (!rs.isBeforeFirst()) {
			res = false;
		} else {
			System.out.println("Aggiorno tupla!");
			res = true;
			// aggiorno la tupla nel database
			String update = null;
			if (this.currentPage.getType().equals("crawler4j")) {
				update = "UPDATE Search SET KEYWORD = \"" + this.currentPage.getKeyword() + "\",LINK = \""
						+ this.currentPage.getLink() + "\",TITLE = \"" + this.currentPage.getTitle() + "\",SNIPPET = \""
						+ this.currentPage.getSnippet() + "\",LENGTH = \"" + this.currentPage.getLength()
						+ "\" WHERE LINK = \"" + this.currentPage.getLink() + "\"";
			} else {
				update = "UPDATE Search SET KEYWORD = \"" + this.currentPage.getKeyword() + "\",LINK = \""
						+ this.currentPage.getLink() + "\",TITLE = \"" + this.currentPage.getTitle() + "\",SNIPPET = \""
						+ this.currentPage.getSnippet() + "\" WHERE LINK = \"" + this.currentPage.getLink() + "\"";
			}
			stmt.executeUpdate(update);
		}
		conn.close();
		return res;
	}

	public void setStart() {
		SysKb.setRunning(true);
	}

	// se ho stoppato col bottone, invio al server quello che ho
	public void setStopByButton() {
		SysKb.setRunning(false);
		this.send.setList(this.pag);
		this.send.insert();
		for (int i = 0; i < ButtonListener.getFinalList().size(); i++) {
			this.pag = new ArrayList<Object>();
		}
		this.numberOfResults = 0;
		this.index = 0;
		this.maxNormal = 0;
		this.maxSyn = 0;
	}

	public void setButtonStart(final JButton btnStart1) {
		this.btnStart = btnStart1;
	}

	public void setConnections(final List<Object> c) {
		this.connections = c;
	}

}
