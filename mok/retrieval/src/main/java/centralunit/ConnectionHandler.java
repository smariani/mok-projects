package centralunit;

import interfaces.IRegistration;
import interfaces.IWebPage;
import interfaces.centralunit.IReceiver;

import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;

import common.WebPage;

/**
 * Connection handler with the crawler.
 * 
 * @author Giulio Crestani
 * @author Gianluca Spadazzi
 * @version 1.0
 *
 */

public class ConnectionHandler extends Thread {

	private Socket socket = null;
	private ObjectInputStream inStream;
	private ObjectOutputStream outStream;

	private IRegistration reg;
	private IReceiver rec;

	/**
	 * Constructor that initialize fields.
	 * 
	 * @param s
	 *            Socket relative to connection with crawler.
	 * @param reg1
	 *            Registration that take care of adding to the crawler list the
	 *            crawlers that they are going to connect.
	 * @param rec1
	 *            Receiver that takes care of receiving and filtering of
	 *            received pages.
	 */
	public ConnectionHandler(final Socket s, final IRegistration reg1, final IReceiver rec1) {
		this.reg = reg1;
		this.rec = rec1;
		this.socket = s;
		try {
			this.outStream = new ObjectOutputStream(this.socket.getOutputStream());
		} catch (IOException e) {
			e.printStackTrace();
		}
		try {
			this.inStream = new ObjectInputStream(this.socket.getInputStream());
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * It receives data of the task by crawlers and web pages which then
	 * will send to Receiver for their processing.
	 */
	@Override
	public void run() {
		while (true) {
			Object read = null;
			while (read == null) {
				try {
					read = this.inStream.readObject();
				} catch (ClassNotFoundException e) {
					e.printStackTrace();
				} catch (IOException e) {
					continue; // lasciato vuoto apposta.
				}
			}
			if (read instanceof String) {
				System.out.println(read);
				// se ip
				switch (((String) read).split(" ")[0]) {
				case ("IP"):
					String ip = ((String) read).split(" ")[1];
					System.out.println("Object received = " + ip);
					this.reg.register(ip);
					break;
				default:
					break;
				}
				if (SysKb.isRunning()) {
					try {
						this.outStream.writeObject(
								"TASK " + SysKb.getKeyword() + " " + SysKb.getNumber() + " " + SysKb.isSyn());
						this.outStream.writeObject("START");
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
			} else if (read instanceof IWebPage) {
				System.out.println("CU VERA: RICEVO PAGINA");
				IWebPage p = new WebPage(((IWebPage) read).getKeyword(),
						((IWebPage) read).getTitle().replaceAll("\"", ""), ((IWebPage) read).getLink(),
						((IWebPage) read).getSnippet().replaceAll("\"", ""), ((IWebPage) read).getLength(),
						((IWebPage) read).getType());
				// aggiungo la pagina
				this.rec.addWebPage(p);
			}
		}
	}

	/**
	 * Write an object on ObjectOutputStream.
	 * 
	 * @param o
	 *            Object to write.
	 */
	public void writeSocket(final Object o) {
		try {
			this.outStream.writeObject(o);
		} catch (IOException e) {
			System.out.println(); // lasciato vuoto apposta.
		}
	}

	/**
	 * Getter of the socket's ObjectOutputStream.
	 * 
	 * @return Socket's ObjectOutputStream.
	 */
	public ObjectOutputStream getOutputStream() {
		return this.outStream;
	}

	/**
	 * Getter of the socket's ObjectInputStream.
	 * 
	 * @return Socket's ObjectInputStream.
	 */
	public InputStream getInputStream() {
		return this.inStream;
	}

}
