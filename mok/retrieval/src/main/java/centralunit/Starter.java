package centralunit;

import java.util.List;

import interfaces.IStarter;

/**
 * Si occupa di inviare il messaggio �START� ai crawler per comunicargli che
 * devono iniziare le loro operazioni di ricerca.
 * 
 * @author Giulio Crestani
 * @author Gianluca Spadazzi
 * @version 1.0
 *
 */

public class Starter implements IStarter {

	private List<Object> c;
	private ConnectionHandler con;

	/**
	 * Constructor that initializes the internal list of Connection Handler
	 * related to crawlers connected.
	 * 
	 * @param c1
	 *            List that contains Connection Handlers related to crawlers
	 *            connected.
	 */
	public Starter(final List<Object> c1) {
		this.c = c1;
		this.con = null;
	}

	public void launchAll() {
		for (int i = 0; i < this.c.size(); i++) {
			this.con = (ConnectionHandler) this.c.get(i);
			this.con.writeSocket("START");
		}

	}

}
