package centralunit;

import java.util.ArrayList;
import java.util.List;

import common.Sender;

import gui.GUI;
import interfaces.IRegistration;
import interfaces.ISender;
import interfaces.centralunit.IReceiver;

/**
 * Contains reserve Central Unit main. Initialize lists and components.
 * 
 * @author Giulio Crestani
 * @author Gianluca Spadazzi
 * @version 1.0
 *
 */

public final class CrawlerCommander {

	/**
	 * Initialize the lists of web pages, Registration, Sender, Receiver, it starts thread
	 * relative of server and creates GUI.
	 * 
	 * @param args
	 *            Main parameters.
	 */
    @SuppressWarnings("unused")
    public static void main(final String[] args) {

    	List<Object> l1 = new ArrayList<Object>(); // crawler (i loro ip)
    	List<Object> l2 = new ArrayList<Object>(); // web page

        IRegistration reg = new Registration(l1);
        ISender sender = new Sender(l2);
        IReceiver rec = new Receiver(l2, sender);

        ServerThread s = new ServerThread(rec, reg);
        s.start();

        new GUI(l1, rec);

    }

    private CrawlerCommander() {
    }

}
