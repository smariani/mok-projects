package centralunit;

import interfaces.IRegistration;
import interfaces.centralunit.IReceiver;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;

/**
 * It manages socket of the Central Unit and creates a Connection Handler for
 * each connected crawler.
 * 
 * @author Giulio Crestani
 * @author Gianluca Spadazzi
 * @version 1.0
 *
 */

public class ServerThread extends Thread {

	private ServerSocket serverSocket;
	private Socket socket;

	/**
	 * List of client socket.
	 * 
	 */
	private static List<ConnectionHandler> list;

	/**
	 * Getter of the Connection Handler list.
	 * 
	 * @return List of Connection Handler.
	 */
	public static List<ConnectionHandler> getList() {
		return list;
	}

	/**
	 * Setter of the Connection Handler list.
	 * 
	 * @param list1
	 *            List of Connection Handler.
	 */
	public static void setList(final List<ConnectionHandler> list1) {
		ServerThread.list = list1;
	}

	private IRegistration reg;
	private IReceiver rec;

	/**
	 * Constructor that initialize internal field.
	 * 
	 * @param rec1
	 *            Receiver is the entity that is responsible for receiving and
	 *            filtering web pages.
	 * @param reg1
	 *            Entity that is responsible for saving ip about connected
	 *            crawler.
	 */
	public ServerThread(final IReceiver rec1, final IRegistration reg1) {
		this.rec = rec1;
		this.reg = reg1;
		this.serverSocket = null;
		list = new ArrayList<ConnectionHandler>();
		this.socket = null;
	}

	/**
	 * Creates the socket on the server and a Connection Handler for each
	 * crawler that connects.
	 */
	@Override
	public void run() {
		try {
			// creo il server per accettare le connessioni dai crawler
			this.serverSocket = new ServerSocket(SysKb.getPortServer(), SysKb.getMaxConn());
			while (true) {
				this.socket = this.serverSocket.accept();
				// faccio la write di TEST di prova per fare in modo di
				// eliminare eventuali crawler disconnessi
				for (int i = 0; i < ServerThread.list.size(); i++) {
					ObjectOutputStream outStream = ServerThread.list.get(i).getOutputStream();
					try {
						outStream.writeObject("TEST");
					} catch (IOException e1) {
						ServerThread.list.remove(i);
						i--;
					}

				}
				// controllo la dimensione per rispettare il massimo numero di
				// connessioni
				if (ServerThread.list.size() <= SysKb.getMaxConn() - 1) {
					// accetto la connessione
					ConnectionHandler conn = new ConnectionHandler(this.socket, this.reg, this.rec);
					conn.start();
					addToList(conn);
				} else {
					// rifiuto la connessione
					this.socket.close();
				}
			}
		} catch (Exception e) {
			System.out.println("exc 1");
			e.printStackTrace();
		}
	}

	/**
	 * Add Connection Handler to internal list.
	 * 
	 * @param c
	 *            Connection Handler to add.
	 */
	public static void addToList(final ConnectionHandler c) {
		ServerThread.list.add(c);
	}

}
