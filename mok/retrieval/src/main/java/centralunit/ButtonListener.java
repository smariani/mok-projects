package centralunit;

import interfaces.centralunit.IReceiver;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

/**
 * Implements listener used for Start and Stop buttons of the GUI.
 * 
 * @author Giulio Crestani
 * @author Gianluca Spadazzi
 * @version 1.0
 *
 */

public class ButtonListener implements ActionListener {

    private List<Object> c; // degli ip
    private JTextField searchName;
    private JTextField numberResult;
    private JFrame frame;
    private JCheckBox myCheckBox;
    private IReceiver rec;
    private static List<ConnectionHandler> finalList;

    public static List<ConnectionHandler> getFinalList() {
        return finalList;
    }

    public static void setFinalList(final List<ConnectionHandler> finalList1) {
        ButtonListener.finalList = finalList1;
    }

    private JButton btnStart;
    private static JButton btnStop;

    /**
     * Constructor of the Start and Stop buttons. It initialize the fields.
     * 
     * @param c1
     *            List of IP crawlers.
     * @param searchName1
     *            TextField for the keyword.
     * @param numberResult1
     *            TextField for results number.
     * @param frame1
     *            GUI interface.
     * @param myCheckBox1
     *            Checkbox to search with or without synonyms.
     * @param rec1
     *            Receiver.
     * @param btnStart1
     *            Start button.
     * @param btnstop1
     *            Stop button.
     */

    public ButtonListener(final List<Object> c1, final JTextField searchName1,
            final JTextField numberResult1, final JFrame frame1,
            final JCheckBox myCheckBox1, final IReceiver rec1,
            final JButton btnStart1, final JButton btnstop1) {
        this.c = c1;
        this.searchName = searchName1;
        this.numberResult = numberResult1;
        this.frame = frame1;
        this.myCheckBox = myCheckBox1;
        this.rec = rec1;
        this.btnStart = btnStart1;
        btnStop = btnstop1;
    }

    @Override
    public void actionPerformed(final ActionEvent e) {
        // premo start
        if (e.getSource() == this.btnStart) {
            if (this.searchName.getText().equals("")
                    || this.numberResult.getText().equals("")) {
                JOptionPane.showMessageDialog(this.frame,
                        "Inserire un valore in tutti i campi.", "Warning",
                        JOptionPane.WARNING_MESSAGE);
            } else {
                String s = null;
                Integer n = null;
                try {
                    s = this.searchName.getText();
                    n = new Integer(this.numberResult.getText());
                } catch (Exception ex) {
                    JOptionPane.showMessageDialog(this.frame,
                            "Numero risultati deve essere un numero.",
                            "Warning", JOptionPane.WARNING_MESSAGE);
                }

                SysKb.setKeyword(s);
                SysKb.setNumber(n);
                SysKb.setSyn(this.myCheckBox.isSelected());

                this.rec.setNumberOfResults(n);
                this.rec.setKeyword(s);
                this.rec.setSynonyms(this.myCheckBox.isSelected());
                this.rec.setButtonStart(this.btnStart);

                // controllo che ci siano crawler connessi
                // controllo il loro stato prima di procedere. se qualcuno si �
                // disconnesso lo devo sapere e toglierlo dalla lista
                for (int i = 0; i < ServerThread.getList().size(); i++) {
                    ObjectOutputStream outStream = ServerThread.getList().get(i)
                            .getOutputStream();
                    try {
                        outStream.writeObject("TEST");
                    } catch (IOException e1) {
                        ServerThread.getList().remove(i);
                        i--;
                    }

                }

                if (ServerThread.getList().isEmpty()) {
                    JOptionPane.showMessageDialog(this.frame,
                            "Nessun crawler connesso, impossibile iniziare la ricerca.",
                            "Warning", JOptionPane.WARNING_MESSAGE);
                } else {
                    if (this.myCheckBox.isSelected()) {
                        // RICERCA CON SINONIMI
                        this.rec.setStart();
                        finalList = ServerThread.getList();
                        for (int i = 0; i < finalList.size(); i++) {
                            finalList.get(i).writeSocket(
                                    "TASK " + s + " " + n + " true");
                        }
                    } else {
                        // RICERCA SENZA SINONIMI
                        this.rec.setStart();
                        finalList = ServerThread.getList();
                        String str = "TASK " + s + " " + n + " false";
                        System.out.println(str);
                        for (int i = 0; i < finalList.size(); i++) {
                            finalList.get(i).writeSocket(str);
                        }
                    }
                    this.c = new ArrayList<Object>();
                    for (int i = 0; i < finalList.size(); i++) {
                        this.c.add(finalList.get(i));
                    }
                    Starter starter = new Starter(this.c);
                    starter.launchAll();
                    this.rec.setConnections(this.c);

                    // setto che la ricerca � gi� partita, nel caso si connetta
                    // qualcuno mentre gi� sic erca
                    SysKb.setRunning(true);

                    btnStop.setEnabled(true);
                    this.btnStart.setEnabled(false);

                }
            }
        }
        if (e.getSource() == btnStop) {
            for (int i = 0; i < finalList.size(); i++) {
                finalList.get(i).writeSocket("STOP");
                this.rec.setStopByButton();
                btnStop.setEnabled(false);
                this.btnStart.setEnabled(true);
            }
        }

    }

    /**
     * It makes the stop button unclickable.
     */
    public static void setButtonStop() {
        btnStop.setEnabled(false);
    }
}
