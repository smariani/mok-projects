package interfaces.google;

import interfaces.ISearch;
import interfaces.IWebPage;

import java.util.List;

/**
 * @author Giulio Crestani
 * @author Gianluca Spadazzi
 * @version 1.0
 *
 */

public interface ISearchGoogle extends ISearch {


    /**
     * Starts to examine the web pages considering the synonyms.
     * 
     * @param index
     *            Start index.
     */
    void startCrawlingWithSynonyms(long index);

    /**
     * Getter of the web pages' lists.
     * 
     * @return List of web pages
     */
    List<IWebPage> getList();

    /**
     * Setter of the keyword.
     * 
     * @param keyword
     *            Keyword's name.
     */
    void setKeyword(String keyword);

}
