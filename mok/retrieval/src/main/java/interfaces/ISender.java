package interfaces;

import java.util.List;

/**
 * @author Giulio Crestani
 * @author Gianluca Spadazzi
 * @version 1.0
 *
 */

public interface ISender {

    /**
     * Insert data into database.
     */
    void insert();

    /**
     * Set the list of WebPage which will be sent to database.
     * 
     * @param list
     *            List to be set.
     */
    void setList(List<Object> list);

}
