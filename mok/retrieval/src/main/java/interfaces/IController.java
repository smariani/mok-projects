package interfaces;

/**
 * @author Giulio Crestani
 * @author Gianluca Spadazzi
 * @version 1.0
 *
 */

public interface IController {

    /**
     * Starts the research without synonyms.
     * 
     * @throws Exception
     *             Exception.
     */
    void launch() throws Exception;

    /**
     * Starts the research with synonyms.
     * 
     * @throws Exception
     *             Exception.
     */
    void startWithSynonyms() throws Exception;

}
