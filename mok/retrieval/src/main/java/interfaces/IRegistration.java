package interfaces;

/**
 * @author Giulio Crestani
 * @author Gianluca Spadazzi
 * @version 1.0
 *
 */

public interface IRegistration {

    /**
     * Add crawler to list.
     * 
     * @param ip
     *            IP address of the crawler.
     * @return Result of the operation.
     */
    boolean register(String ip);

    /**
     * Delete crawler from list.
     * 
     * @param id
     *            Identifier of the crawler's position.
     */
    void removeCrawler(int id);

    /**
     * Print the number of crawler in the list.
     */
    void getNumElems();
}
