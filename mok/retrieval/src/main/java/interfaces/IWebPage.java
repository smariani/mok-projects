package interfaces;

/**
 * @author Giulio Crestani
 * @author Gianluca Spadazzi
 * @version 1.0
 *
 */

public interface IWebPage {

    /**
     * 
     * Set the page's title.
     * 
     * @param s
     *            String that identifies the page's title.
     */
    void setTitle(String s);

    /**
     * Set the page's link.
     * 
     * @param s
     *            String that identifies the link.
     */
    void setLink(String s);

    /**
     * Set the 'keyword' field of the page
     * 
     * @param s
     *            String that identifies the keyword.
     */
    void setKeyword(String s);

    /**
     * Set the page's text.
     * 
     * @param s
     *            String that identifies the page's text.
     */
    void setText(String s);

    /**
     * Set the page's length.
     * 
     * @param s
     *            String that identifies the page's length on integer form.
     */
    void setLength(String s);

    /**
     * Getter of the title.
     * 
     * @return Page's title.
     */
    String getTitle();

    /**
     * Getter of the link.
     * 
     * @return Page's link.
     */
    String getLink();

    /**
     * Getter of the keyword.
     * 
     * @return Page's keyword.
     */
    String getKeyword();

    /**
     * Getter della lunghezza.
     * 
     * @return Lunghezza della pagina.
     */
    String getLength();

    /**
     * Getter of the text.
     * 
     * @return Page's text.
     */
    String getText();

    /**
     * Getter of the snippet.
     * 
     * @return Return the page's snippet.
     */
    String getSnippet();

    /**
     * Set the crawler's type which have found the WebPage.
     * 
     * @param s
     *            Crawler's type.
     */
    void setType(String s);

    /**
     * Getter of the crawler's type.
     * 
     * @return Type of the crawler.
     */
    String getType();
}
