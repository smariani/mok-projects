package interfaces.centralunit;

import interfaces.IReceiverBase;

import java.util.List;

import javax.swing.JButton;

public interface IReceiver extends IReceiverBase {
	
	void setStart();

	/**
	 * If the search is stopped manually using the stop button, the results
	 * found until then are sent to the server.
	 */
	void setStopByButton();

	/**
	 * Sets the field for the start button.
	 * 
	 * @param btnStart
	 *            Start button.
	 */
	void setButtonStart(JButton btnStart);

	/**
	 * Sets the field to the list of connection handlers for crawlers.
	 * 
	 * @param c
	 *            List of connection handler for crawlers.
	 */
	void setConnections(List<Object> c);

}
