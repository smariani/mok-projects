package interfaces;

public interface ISearch {
	
    /**
     * Starts to examine the web pages.
     * 
     * @throws Exception
     *             Exception.
     */
    void startCrawling() throws Exception;

}
