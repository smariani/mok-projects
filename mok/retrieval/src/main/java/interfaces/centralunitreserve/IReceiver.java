package interfaces.centralunitreserve;

import interfaces.IReceiverBase;

public interface IReceiver extends IReceiverBase {
	
	/**
	 * Reset the counter of founded results.
	 * 
	 */
	void resetCount();

}
