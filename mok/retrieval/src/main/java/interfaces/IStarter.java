package interfaces;

/**
 * @author Giulio Crestani
 * @author Gianluca Spadazzi
 * @version 1.0
 *
 */

public interface IStarter {

	/**
	 * Send a start message to crawlers for tell them that the research must
	 * start.
	 * 
	 */
	void launchAll();

}
