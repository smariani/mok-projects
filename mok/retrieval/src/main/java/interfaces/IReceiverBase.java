package interfaces;

/**
 * @author Giulio Crestani
 * @author Gianluca Spadazzi
 * @version 1.0
 *
 */

public interface IReceiverBase {

	/**
	 * Adds the page to the list of Web Page upon inspection that is or is not
	 * present in the database or in the list.
	 * 
	 * @param wp
	 *            Page to add to list.
	 */
	void addWebPage(IWebPage wp);

	/**
	 * Keyword's setter.
	 * 
	 * @param keyword
	 *            String that identifies the WebPage's keyword .
	 */
	void setKeyword(String keyword);

	/**
	 * Set the field that indicates if the research is carried out with or
	 * without synonyms.
	 * 
	 * @param syn
	 *            indicates if the research is carried out with or without
	 *            synonyms.
	 */
	void setSynonyms(boolean syn);

	/**
	 * Set the field that indicates the number of results to be obtained from
	 * research.
	 * 
	 * @param n
	 *            indicates the number of results to be obtained from research.
	 */
	void setNumberOfResults(long n);

	/**
	 * Reset the counter of founded results.
	 * 
	 */
}
