package interfaces;

/**
 * @author Giulio Crestani
 * @author Gianluca Spadazzi
 * @version 1.0
 *
 */

public interface ICrawler {

    /**
     * Starts the research.
     */
    void start();

    /**
     * Stop the research.
     */
    void stop();

    /**
     * Set the fields about the research.
     * 
     * @param keyword
     *            Research's keyword.
     * @param withSynonims
     *            Boolean that tell if the research is with or without synonyms.
     */
    void receiveTask(String keyword, boolean withSynonims);

}
