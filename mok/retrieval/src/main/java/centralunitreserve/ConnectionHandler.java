package centralunitreserve;

import interfaces.IWebPage;
import interfaces.centralunitreserve.IReceiver;

import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;

import common.WebPage;

/**
 * Connection handler with the crawler.
 * 
 * @author Giulio Crestani
 * @author Gianluca Spadazzi
 * @version 1.0
 *
 */

public class ConnectionHandler extends Thread {

	private final Socket socket;
	private ObjectInputStream inStream;
	private ObjectOutputStream outStream;
	private boolean alreadyRecTask;

	private final IReceiver rec;

	/**
	 * Constructor that initialize fields.
	 * 
	 * @param s
	 *            Socket relative to connection with crawler.
	 * @param rec1
	 *            Receiver that takes care of receiving and filtering of
	 *            received pages.
	 */
	public ConnectionHandler(final Socket s, final IReceiver rec1) {
		this.alreadyRecTask = false;
		this.inStream = null;
		this.outStream = null;
		this.rec = rec1;
		this.socket = s;
		try {
			this.outStream = new ObjectOutputStream(this.socket.getOutputStream());
		} catch (IOException e) {
			e.printStackTrace();
		}
		try {
			this.inStream = new ObjectInputStream(this.socket.getInputStream());
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * It receives data of the task by crawlers and web pages which then
	 * will send to Receiver for their processing.
	 */
	@Override
	public void run() {
		while (true) {
			Object read = null;
			while (read == null) {
				try {
					read = this.inStream.readObject();
				} catch (ClassNotFoundException e) {
					e.printStackTrace();
				} catch (IOException e) {
					continue; // lasciato vuoto apposta; per le
								// preferenze
				}
			}
			if (read instanceof String) {
				// task da fare
				switch (((String) read).split(" ")[0]) {
				case ("TASK"):
					if (!this.alreadyRecTask) {
						System.out.println("TASK received by Central Unit Reserve");
						String keyword = ((String) read).split(" ")[1];
						int number = Integer.parseInt(((String) read).split(" ")[2]);
						boolean withSynonyms = Boolean.parseBoolean(((String) read).split(" ")[3]);
						SysKb.setKeyword(keyword);
						SysKb.setNumber(number);
						SysKb.setSyn(withSynonyms);

						// lo setto nel receiver
						this.rec.setKeyword(SysKb.getKeyword());
						this.rec.setNumberOfResults(SysKb.getNumber());
						this.rec.setSynonyms(SysKb.isSyn());
						this.rec.resetCount();
						this.alreadyRecTask = true;
					}
					break;
				default:
					break;
				}
				// se crawler si connettono alla seconda Central unit ma non
				// hanno i dati
				if (SysKb.isRunning()) {
					try {
						this.outStream.writeObject(
								"TASK " + SysKb.getKeyword() + " " + SysKb.getNumber() + " " + SysKb.isSyn());
						this.outStream.writeObject("START");
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
			} else if (read instanceof IWebPage) {
				System.out.println("CU RISERVA: RICEVO PAGINA");
				IWebPage p = new WebPage(((IWebPage) read).getKeyword(),
						((IWebPage) read).getTitle().replaceAll("\"", ""),
						((IWebPage) read).getLink(),
						((IWebPage) read).getSnippet().replaceAll("\"", ""),
						((IWebPage) read).getLength(),
						((IWebPage) read).getType());
				// aggiungo la pagina
				this.rec.addWebPage(p);	
			}
		}
	}

	/**
	 * Write an object on ObjectOutputStream.
	 * 
	 * @param o
	 *            Object to write.
	 */
	public void writeSocket(final Object o) {
		try {
			this.outStream.writeObject(o);
		} catch (IOException e) {
			System.out.println(); // lasciato vuoto apposta; per le preferenze
		}
	}

	/**
	 * Getter of the socket's ObjectOutputStream.
	 * 
	 * @return Socket's ObjectOutputStream.
	 */
	public ObjectOutputStream getOutputStream() {
		return this.outStream;
	}

	/**
	 * Getter of the socket's ObjectInputStream.
	 * 
	 * @return Socket's ObjectInputStream.
	 */
	public InputStream getInputStream() {
		return this.inStream;
	}

}
