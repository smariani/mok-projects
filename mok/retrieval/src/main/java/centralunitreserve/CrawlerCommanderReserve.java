package centralunitreserve;

import java.util.ArrayList;
import java.util.List;

import common.Sender;

import interfaces.ISender;
import interfaces.centralunitreserve.IReceiver;

/**
 * Contains reserve Central Unit main. Initialize lists and components.
 * 
 * @author Giulio Crestani
 * @author Gianluca Spadazzi
 * @version 1.0
 *
 */

public final class CrawlerCommanderReserve {

	/**
	 * Initialize the lists of web pages, Sender, Receiver and it starts thread
	 * relative of server.
	 * 
	 * @param args
	 *            Main parameters.
	 */
	public static void main(final String[] args) {

		List<Object> l2 = new ArrayList<Object>(); // web page

		ISender sender = new Sender(l2);
		IReceiver rec = new Receiver(l2, sender);

		ServerThread s = new ServerThread(rec);
		s.start();

	}

	private CrawlerCommanderReserve() {
	}

}
