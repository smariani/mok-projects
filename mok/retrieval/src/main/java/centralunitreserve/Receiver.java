package centralunitreserve;

import interfaces.ISender;
import interfaces.IWebPage;
import interfaces.centralunitreserve.IReceiver;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;


/**
 * It receives and filters web pages. In addition to this, it sends the message
 * STOP to crawler when the search ends.
 * 
 * @author Giulio Crestani
 * @author Gianluca Spadazzi
 * @version 1.0
 *
 */

// riceve i dati dai crawler
public class Receiver implements IReceiver {

	private List<Object> c;
	private long numberOfResults;
	private String keyword;
	private boolean searchSyn;
	private int index; // per dire a google da che indice cominciare a cercare
	private ISender send;

	// per i sinonimi
	private int countNormal = 0;
	private int countSyn = 0;
	private long maxNormal;
	private long maxSyn;

	private IWebPage currentPage;

	/**
	 * Constructor that initialize fields.
	 * 
	 * @param c1
	 *            List of pages.
	 * @param send1
	 *            Sender that send pages to online database.
	 */
	public Receiver(final List<Object> c1, final ISender send1) {
		this.c = c1;
		this.index = 0;
		this.send = send1;
		this.keyword = SysKb.getKeyword();
		this.numberOfResults = SysKb.getNumber();
		this.searchSyn = SysKb.isSyn();
		this.maxNormal = 0;
		this.maxSyn = 0;
	}

	public synchronized void addWebPage(final IWebPage wp) {
		this.currentPage = wp;
		// se cerco con o senza sinonimi
		if (!this.searchSyn) {
			try {
				addWebPageNormal(wp);
			} catch (ClassNotFoundException | SQLException e) {
				e.printStackTrace();
			}
		} else {
			try {
				addWebPageSynonyms(wp);
			} catch (ClassNotFoundException | SQLException e) {
				e.printStackTrace();
			}
		}
	}

	/**
	 * Adds the page to the list in case the research is without synonyms.
	 * 
	 * @param wp
	 *            Web page.
	 * @throws ClassNotFoundException
	 * @throws SQLException
	 */
	// classica aggiunta di pagina normale come abbiamo sempre fatto
	private void addWebPageNormal(final IWebPage wp) throws ClassNotFoundException, SQLException {
		// controllo che non sia nella lista della ricerca corrente
		if (!isContained(wp)) {
			// controllo anche che non sia gi� nel database
			if (!read(wp.getLink())) {
				this.c.add(wp);
				this.index++;
				System.out.println("NUMERO PAGINE IN LISTA: " + this.index);
				if (this.index == this.numberOfResults) {
					System.out.println("FERMO I CRAWLER");
					// fermi tutti i crawler
					for (int i = 0; i < ServerThread.getList().size(); i++) {
						ServerThread.getList().get(i).writeSocket("STOP");
					}
					// // invio al server
					this.send.setList(this.c);
					this.send.insert();

					for (int i = 0; i < ServerThread.getList().size(); i++) {
						this.c = new ArrayList<Object>();
					}

					// termino perche' sono quella di riserva
					System.exit(0);
				}
			}
		}
	}

	/**
	 * Adds the page to the list in case the research is with synonyms.
	 * 
	 * @param wp
	 *            Web Page.
	 * @throws ClassNotFoundException
	 * @throws SQLException
	 */
	// continuo a aggiungere fino a che non ho il 50% di risultati della keyword
	// e il 50% di sinonimi
	private void addWebPageSynonyms(final IWebPage wp) throws ClassNotFoundException, SQLException {
		// controllo che non sia nella lista della ricerca corrente
		if (!isContained(wp)) {
			// se la keyword e' la stessa tengo il conto nella rispettiva
			// variabile
			if (!read(wp.getLink()) && wp.getKeyword().equals(this.keyword)) {
				this.countNormal++;
				if (this.countNormal <= this.maxNormal) {
					// controllo di non eccedere il contatore
					this.c.add(wp);
					this.index++;
				}
			} else if (!read(wp.getLink()) && !wp.getKeyword().equals(this.keyword)) {
				this.countSyn++;
				if (this.countSyn <= this.maxSyn) {
					// controllo di non eccedere il contatore
					this.c.add(wp);
					this.index++;
				}

			}
			System.out.println("NUMERO PAGINE IN LISTA: " + this.index);
			if (this.index == this.numberOfResults) {
				System.out.println("FERMO I CRAWLER");
				// fermi tutti i crawler
				for (int i = 0; i < ServerThread.getList().size(); i++) {
					ServerThread.getList().get(i).writeSocket("STOP");
				}

				// // invio al server
				this.send.setList(this.c);
				this.send.insert();

				for (int i = 0; i < ServerThread.getList().size(); i++) {
					this.c = new ArrayList<Object>();
				}
				// termino perch� sono quella di riserva
				System.exit(0);
			}
		}
	}

	/**
	 * Check that the page is not contained in the list of current research.
	 * 
	 * @param page
	 *            Web page.
	 * @return Boolean that indicates if the page is or isn't contained in the
	 *         current list.
	 */
	private boolean isContained(final IWebPage page) {
		for (int i = 0; i < this.c.size(); i++) {
			if (((IWebPage) this.c.get(i)).getLink().equals(page.getLink())) {
				return true;
			}
		}
		return false;
	}

	/**
	 * Reads from the database online to check that a page is not contained in
	 * it. If it is included it is updated with the new data.
	 * 
	 * @param s
	 *            Page link.
	 * @return Boolean that indicates if the page is included it is updated with the new data.
	 * @throws ClassNotFoundException
	 * @throws SQLException
	 */
	private boolean read(final String s) throws ClassNotFoundException, SQLException {
		boolean res;
		Class.forName("com.mysql.jdbc.Driver");
		@SuppressWarnings("resource")
		Connection conn = DriverManager.getConnection(SysKb.serverString(), SysKb.dbName(), SysKb.dbPass());
		Statement stmt = conn.createStatement();
		String query = "SELECT * FROM Search WHERE LINK= \"" + s + "\"";
		ResultSet rs = stmt.executeQuery(query);
		// nessun dato
		if (!rs.isBeforeFirst()) {
			res = false;
		} else {
			res = true;
			// aggiorno la tupla nel database
			String update = null;
			if (this.currentPage.getType().equals("crawler4j")) {
				update = "UPDATE Search SET KEYWORD = \"" + this.currentPage.getKeyword() + "\",LINK = \""
						+ this.currentPage.getLink() + "\",TITLE = \"" + this.currentPage.getTitle() + "\",SNIPPET = \""
						+ this.currentPage.getSnippet() + "\",LENGTH = \"" + this.currentPage.getLength()
						+ "\" WHERE LINK = \"" + this.currentPage.getLink() + "\"";
			} else {
				update = "UPDATE Search SET KEYWORD = \"" + this.currentPage.getKeyword() + "\",LINK = \""
						+ this.currentPage.getLink() + "\",TITLE = \"" + this.currentPage.getTitle() + "\",SNIPPET = \""
						+ this.currentPage.getSnippet() + "\" WHERE LINK = \"" + this.currentPage.getLink() + "\"";
			}
			stmt.executeUpdate(update);
		}
		conn.close();
		return res;
	}

	public void setKeyword(final String keyword1) {
		this.keyword = keyword1;
	}

	public void setSynonyms(final boolean syn) {
		this.searchSyn = syn;
	}

	public void setNumberOfResults(final long n) {
		this.numberOfResults = n;
		
		long y = this.numberOfResults;
		float x = y;
		float per = SysKb.getPercentageSyn(); // Prendo percentuale di sinonimi
		this.maxSyn = (long) (x * (per / 100)); // Numero di risultati sinonimi
		this.maxNormal = y - this.maxSyn; // Numero di risultati normali
	}

	public void resetCount() {
		this.index = 0;
		this.countNormal = 0;
		this.countSyn = 0;
	}

}
