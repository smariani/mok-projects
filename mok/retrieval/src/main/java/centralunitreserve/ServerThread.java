package centralunitreserve;

import interfaces.centralunitreserve.IReceiver;

import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;

/**
 * It manages socket of the Central Unit and creates a Connection Handler for
 * each crawler connected.
 * 
 * @author Giulio Crestani
 * @author Gianluca Spadazzi
 * @version 1.0
 *
 */

public class ServerThread extends Thread {

	private ServerSocket serverSocket;
	private Socket socket = null;
	// mi salvo la lista delle socket dei client
	/**
	 * Lista delle socket dei client
	 */
	private static List<ConnectionHandler> list;

	// per identificare il crawler

	/**
	 * Getter of the Connection Handler list.
	 * 
	 * @return List of Connection Handler.
	 */
	public static List<ConnectionHandler> getList() {
		return list;
	}

	/**
	 * Setter of the Connection Handler list.
	 * 
	 * @param list1
	 *            List of Connection Handler.
	 */
	public static void setList(final List<ConnectionHandler> list1) {
		ServerThread.list = list1;
	}

	private IReceiver rec;

	/**
	 * Constructor that initialize internal field.
	 * 
	 * @param rec1
	 *            Receiver is the entity that is responsible for receiving and
	 *            filtering web pages.
	 */
	public ServerThread(final IReceiver rec1) {
		this.rec = rec1;
		this.serverSocket = null;
		list = new ArrayList<ConnectionHandler>();
	}

	/**
	 * Creates the socket on the server and a Connection Handler for each crawler that connects.
	 */
	@Override
	public void run() {
		try {
			this.serverSocket = new ServerSocket(SysKb.getPortServer(), SysKb.getMaxConn());
			while (true) {
				this.socket = this.serverSocket.accept();
				ConnectionHandler conn = new ConnectionHandler(this.socket, this.rec);
				conn.start();
				addToList(conn);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Add Connection Handler to internal list.
	 * 
	 * @param c
	 *            Connection Handler to add.
	 */
	public static void addToList(final ConnectionHandler c) {
		list.add(c);
	}

}
