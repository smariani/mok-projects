package centralunitreserve;

/**
 * Static class where it's possible to find all the information to make the
 * software work.
 * 
 * @author Giulio Crestani
 * @author Gianluca Spadazzi
 * @version 1.0
 *
 */

public final class SysKb {

	// info di rete
	private static int portServer = 4444;
	private static int maxconn = 10;

	private static boolean isRunning = false;

	// dati database online
	private static String serverString = "jdbc:mysql://sql2.freemysqlhosting.net:3306/sql288779";
	private static String dbName = "sql288779";
	private static String dbPass = "yX4*wU2%";
	private static String keyword;
	private static boolean syn;
	private static int number;
	
	private static int percentageSyn = 35;

	private SysKb() {
	}

	/**
	 * Getter of the boolean that indicates if the research is running or not.
	 * 
	 * 
	 * @return Boolean that indicates if the research is running or not.
	 */
	public static boolean isRunning() {
		return isRunning;
	}

	/**
	 * Set the boolean that indicates if the research is running or not.
	 * 
	 * @param stop
	 *            Boolean that indicates if the research is running or not.
	 */
	public static void setRunning(final boolean stop) {
		isRunning = stop;
	}

	/**
	 * Getter of the string for connect to database online.
	 * 
	 * @return String for connect to database online.
	 */
	public static String serverString() {
		return serverString;
	}

	/**
	 * Getter of the database name
	 * 
	 * @return Database name.
	 */
	public static String dbName() {
		return dbName;
	}

	/**
	 * Getter of the database password.
	 * 
	 * @return Database password.
	 */
	public static String dbPass() {
		return dbPass;
	}

	/**
	 * Getter of the research keyword.
	 * 
	 * @return Research keyword.
	 */
	public static String getKeyword() {
		return keyword;
	}

	/**
	 * Set the research keyword.
	 * 
	 * @param keyword1
	 *            Research keyword.
	 */
	public static void setKeyword(final String keyword1) {
		SysKb.keyword = keyword1;
	}

	/**
	 * It returns a boolean that indicates if the research is carried out with
	 * or without synonyms.
	 * 
	 * @return Boolean that indicates if the research is carried out with or
	 *         without synonyms.
	 */
	public static boolean isSyn() {
		return syn;
	}

	/**
	 * Set boolean that indicates if the research is carried out with or without
	 * synonyms.
	 * 
	 * @param syn1
	 *            Boolean that indicates if the research is carried out with or
	 *            without synonyms.
	 */
	public static void setSyn(final boolean syn1) {
		SysKb.syn = syn1;
	}

	/**
	 * Getter of the number of results to search.
	 * 
	 * @return Number of results to search.
	 */
	public static int getNumber() {
		return number;
	}

	/**
	 * Set the number of results to search.
	 * 
	 * @param number1
	 *            Number of results to search.
	 */
	public static void setNumber(final int number1) {
		number = number1;
	}

	/**
	 * Getter of the Central Unit port number.
	 * 
	 * @return Central Unit port number.
	 */
	public static int getPortServer() {
		return portServer;
	}

	/**
	 * Set the Central Unit port number.
	 * 
	 * @param portServer1
	 *            Central Unit port number.
	 *
	 **/
	public static void setPortServer(final int portServer1) {
		portServer = portServer1;
	}

	/**
	 * Getter of max number of connection that the Central Unit can accept.
	 * 
	 * @return Max number of connection that the Central Unit can accept.
	 */
	public static int getMaxConn() {
		return maxconn;
	}

	/**
	 * Getter of synonyms percentage.
	 * 
	 * @return Percentage of synonyms.
	 */
	public static int getPercentageSyn() {
		return percentageSyn;
	}
}
