package gui;

import interfaces.centralunit.IReceiver;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSeparator;
import javax.swing.JTextField;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;

import common.IntegerJTextField;

import centralunit.ButtonListener;

/**
 * GUI of the CrawlerCommander.
 * 
 * @author Giulio Crestani
 * @author Gianluca Spadazzi
 * @version 1.0
 *
 */

public class GUI extends JFrame {

    private static List<Object> c;
    private static IReceiver rec;
    private static final long serialVersionUID = 1L;
    private static final int MAXGAP = 20;
    private static JButton applyButton = new JButton("Apply gaps");
    private static GridLayout experimentLayout = new GridLayout(4, 1);

    /**
     * Initializes GUI. It's possible specify a keyword,
     * a number of pages to search and it is possible to start and stop
     * the search with respective buttons.
     * 
     * @param cc
     *            List of Connection handlers about the connected crawlers.
     * @param recc
     *            Receiver. Entity that is responsible for receiving and filter web pages.
     *            
     */
    public GUI(final List<Object> cc, final IReceiver recc) {
        super("CrawlerCommander");
        c = cc;
        rec = recc;

        try {
            // UIManager.setLookAndFeel("com.sun.java.swing.plaf.windows.WindowsLookAndFeel");
            UIManager.setLookAndFeel("javax.swing.plaf.metal.MetalLookAndFeel");
        } catch (UnsupportedLookAndFeelException ex) {
            ex.printStackTrace();
        } catch (IllegalAccessException ex) {
            ex.printStackTrace();
        } catch (InstantiationException ex) {
            ex.printStackTrace();
        } catch (ClassNotFoundException ex) {
            ex.printStackTrace();
        }
        /* Turn off metal's use of bold fonts */
        UIManager.put("swing.boldMetal", Boolean.FALSE);

        javax.swing.SwingUtilities.invokeLater(new Runnable() {

            public void run() {
                createAndShowGUI();
            }
        });
        setResizable(false);
        final int d1 = 300;
        final int d2 = 100;
        setLocation(d1, d2);
    }

    /**
     * Creates and show the GUI.
     */
    private static void createAndShowGUI() {
        // Create and set up the window.
        final JFrame frame = new JFrame("CrawlerCommander");
        final int d1 = 300;
        final int d2 = 100;
        frame.setLocation(d1, d2);
        frame.setResizable(false);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        // Set up the content pane.
        final JPanel compsToExperiment = new JPanel();
        compsToExperiment.setLayout(experimentLayout);

        // Creato solo per prendere a dimensione del bottone
        JButton b = new JButton("Just fake button");
        Dimension buttonSize = b.getPreferredSize();
        final double d3 = 2.5;
        compsToExperiment.setPreferredSize(new Dimension((int) (buttonSize
                .getWidth() * d3) + MAXGAP, (int) (buttonSize.getHeight() * 2)
                + MAXGAP * 2));

        // Add buttons to experiment with Grid Layout
        JLabel l = new JLabel("Keyword:");
        compsToExperiment.add(l);
        JTextField textSearch = new JTextField();
        compsToExperiment.add(textSearch);

        compsToExperiment.add(new JLabel("N Results:"));
        JTextField numberResult = new IntegerJTextField();
        compsToExperiment.add(numberResult);

        compsToExperiment.add(new JLabel());
        final JCheckBox myCheckBox = new JCheckBox("Cerca anche sinonimi");
        compsToExperiment.add(myCheckBox);

        JButton myButton = new JButton("Search");
        JButton myButton2 = new JButton("Stop");
        myButton2.setEnabled(false);
        compsToExperiment.add(myButton);
        compsToExperiment.add(myButton2);

        frame.getRootPane().setDefaultButton(myButton);

        // Process the Apply gaps button press
        applyButton.addActionListener(new ActionListener() {

            public void actionPerformed(final ActionEvent e) {
                // Set up the layout of the buttons
                experimentLayout.layoutContainer(compsToExperiment);
            }
        });
        numberResult.addFocusListener(new FocusListener() {

            @Override
            public void focusLost(final FocusEvent arg0) {
                System.out.println(); // lasciato vuoto apposta da preferenze.
            }

            @Override
            public void focusGained(final FocusEvent arg0) {
                numberResult.setText("");
            }
        });
        myButton.addActionListener(new ButtonListener(c, textSearch,
                numberResult, frame, myCheckBox, rec, myButton, myButton2));

        myButton2.addActionListener(new ButtonListener(c, textSearch,
                numberResult, frame, myCheckBox, rec, myButton, myButton2));

        frame.getContentPane().add(compsToExperiment, BorderLayout.NORTH);
        frame.getContentPane().add(new JSeparator(), BorderLayout.CENTER);
        // Display the window.
        frame.pack();
        frame.setVisible(true);
    }

    /**
     * Getter of the button test.
     * 
     * @return Button test.
     */
    public static JButton getApplyButton() {
        return applyButton;
    }

    /**
     * Setter of the button test.
     * 
     * @param applyButton1
     *            Button test.
     */
    public static void setApplyButton(final JButton applyButton1) {
        applyButton = applyButton1;
    }

    /**
     * Getter of the layout.
     * 
     * @return Layout.
     */
    public static GridLayout getExperimentLayout() {
        return experimentLayout;
    }

    /**
     * Setter of the layout.
     * 
     * @param experimentLayout1
     *            Layout.
     */
    public static void setExperimentLayout(final GridLayout experimentLayout1) {
        experimentLayout = experimentLayout1;
    }

}
