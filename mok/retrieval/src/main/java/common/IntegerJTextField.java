package common;

import javax.swing.JTextField;
import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.Document;
import javax.swing.text.PlainDocument;

/**
 * Class for manage TextFields. It allows you to enter only numbers.
 * 
 * @author Giulio Crestani
 * @author Gianluca Spadazzi
 * @version 1.0
 *
 */

public class IntegerJTextField extends JTextField {

    private static final long serialVersionUID = 1L;

    /**
     * Constructor without argument that invokes the JTextField's constructor.
     */
    public IntegerJTextField() {
        super();
    }

    /**
     * Constructor that initialize textfield with a number.
     * 
     * @param cols
     *            Number to write in the textfield.
     */
    public IntegerJTextField(final int cols) {
        super(cols);
        setText("" + cols);
    }

    @Override
    protected Document createDefaultModel() {
        return new UpperCaseDocument();
    }

    static class UpperCaseDocument extends PlainDocument {

        private static final long serialVersionUID = 1L;

        /**
         * Check to be inserted only numbers.
         */
        @Override
        public void insertString(final int offs, final String str,
                final AttributeSet a) throws BadLocationException {

            if (str == null) {
                return;
            }

            char[] chars = str.toCharArray();
            boolean ok = true;

            for (int i = 0; i < chars.length; i++) {

                try {
                    Integer.parseInt(String.valueOf(chars[i]));
                } catch (NumberFormatException exc) {
                    ok = false;
                    break;
                }

            }

            if (ok) {
                super.insertString(offs, new String(chars), a);
            }

        }
    }

}
