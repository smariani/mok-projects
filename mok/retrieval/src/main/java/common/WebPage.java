package common;

import interfaces.IWebPage;

import java.io.Serializable;

/**
 * Modella le pagine web e le relative informazioni.
 * 
 * @author Giulio Crestani 
 * @author Gianluca Spadazzi
 * @version 1.0
 *
 */

public class WebPage implements IWebPage, Serializable {
	private static final long serialVersionUID = 1L;
	private String title, link, keyword, text, length, snippet, type;

	/**
	 * Costruttore che inizializza i campi se la pagina � stata cercata da Google.
	 * 
	 * @param keyword1 Keyword della ricerca
	 * @param title1 Titolo della pagina
	 * @param link1 Link della pagina
	 * @param snippet1 Snippet della pagina
	 * @param type1 Tipo di crawler che ha cercato la pagina
	 */
	// costruttore crawler_google
	public WebPage(final String keyword1, final String title1, final String link1, final String snippet1,
			final String type1) {
		this.title = title1;
		this.link = link1;
		this.snippet = snippet1;
		this.keyword = keyword1;
		this.type = type1;
	}

	/**
	 * Costruttore che inizializza i campi se la pagina � stata cercata da Crawler4j.
	 * 
	 * @param keyword1 Keyword della ricerca
	 * @param title1 Titolo della pagina
	 * @param link1 Link della pagina
	 * @param snippet1 Snippet della pagina
	 * @param length1 Lunghezza della pagina
	 * @param type1 Tipo di crawler che ha cercato la pagina
	 */
	// costruttore crawler4j
	public WebPage(final String keyword1, final String title1, final String link1, final String snippet1,
			final String length1, final String type1) {
		this.title = title1;
		this.link = link1;
		this.snippet = snippet1;
		this.keyword = keyword1;
		this.length = length1;
		this.type = type1;
	}

	@Override
	public void setTitle(final String s) {
		this.title = s;
	}

	@Override
	public void setLink(final String s) {
		this.link = s;
	}

	@Override
	public void setKeyword(final String s) {
		this.keyword = s;
	}

	public String getTitle() {
		return this.title;
	}

	public String getLink() {
		return this.link;
	}

	public String getKeyword() {
		return this.keyword;
	}

	@Override
	public void setText(final String s) {
		this.length = s;

	}

	@Override
	public void setLength(final String s) {
		this.length = s;

	}

	@Override
	public String getLength() {
		return this.length;
	}

	@Override
	public String getText() {
		return this.text;
	}

	public String getSnippet() {
		return this.snippet;
	}

	public void setSnippet(final String snippet1) {
		this.snippet = snippet1;
	}

	public String getType() {
		return this.type;
	}

	public void setType(final String type1) {
		this.type = type1;
	}

}
