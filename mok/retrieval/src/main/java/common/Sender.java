package common;

import interfaces.ISender;
import interfaces.IWebPage;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import centralunit.SysKb;

/**
 * 
 * Si occupa di inviare la lista delle pagine con le relative informazioni al
 * database online.
 * 
 * @author Giulio Crestani
 * @author Gianluca Spadazzi
 * @version 1.0
 *
 */

// invia i dati al db una volta che abbiamo finito la ricerca
public class Sender implements ISender {

	private List<Object> list;

    /**
     * Costruttore che inizializza la lista di pagine web.
     * 
     * @param list1
     *            Lista delle pagine web.
     */
    public Sender(final List<Object> list1) {
        this.list = list1;
    }

    public void insert() {
        IWebPage p;
        String query = "";
        for (int i = 0; i < this.list.size(); i++) {
            p = (IWebPage) this.list.get(i);
            // salvi solo quelli che il crawler specifico possiede
            if (p.getType().equals("google")) {

                query = "INSERT INTO Search (KEYWORD,LINK,TITLE,SNIPPET) VALUES( \""
                        + p.getKeyword() + "\",\"" + p.getLink() + "\",\""
                        + p.getTitle() + "\",\"" + p.getSnippet() + "\");";
            } else if (p.getType().equals("crawler4j")) {
                query = "INSERT INTO Search (KEYWORD,LINK,TITLE,SNIPPET,LENGTH) VALUES( \""
                        + p.getKeyword() + "\",\"" + p.getLink() + "\",\""
                        + p.getTitle() + "\",\"" + p.getSnippet() + "\",\""
                        + p.getLength() + "\");";
            } else {
                continue; // altri tipi: to be continued
            }
            try {
                write(query);

            } catch (ClassNotFoundException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            } catch (SQLException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

        }
    }

    /**
     * Scrive la lista delle pagine nel database online.
     * 
     * @param query
     *            Query da eseguire.
     * @throws ClassNotFoundException
     * @throws SQLException
     */
    private static void write(final String query)
            throws ClassNotFoundException, SQLException {
        Class.forName("com.mysql.jdbc.Driver");
        @SuppressWarnings("resource")
        Connection conn = DriverManager.getConnection(SysKb.serverString(),
                SysKb.dbName(), SysKb.dbPass());
        Statement stmt = conn.createStatement();
        StringBuilder builder = new StringBuilder(query);
        for (int j = 0; j < query.length(); j++) {
            if (query.codePointAt(j) == 10) {
                builder.setCharAt(j, ' ');
            }
        }
        String query2 = builder.toString().replaceAll("\\s" + "\\s", " ");
        List<String> queries = new ArrayList<String>();
        queries.add(query2);
        int i = 0;
        while (queries.get(i).contains("\\s" + "\\s")) {
            queries.add(new String(queries.get(i).replace("\\s" + "\\s", " ")));
            i++;
        }

        // inserisco i nuovi dati
        stmt.executeUpdate(queries.get(queries.size() - 1));
        conn.close();

    }

    public void setList(final List<Object> list1) {
        this.list = list1;
    }

}
