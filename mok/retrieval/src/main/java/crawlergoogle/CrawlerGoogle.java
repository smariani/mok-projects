package crawlergoogle;


import interfaces.ICrawler;

import java.io.ObjectOutputStream;

/**
 * Represents the google crawler that does interact with their controller in
 * which there is all the logic.
 * 
 * @author Giulio Crestani
 * @author Gianluca Spadazzi
 * @version 1.0
 *
 */

public class CrawlerGoogle extends Thread implements ICrawler {

	private String keyword;
	private boolean withSynonyms;
	private ObjectOutputStream outputStream;

	/**
	 * Constructor that initialize internal fields.
	 * 
	 * @param outputStream1
	 *            Socket's ObjectOutputStream.
	 */
	public CrawlerGoogle(final ObjectOutputStream outputStream1) {
		this.keyword = "";
		this.withSynonyms = false;
		this.outputStream = outputStream1;

	}

	@Override
	public void run() {
		Controller c = new Controller(this.keyword, this.outputStream);
		try {
			if (this.withSynonyms) {
				// RICERCA CON SINONIMI
				c.startWithSynonyms();
			} else {
				// RICERCA SENZA SINONIMI
				c.launch();
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		}

	}

	@Override
	public void receiveTask(final String keyword1, final boolean withSynonyms1) {
		this.keyword = keyword1;
		this.withSynonyms = withSynonyms1;
	}

}
