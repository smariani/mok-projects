package crawlergoogle;

import interfaces.IController;

import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.List;

import edu.smu.tspell.wordnet.Synset;
import edu.smu.tspell.wordnet.SynsetType;
import edu.smu.tspell.wordnet.WordNetDatabase;

/**
 * Starts the search with or without synonyms. If the search is with synonyms,
 * is recovers them from WordNet.
 * 
 * @author Giulio Crestani
 * @author Gianluca Spadazzi
 * @version 1.0
 *
 */

// gestisce varie cose per alleggerire il codice. l'interfaccia grafica chiama i
// metodi del controller e basta, la logica sta qui non di l�
public class Controller implements IController {

	private String keyword;
	private SearchGoogle src;
	private ObjectOutputStream outputStream;

	/**
	 * Constructor that initialize fields.
	 * 
	 * @param keyword1
	 *            Search's keyword.
	 * @param outputStream1
	 *            Socket's ObjectOutputStream.
	 */
	public Controller(final String keyword1, final ObjectOutputStream outputStream1) {
		this.keyword = keyword1;
		this.outputStream = outputStream1;
		this.src = null;
	}

	// ricerca senza sinonimi
	public void launch() {
		this.src = new SearchGoogle(this.keyword, this.outputStream);
		this.src.startCrawling();
	}

	/**
	 * Starts the search with synonyms.
	 * 
	 * @param index
	 *            Start index.
	 */
	// ricerca con sinonimi
	private void startSyn(final long index) {
		this.src.setKeyword(this.keyword);
		this.src.startCrawlingWithSynonyms(index);
	}

	public void startWithSynonyms() {
		this.src = new SearchGoogle(this.keyword, this.outputStream);
		// prendo i sinonimi
		List<String> synonyms = getSynonyms(this.keyword);
		for (int i = 0; i < synonyms.size(); i++) {
			System.out.println("SINONIMO: " + synonyms.get(i));
		}
		int max = 0;
		long index = 1; // va passato a google per fargli capire da dove cercare
						// (senn� cerca sempre dall'inizio)
		// il primo sinonimo della lista � la keyword stessa. alterno 5
		// keyword,
		// 5 primo sinonimo..5 terzo sinonimo
		if (synonyms.size() > 0 && synonyms.size() < 3) {
			max = synonyms.size();
		} else if (synonyms.size() == 0) {
			// se non c'� neanche un sinonimo la da vuota. cerco senza
			// sinonimi
			// allora
			launch();
		} else {
			max = 3;
		}
		while (!SysKb.isStopped()) {
			for (int i = 0; i < max; i++) {
				// ricomincia fino a a che non � stoppato
				if (SysKb.isStopped()) {
					break;
				}
				this.keyword = synonyms.get(i);
				index = index + 1;
				startSyn(index);
			}
		}
	}

	/**
	 * Recovers word's synonyms.
	 * 
	 * @param s
	 *            Word to find synonyms.
	 * @return List containing synonyms of the word.
	 */
	// recupero i sinonimi
	private static List<String> getSynonyms(final String s) {
		System.setProperty("wordnet.database.dir", SysKb.wordnet());
		WordNetDatabase database = WordNetDatabase.getFileInstance();
		ArrayList<String> synonyms = new ArrayList<String>();
		String wordForm = s.toString();
		Synset[] synsets = database.getSynsets(wordForm, SynsetType.VERB);
		if (synsets.length > 0) {
			for (int i = 0; i < synsets.length; i++) {
				String[] wordForms = synsets[i].getWordForms();
				for (int j = 0; j < wordForms.length; j++) {
					if (!synonyms.contains(wordForms[j])) {
						synonyms.add(wordForms[j]);
					}
				}
			}
		}
		return synonyms;
	}

}
