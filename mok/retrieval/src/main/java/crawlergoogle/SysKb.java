package crawlergoogle;

/**
 * Static class where it's possible to find all the information to make the
 * software work.
 * 
 * @author Giulio Crestani
 * @author Gianluca Spadazzi
 * @version 1.0
 *
 */

public final class SysKb {

    private static boolean stopped = false;

    // info di rete
    private static String ipNormal = "127.0.0.1";
    private static String ipReserve = "127.0.0.1";
    private static int portNormal = 4445;
    private static int portReserve = 4444;

    // dati database online
    private static String serverString = "jdbc:mysql://sql2.freemysqlhosting.net:3306/sql288779";
    private static String dbName = "sql288779";
    private static String dbPass = "yX4*wU2%";
    // path wordnet sul pc
    private static String wordnet = "/Users/ste/Documents/Crestani-Spadazzi/WordNet-3.0/dict/";
    // da mandare alla central unit di riserva
    private static String keyword;
    private static boolean syn;
    private static int number;
    // se cerco con la cu di riserva mi termino
    private static boolean isReserve = false;

    // dati relativi al motore di ricerca creato, non vanno modificati
    private static String googleSrcUrl = "https://www.googleapis.com/customsearch/v1?";
    private static String apiKey = "AIzaSyBrjIRge_qdYx-0Xtx948k_KH2lxli5_5g";
    private static String searchEngineId = "007390308112418266202:xga2fmt6xd8";
    private static String finalUrl = googleSrcUrl + "key=" + apiKey + "&cx="
            + searchEngineId;

    private SysKb() {
    }

    /**
     * Getter of the string googleSrcUrl.
     * 
     * @return String googleSrcUrl.
     */
    public static String getGOOGLE_SEARCH_URL() {
        return googleSrcUrl;
    }

    /**
     * Getter of the string apiKey.
     * 
     * @return String apiKey.
     */
    public static String getAPI_KEY() {
        return apiKey;
    }

    /**
     * Getter of the string searchEngineId.
     * 
     * @return String searchEngineId.
     */
    public static String getSEARCH_ENGINE_ID() {
        return searchEngineId;
    }

    /**
     * Getter of the string finalUrl.
     * 
     * @return String finalUrl
     */
    public static String getFINAL_URL() {
        return finalUrl;
    }

    /**
     * Getter of the field that indicates if the research is terminated or not.
     * 
     * @return Boolean that indicates if the research is terminated (true) or
     *         not (false).
     */
    public static boolean isStopped() {
        return stopped;
    }

    /**
     * Set the field that indicates if the research is terminated or not.
     * 
     * @param stop
     *            Parameter that indicates if the research is terminated or not.
     */
    public static void setStopped(final boolean stop) {
        stopped = stop;
    }

    /**
     * Getter of the string for connect to database online.
     * 
     * @return String for connect to database online.
     */
    public static String serverString() {
        return serverString;
    }

    /**
     * Getter of the database name
     * 
     * @return Database name.
     */
    public static String dbName() {
        return dbName;
    }

    /**
     * Getter of the database password.
     * 
     * @return Database password.
     */
    public static String dbPass() {
        return dbPass;
    }

    /**
     * Getter of the path on local pc of the WordNet database.
     * 
     * @return path on local pc of the WordNet database.
     */
    public static String wordnet() {
        return wordnet;
    }

    /**
     * Getter of the research keyword.
     * 
     * @return Research keyword.
     */
    public static String getKeyword() {
        return keyword;
    }

    /**
     * Set the research keyword.
     * 
     * @param keyword1
     *            Research keyword.
     */
    public static void setKeyword(final String keyword1) {
        keyword = keyword1;
    }

    /**
     * It returns a boolean that indicates if the research is carried out with
     * or without synonyms.
     * 
     * @return Boolean that indicates if the research is carried out with or
     *         without synonyms.
     */
    public static boolean isSyn() {
        return syn;
    }

    /**
     * Set boolean that indicates if the research is carried out with or without
     * synonyms.
     * 
     * @param syn1
     *            Boolean that indicates if the research is carried out with or
     *            without synonyms.
     */
    public static void setSyn(final boolean syn1) {
        syn = syn1;
    }

    /**
     * Getter of the number of results to search.
     * 
     * @return Number of results to search.
     */
    public static int getNumber() {
        return number;
    }

    /**
     * Set the number of results to search.
     * 
     * @param number1
     *            Number of results to search.
     */
    public static void setNumber(final int number1) {
        number = number1;
    }

    /**
     * Getter of the boolean that indicates if is reserve Central Unit or not.
     * 
     * @return Boolean that indicates if is reserve Central Unit or not.
     */
    public static boolean isReserve() {
        return isReserve;
    }

    /**
     * Set the boolean that indicates if is reserve Central Unit or not.
     * 
     * @param isReserve1
     *            Boolean that indicates if is reserve Central Unit or not.
     */
    public static void setReserve(final boolean isReserve1) {
        isReserve = isReserve1;
    }

    /**
     * Getter of the main Central Unit IP.
     * 
     * @return Main Central Unit IP.
     */
    public static String getIpNormal() {
        return ipNormal;
    }

    /**
     * Set the main Central Unit IP.
     * 
     * @param ipNormal1
     *            Main Central Unit IP.
     */
    public static void setIpNormal(final String ipNormal1) {
        SysKb.ipNormal = ipNormal1;
    }

    /**
     * Getter of the reserve Central Unit IP.
     * 
     * @return Reserve Central Unit IP.
     */
    public static String getIpReserve() {
        return ipReserve;
    }

    /**
     * Set the reserve Central Unit IP.
     * 
     * @param ipReserve1
     *            Reserve Central Unit IP.
     */
    public static void setIpReserve(final String ipReserve1) {
        ipReserve = ipReserve1;
    }

    /**
     * Getter of the main Central Unit port number.
     * 
     * @return Main Central Unit port number.
     */
    public static int getPortNormal() {
        return portNormal;
    }

    /**
     * Set the main Central Unit port number.
     * 
     * @param portNormal1
     *            Main Central Unit port number.
     */
    public static void setPortNormal(final int portNormal1) {
        SysKb.portNormal = portNormal1;
    }

    /**
     * Getter of the reserve Central Unit port number.
     * 
     * @return Reserve Central Unit port number.
     */
    public static int getPortReserve() {
        return portReserve;
    }

    /**
     * Set the reserve Central Unit port number.
     * 
     * @param portReserve1
     *            Reserve Central Unit port number.
     */
    public static void setPortReserve(int portReserve1) {
        portReserve = portReserve1;
    }

}
