package crawlergoogle;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.InetAddress;
import java.net.Socket;
import java.net.SocketException;

/**
 * Class that tries to establish a connection to the Central Unit and that, once
 * connected, it reads the messages that arrive and handle them.
 * 
 * @author Giulio Crestani
 * @author Gianluca Spadazzi
 * @version 1.0
 *
 */

public class ClientThread extends Thread {
	private Socket socket;
	private static ObjectInputStream inputStream;
	private static ObjectOutputStream outputStream;
	private boolean isConnected;
	private CrawlerGoogle crawler;

	/**
	 * Constructor without arguments that initialize fields.
	 */
	public ClientThread() {
		this.socket = null;
		inputStream = null;
		outputStream = null;
		this.isConnected = false;
		this.crawler = null;
	}

	/**
	 * Establishes the connection with the server (central unit) and receive
	 * commands from it.
	 */
	@Override
	public void run() {

		while (!this.isConnected) {
			try {
				this.socket = new Socket(SysKb.getIpNormal(), SysKb.getPortNormal());
				System.out.println("Connesso alla central unit");
				try {
					outputStream = new ObjectOutputStream(this.socket.getOutputStream());
				} catch (Exception e) {
					System.out.println("Connessione rifiutata o altri problemi, termino!");
					System.exit(0);
				}
				try {
					inputStream = new ObjectInputStream(this.socket.getInputStream());
				} catch (Exception e) {
					System.out.println("Connessione rifiutata o altri problemi, termino!");
					System.exit(0);
				}
				this.isConnected = true;
				InetAddress a = InetAddress.getLocalHost();
				String ip = a.getHostAddress();
				// invio ip al server
				System.out.println("Ip client = " + ip);
				outputStream.writeObject("IP " + ip);

			} catch (SocketException se) {
				System.out.println("Central unit non raggiungibile, termino!");
				System.exit(0);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

		// ricevo comandi dal server
		while (true) {
			Object read = null;
			try {
				while (read == null) {
					read = inputStream.readObject();
				}

			} catch (ClassNotFoundException e) {
				e.printStackTrace();
			} catch (IOException e) {
				continue;
			}
			if (read instanceof String) {
				// task da fare
				switch (((String) read).split(" ")[0]) {
				case ("TASK"):
					System.out.println("TASK received by client");
					String keyword = ((String) read).split(" ")[1];
					int number = Integer.parseInt(((String) read).split(" ")[2]);
					boolean withSynonyms = Boolean.parseBoolean(((String) read).split(" ")[3]);
					SysKb.setKeyword(keyword);
					SysKb.setNumber(number);
					SysKb.setSyn(withSynonyms);
					// passo i dati al crawler
					this.crawler = new CrawlerGoogle(outputStream);
					this.crawler.receiveTask(keyword, withSynonyms);
					break;

				case ("START"):
					System.out.println("START received by client");
					SysKb.setStopped(false);
					this.crawler.start();
					break;

				case ("STOP"):
					System.out.println("STOP received by client");
					// termino tutto se cerco con quella di riserva
					if (SysKb.isReserve()) {
						System.exit(0);
					}
					SysKb.setStopped(true);
					break;
				case ("TEST"):
					System.out.println("TEST received by client");
					break;
				default:
					try {
						this.socket.close();
					} catch (IOException e) {
						e.printStackTrace();
					}
					break;
				}
			}

		}

	}

	/**
	 * Set ObjectInputStream.
	 * 
	 * @param in
	 *            ObjectInputStream to set up.
	 */
	public static void setInputStream(final ObjectInputStream in) {
		inputStream = in;
	}

	/**
	 * Getter of the socket's ObjectOutputStream.
	 * 
	 * @return Socket's ObjectOutpuStream.
	 */
	public static ObjectOutputStream getOutputStream() {
		return outputStream;
	}

}