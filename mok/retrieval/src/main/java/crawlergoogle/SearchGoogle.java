package crawlergoogle;

import com.google.api.client.http.HttpTransport;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.services.customsearch.Customsearch;
import com.google.api.services.customsearch.model.Result;
import com.google.api.services.customsearch.model.Search;
import common.WebPage;

import interfaces.IWebPage;
import interfaces.google.ISearchGoogle;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.net.SocketException;
import java.util.ArrayList;
import java.util.List;

/**
 * Executes crawling of the pages. It creates pages to send to Central Unit.
 * 
 * @author Giulio Crestani
 * @author Gianluca Spadazzi
 * @version 1.0
 *
 */

public class SearchGoogle implements ISearchGoogle {

	private String keyword;
	private List<IWebPage> data;
	private IWebPage page;

	private ObjectOutputStream outputStream;

	private Customsearch customsearch;
	private List<Result> resultList;

	/**
	 * Constructor that initialize internal fields.
	 * 
	 * @param keyword1
	 *            Search's keyword.
	 * @param outputStream1
	 *            Socket's ObjectOutputStream.
	 */
	// fa partire la ricerca e salva una lista di oggetti Page (su disco, poi li
	// manda al server)
	public SearchGoogle(final String keyword1, final ObjectOutputStream outputStream1) {
		this.keyword = keyword1;
		this.data = null;
		this.page = null;
		this.outputStream = outputStream1;

		HttpTransport httpTransport = new NetHttpTransport();
		JsonFactory jsonFactory = new JacksonFactory();
		this.customsearch = new Customsearch(httpTransport, jsonFactory, null);
		this.resultList = null;
	}

	public void startCrawling() {
		long cont = 0L;
		// per fare al massimo 10 risultati alla volta
		while (!SysKb.isStopped()) {
			this.resultList = getSearchResult(this.keyword, 10L, cont);
			cont++;
			// per la limitazione dei 100 risultati
			if (cont == 10) {
				SysKb.setStopped(true);
			}
			// se non ci sono risultati (es: parola che non esiste)
			if (this.resultList == null) {
				System.out.println("La parola cercata e' inesistente o non ci sono risultati per essa!");
				SysKb.setStopped(true);
			} else {
				// se ci sono risultati
				this.data = new ArrayList<IWebPage>(this.resultList.size());
				if (this.resultList.size() > 0) {
					for (Result result : this.resultList) {
						if (!SysKb.isStopped()) {
							// aggiunto la pagina alla lista e stampo i dati
							this.page = new WebPage(this.keyword, result.getHtmlTitle(), result.getLink(),
									result.getSnippet(), "google");
							this.data.add(this.page);
							try {
								this.outputStream.writeObject(this.page);
							} catch (IOException e) {
								connectReserve();
							}
						} else {
							break;
						}
					}

				}
			}
		}
	}

	public void startCrawlingWithSynonyms(final long index) {
		final int var1 = 20;
		final long var2 = 5L;
		// fa solo 5 risultati, quindi niente ciclo
		// indice per la limitazione dei risultati gratuiti
		if (index < var1) {
			this.resultList = getSearchResult(this.keyword, var2, index);
			this.data = new ArrayList<IWebPage>(this.resultList.size());
			if (this.resultList.size() > 0) {
				for (Result result : this.resultList) {
					if (!SysKb.isStopped()) {
						// aggiunto la pagina alla lista e stampo i dati
						this.page = new WebPage(this.keyword, result.getTitle(), result.getLink(), result.getSnippet(),
								"google");
						this.data.add(this.page);
						try {
							this.outputStream.writeObject(this.page);
						} catch (IOException e) {
							connectReserve();
						}
					} else {
						break;
					}
				}

			}
		}
	}

	/**
	 * Starting from a certain index, retrieves a number of pages.
	 * 
	 * @param keyword1
	 *            Search's keyword.
	 * @param num
	 *            Number of results.
	 * @param cont
	 *            Number of sub-search that were performed.
	 * @return List of search's results.
	 */
	// metodo che fa il crawling
	private List<Result> getSearchResult(final String keyword1, final long num, final long cont) {

		try {
			Customsearch.Cse.List list = this.customsearch.cse().list(keyword1);
			list.setKey(SysKb.getAPI_KEY());
			list.setCx(SysKb.getSEARCH_ENGINE_ID());
			// NUMERO DI RISULTATI DA VISUALIZZARE
			list.setNum(num);
			// PER PARTIRE DAL RISULTATO PRECEDENTE DELLA RICERCA E NON CERCARE
			// OGNI VOLTA LE STESSE COSE
			list.setStart((num * cont) + 1);
			Search results = list.execute();
			this.resultList = results.getItems();
		} catch (Exception e) {
			e.printStackTrace();
		}

		return this.resultList;

	}

	public List<IWebPage> getList() {
		return this.data;
	}

	/**
	 * Connect crawler to reserve Central Unit in case of the main Central Unit
	 * isn't achievable.
	 */
	private void connectReserve() {
		System.out.println("Central unit non raggiungibile, mi connetto a quella di riserva..");
		boolean isConnected = false;
		Socket socket = null;
		ObjectOutputStream o = null;
		ObjectInputStream i = null;
		while (!isConnected) {
			try {
				socket = new Socket(SysKb.getIpReserve(), SysKb.getPortReserve());
				socket.setSoTimeout(1000);
				System.out.println("Connesso alla central unit di riserva");
				try {
					o = new ObjectOutputStream(socket.getOutputStream());
				} catch (IOException e) {
					e.printStackTrace();
				}
				try {
					i = new ObjectInputStream(socket.getInputStream());
				} catch (IOException e) {
					e.printStackTrace();
				}
				isConnected = true;
				// ora inviera' allo stream della central unit di riserva!
				this.outputStream = o;
				ClientThread.setInputStream(i);
			} catch (SocketException se) {
				System.out.println("Central unit di riserva non raggiungibile, termino!");
				System.exit(0);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		SysKb.setReserve(true);
		// mandiamo i dati della ricerca alla cu di riserva
		try {
			o.writeObject("TASK " + SysKb.getKeyword() + " " + SysKb.getNumber() + " " + SysKb.isSyn());
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void setKeyword(final String keyword1) {
		this.keyword = keyword1;
	}

}
