package crawler4j;

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JSeparator;
import javax.swing.JTextField;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;

import common.IntegerJTextField;

/**
 * Class that contains main and that starts the crawler's GUI of Crawler4j.
 * 
 * @author Giulio Crestani
 * @author Gianluca Spadazzi
 * @version 1.0
 *
 */

public class CrawlerViewer extends JFrame {

    private static final long serialVersionUID = 1L;
    static final int MAXGAP = 15;
    private GridLayout experimentLayout = new GridLayout(0, 4);

    /**
     * Constructor that initialize frame's position.
     * 
     * @param name
     *            Name of the GUI.
     */
    public CrawlerViewer(final String name) {
        super(name);
        setResizable(false);
        final int w = 300, h = 100;
        setLocation(w, h);
    }

    /**
     * Add components to GUI.
     * 
     * @param pane
     *            GUI's container.
     */
    public void addComponentsToPane(final Container pane) {
        final JPanel compsToExperiment = new JPanel();
        compsToExperiment.setLayout(this.experimentLayout);

        // Creato solo per prendere a dimensione del bottone
        JButton b = new JButton("Just fake button");
        Dimension buttonSize = b.getPreferredSize();
        final double var1 = 3.5;
        final double var2 = 3;
        compsToExperiment.setPreferredSize(
                new Dimension((int) (buttonSize.getWidth() * var1) + MAXGAP,
                        (int) (buttonSize.getHeight() * var2) + MAXGAP * 2));

        compsToExperiment.add(new JLabel(""));
        compsToExperiment.add(new JLabel(""));
        compsToExperiment.add(new JLabel(""));
        compsToExperiment.add(new JLabel(""));

        JTextField ip1 = new JTextField("127.0.0.1");
        JTextField port1 = new IntegerJTextField(SysKb.getPortNormal());
        JTextField ip2 = new JTextField("127.0.0.1");
        JTextField port2 = new IntegerJTextField(SysKb.getPortReserve());

        // Add buttons to experiment with Grid Layout
        compsToExperiment.add(new JLabel("IP1:"));
        compsToExperiment.add(ip1);
        compsToExperiment.add(new JLabel("    Port1:"));
        compsToExperiment.add(port1);

        compsToExperiment.add(new JLabel(""));
        compsToExperiment.add(new JLabel(""));
        compsToExperiment.add(new JLabel(""));
        compsToExperiment.add(new JLabel(""));

        compsToExperiment.add(new JLabel("IP2:"));
        compsToExperiment.add(ip2);
        compsToExperiment.add(new JLabel("    Port2:"));
        compsToExperiment.add(port2);

        compsToExperiment.add(new JLabel(""));
        compsToExperiment.add(new JLabel(""));
        compsToExperiment.add(new JLabel(""));
        compsToExperiment.add(new JLabel(""));

        JButton launch = new JButton("Launch!");
        JButton stop = new JButton("Exit");
        stop.setEnabled(false);

        compsToExperiment.add(new JLabel(""));
        compsToExperiment.add(launch);
        compsToExperiment.add(stop);

        launch.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(final ActionEvent e) {
                if (checkIfIP(ip1.getText()) && checkIfIP(ip2.getText())
                        && checkIfPort(port1.getText())
                        && checkIfPort(port2.getText())) {
                    SysKb.setIpNormal(ip1.getText());
                    SysKb.setIpReserve(ip2.getText());
                    SysKb.setPortNormal(Integer.parseInt(port1.getText()));
                    SysKb.setPortReserve(Integer.parseInt(port2.getText()));
                    launch.setEnabled(false);
                    stop.setEnabled(true);
                    new ClientThread().start();
                } else {
                    JOptionPane.showMessageDialog(null,
                            "Inserire un valore in tutti i campi.", "Warning",
                            JOptionPane.WARNING_MESSAGE);
                }
            }

            private boolean checkIfIP(final String ip) {
                try {
                    if (ip == null || ip.isEmpty()) {
                        return false;
                    }

                    String[] parts = ip.split("\\.");
                    if (parts.length != 4) {
                        return false;
                    }

                    final int var = 255;
                    for (String s : parts) {
                        int i = Integer.parseInt(s);
                        if ((i < 0) || (i > var)) {
                            return false;
                        }
                    }
                    if (ip.endsWith(".")) {
                        return false;
                    }

                    return true;
                } catch (NumberFormatException nfe) {
                    return false;
                }
            }

            private boolean checkIfPort(final String port) {
                final int var = 5;
                if (port.length() > 0 && port.length() <= var) {
                    return true;
                }
                return false;
            }
        });

        stop.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(final ActionEvent e) {
                System.exit(0);
            }
        });

        pane.add(compsToExperiment, BorderLayout.NORTH);
        pane.add(new JSeparator(), BorderLayout.CENTER);
    }

    /**
     * Creates and shows GUI.
     */
    private static void createAndShowGUI() {
        // Create and set up the window.
        CrawlerViewer frame = new CrawlerViewer("CrawlerViewer Crawler4j");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        // Set up the content pane.
        frame.addComponentsToPane(frame.getContentPane());
        // Display the window.
        frame.pack();
        frame.setVisible(true);
    }

    /**
     * Basic set up to creats GUI.
     * 
     * @param args
     *            Main parameters.
     */
    public static void main(final String[] args) {
        /* Use an appropriate Look and Feel */
        try {
            // UIManager.setLookAndFeel("com.sun.java.swing.plaf.windows.WindowsLookAndFeel");
            UIManager.setLookAndFeel("javax.swing.plaf.metal.MetalLookAndFeel");
        } catch (UnsupportedLookAndFeelException ex) {
            ex.printStackTrace();
        } catch (IllegalAccessException ex) {
            ex.printStackTrace();
        } catch (InstantiationException ex) {
            ex.printStackTrace();
        } catch (ClassNotFoundException ex) {
            ex.printStackTrace();
        }
        /* Turn off metal's use of bold fonts */
        UIManager.put("swing.boldMetal", Boolean.FALSE);

        // Schedule a job for the event dispatch thread:
        // creating and showing this application's GUI.
        javax.swing.SwingUtilities.invokeLater(new Runnable() {

            public void run() {
                createAndShowGUI();
            }
        });
    }

}
