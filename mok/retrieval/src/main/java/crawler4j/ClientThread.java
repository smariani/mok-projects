package crawler4j;

import interfaces.IWebPage;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.InetAddress;
import java.net.Socket;
import java.net.SocketException;

/**
 * Class that tries to establish a connection to the Central Unit and that, once
 * connected, it reads the messages that arrive and handle them.
 * 
 * @author Giulio Crestani
 * @author Gianluca Spadazzi
 * @version 1.0
 *
 */

public class ClientThread extends Thread {

	private Socket socket;
	private static ObjectInputStream inputStream;
	private static ObjectOutputStream outputStream;
	private boolean isConnected;
	private Crawler4j crawler;
	private static int pageReceived;
	private static IWebPage pageToSend;

	/**
	 * Constructor without arguments that initialize fields.
	 */
	public ClientThread() {
		inputStream = null;
		outputStream = null;
		pageReceived = 0;
		pageToSend = null;
		this.socket = null;
		this.isConnected = false;
		this.crawler = null;
	}

	/**
	 * Establishes the connection with the central unit, sends to her the pages
	 * and receive commands from it.
	 * 
	 */
	@Override
	public void run() {

		while (!this.isConnected) {
			try {
				this.socket = new Socket(SysKb.getIpNormal(), SysKb.getPortNormal());
				// necessario per evitare che si blocchi tutto nella read
				// sull'inputstream
				final int timeout = 3000;
				this.socket.setSoTimeout(timeout);
				System.out.println("Connesso alla central unit");
				try {
					outputStream = new ObjectOutputStream(this.socket.getOutputStream());
				} catch (Exception e) {
					System.out.println("Connessione rifiutata o altri problemi, termino!");
					ClientThread.outputStream.close();
					System.exit(0);
				}
				try {
					inputStream = new ObjectInputStream(this.socket.getInputStream());
				} catch (Exception e) {
					System.out.println("Connessione rifiutata o altri problemi, termino!");
					ClientThread.inputStream.close();
					System.exit(0);
				}
				this.isConnected = true;
				InetAddress a = InetAddress.getLocalHost();
				String ip = a.getHostAddress();
				// invio ip al server
				System.out.println("Ip client = " + ip);
				outputStream.writeObject("IP " + ip);

			} catch (SocketException se) {
				System.out.println("Central unit non raggiungibile, termino!");
				System.exit(0);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

		// ricevo comandi dal server
		while (true) {
			Object read = null;
			while (read == null) {
				if (ClientThread.pageReceived == 1) {
					try {
						outputStream.writeObject(pageToSend);
					} catch (IOException e) {
						connectReserve();
					}
					pageReceived = 0;
				}
				try {
					read = inputStream.readObject();
				} catch (ClassNotFoundException e) {
					e.printStackTrace();
				} catch (IOException e) {
					continue;
				}

			}

			if (read instanceof String) {
				// task da fare
				switch (((String) read).split(" ")[0]) {
				case ("START"):
					System.out.println("START received by client");
					SysKb.setStopped(false);
					this.crawler.start();
					break;
				case ("TASK"):
					System.out.println("TASK received by client");
					String keyword = ((String) read).split(" ")[1];
					int number = Integer.parseInt(((String) read).split(" ")[2]);
					boolean withSynonyms = Boolean.parseBoolean(((String) read).split(" ")[3]);
					SysKb.setKeyword(keyword);
					SysKb.setNumber(number);
					SysKb.setSyn(withSynonyms);
					// passo i dati al crawler
					this.crawler = new Crawler4j();
					this.crawler.receiveTask(keyword, withSynonyms);
					break;

				case ("TEST"):
					System.out.println("TEST received by client");
					break;

				case ("STOP"):
					SysKb.setStopped(true);
					// termino tutto se cerco con quella di riserva
					System.out.println("STOP received by client");
					if (SysKb.isReserve()) {
						//chiudo gli stream e la socket
						try {
							this.socket.close();
							ClientThread.outputStream.close();
							ClientThread.inputStream.close();
						} catch (IOException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						System.exit(0);
					}
					break;

				default:
					break;
				}
			}

		}

	}

	/**
	 * Notify to crawler's thread that a page is ready to be sent to the central
	 * unit.
	 * 
	 * @param o
	 *            Page to send to Central Unit.
	 */
	public static void writeSocket(final IWebPage o) {
		pageReceived = 1;
		pageToSend = o;
	}

	/**
	 * Set ObjectInputStream.
	 * 
	 * @param in
	 *            ObjectInputStream to set up.
	 */
	public static void setInputStream(final ObjectInputStream in) {
		inputStream = in;
	}

	/**
	 * Getter of the socket's ObjectOutputStream.
	 * 
	 * @return Socket's ObjectOutpuStream.
	 */
	public static ObjectInputStream getInputStream() {
		return inputStream;
	}

	/**
	 * Set ObjectOutputStream.
	 * 
	 * @param out
	 *            ObjectOutputStream to set up.
	 */
	public static void setOutputStream(final ObjectOutputStream out) {
		outputStream = out;
	}

	/**
	 * Getter of the socket's ObjectOutputStream.
	 * 
	 * @return Socket's ObjectOutpuStream.
	 */
	public static ObjectOutputStream getOutputStream() {
		return outputStream;
	}

	/**
	 * Connect crawler to reserve Central Unit in case of the main Central Unit
	 * isn't achievable.
	 */
	private static void connectReserve() {
		System.out.println("Central unit non raggiungibile, mi connetto a quella di riserva..");
		boolean isConnected = false;
		Socket socket = null;
		ObjectOutputStream o = null;
		ObjectInputStream i = null;
		while (!isConnected) {
			try {
				socket = new Socket(SysKb.getIpReserve(), SysKb.getPortReserve());
				socket.setSoTimeout(1000);
				System.out.println("Connected");
				try {
					o = new ObjectOutputStream(socket.getOutputStream());
				} catch (IOException e) {
					e.printStackTrace();
				}
				try {
					i = new ObjectInputStream(socket.getInputStream());
				} catch (IOException e) {
					e.printStackTrace();
				}
				isConnected = true;
				// ora invier� allo stream della central unit di riserva!
				ClientThread.setOutputStream(o);
				ClientThread.setInputStream(i);
			} catch (SocketException se) {
				System.out.println("Central unit di riserva non raggiungibile, termino!");
				System.exit(0);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		SysKb.setReserve(true);
		// mandiamo i dati della ricerca alla cu di riserva
		try {
			o.writeObject("TASK " + SysKb.getKeyword() + " " + SysKb.getNumber() + " " + SysKb.isSyn());
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}
