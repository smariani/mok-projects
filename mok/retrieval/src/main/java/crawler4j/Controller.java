package crawler4j;

import interfaces.IController;
import interfaces.IWebPage;

import java.util.ArrayList;
import java.util.List;

import edu.smu.tspell.wordnet.Synset;
import edu.smu.tspell.wordnet.SynsetType;
import edu.smu.tspell.wordnet.WordNetDatabase;

/**
 * Starts the search with or without synonyms. If the search is with synonyms,
 * is recovers them from WordNet.
 * 
 * @author Giulio Crestani
 * @author Gianluca Spadazzi
 * @version 1.0
 *
 */

// gestisce varie cose per alleggerire il codice. l'interfaccia grafica chiama i
// metodi del controller e basta, la logica sta qui non di l�
public class Controller implements IController {

    private String keyword;
    private static List<IWebPage> list;

    /**
     * Constructor that initialize fields.
     * 
     * @param keyword1
     *            Search's keyword.
     */
    public Controller(final String keyword1) {
        this.keyword = keyword1;
        setList(new ArrayList<IWebPage>());
    }

    // ricerca senza sinonimi
    public void launch() throws Exception {
        Search4j src = new Search4j(this.keyword);
        src.startCrawling(); // la parte server e' in fetcher non qua come per
                             // google

    }

    public void startWithSynonyms() throws Exception {
        ArrayList<String> list1 = getSynonyms(this.keyword);
        Search4j src = new Search4j(list1);
        src.startCrawling(); // la parte server e' in fetcher non qua come per
                             // google

    }

    /**
     * Recovers word's synonyms.
     * 
     * @param s
     *            Word to find synonyms.
     * @return List containing synonyms of the word.
     */
    // recupero i sinonimi
    private static ArrayList<String> getSynonyms(final String s) {
        System.setProperty("wordnet.database.dir", SysKb.wordnet());
        WordNetDatabase database = WordNetDatabase.getFileInstance();
        ArrayList<String> synonyms = new ArrayList<String>();
        String wordForm = s.toString();
        Synset[] synsets = database.getSynsets(wordForm, SynsetType.VERB);
        if (synsets.length > 0) {
            for (int i = 0; i < synsets.length; i++) {
                String[] wordForms = synsets[i].getWordForms();
                for (int j = 0; j < wordForms.length; j++) {
                    if (!synonyms.contains(wordForms[j])) {
                        synonyms.add(wordForms[j]);
                    }
                }
            }
        }
        return synonyms;
    }

    /**
     * Getter of the web pages list.
     * 
     * @return List of web pages.
     */
    public static List<IWebPage> getList() {
        return list;
    }

    /**
     * Setter of the list.
     * 
     * @param list1
     *            List of web pages.
     */
    public static void setList(final List<IWebPage> list1) {
        Controller.list = list1;
    }

}
