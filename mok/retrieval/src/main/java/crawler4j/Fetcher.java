package crawler4j;

import interfaces.IWebPage;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.regex.Pattern;

import common.WebPage;

import edu.uci.ics.crawler4j.crawler.Page;
import edu.uci.ics.crawler4j.crawler.WebCrawler;
import edu.uci.ics.crawler4j.parser.HtmlParseData;
import edu.uci.ics.crawler4j.url.WebURL;

/**
 * Unlike Google here is the Fetcher that governs data pages it encounters.
 * Create pages to be sent subsequently to the Central Unit.
 * 
 * @author Giulio Crestani
 * @author Gianluca Spadazzi
 * @version 1.0
 *
 */

public class Fetcher extends WebCrawler {

	private List<IWebPage> list;
	private String keyword;
	private String snippet;
	// ESCLUDI QUEI TIPI DI FILE
	private static final Pattern FILTERS = Pattern.compile(".*(\\.(css|js|gif|" + "|png|mp3|mp3|zip|gz))$");

	/**
	 * Constructor without arguments that initialize internal fields.
	 */
	public Fetcher() {
		this.keyword = Search4j.keyword();
		this.snippet = "";
		this.list = new ArrayList<IWebPage>();
	}

	@Override
	public boolean shouldVisit(final Page referringPage, final WebURL url) {
		String href = url.getURL().toLowerCase();
		return !FILTERS.matcher(href).matches() && href.startsWith("https://it.wikipedia.org/");
	}

	/**
	 * Performs crawling.
	 */
	@Override
	public void visit(final Page page) {

		if (!SysKb.isStopped()) {
			// informazioni reperibili: titolo, url, lunghezza
			String link = page.getWebURL().getURL();
			if (page.getParseData() instanceof HtmlParseData) {
				HtmlParseData htmlParseData = (HtmlParseData) page.getParseData();
				String text = htmlParseData.getText();
				final int var = 200;
				// prendo lo snippet da tutto il testo
				if (text.length() > var) {
					this.snippet = text.substring(0, var) + "...";
				} else {
					this.snippet = text;
				}
				String title = htmlParseData.getTitle();
				String length = Integer.toString(htmlParseData.getHtml().length());
				if (!Search4j.listWords().isEmpty()) { // sinonimi
					for (int i = 0; i < Search4j.listWords().size(); i++) {
						if (text.contains(Search4j.listWords().get(i))) {
							IWebPage p = new WebPage(Search4j.listWords().get(i), title, link, this.snippet, length,
									"crawler4j");
							htmlParseData.getHtml();
							this.list.add(p);
							// invio il risultato
							ClientThread.writeSocket(p);
							break; // altrimenti potrebbe aggiungere piu' volte
									// la stessa pagina
						}
					}

				} else { // niente sinonimi
					String[] toSearch = this.keyword.split("-");
					if (toSearch.length == 1) {
						if (counterString(text, this.keyword) >= 2) {
							IWebPage p = new WebPage(this.keyword, title, link, this.snippet, length, "crawler4j");
							htmlParseData.getHtml();
							this.list.add(p);
							// invio il risultato
							ClientThread.writeSocket(p);
						}
						// per le keyword con pi� parole separate da TRATTINO
						// (-)
					} else {
						int count = 0;
						for (int i = 0; i < toSearch.length; i++) {
							if (text.contains(toSearch[i])) {
								count++;
							} else {
								count = 0;
							}
						}
						if (count == toSearch.length) {
							IWebPage p = new WebPage(this.keyword, title, link, this.snippet, length, "crawler4j");
							htmlParseData.getHtml();
							this.list.add(p);
							// invio il risultato
							ClientThread.writeSocket(p);
						}
					}

				}
				@SuppressWarnings("unused")
				Set<WebURL> links = htmlParseData.getOutgoingUrls();
			}

		} else {
			Search4j.terminate();
		}
	}

	/**
	 * Tell us how many times a string is contained in another string.
	 * 
	 * @param s
	 *            String where search another string.
	 * @param search
	 *            String to search.
	 * @return Times that the word is contained.
	 */
	private int counterString(final String s, final String search) {
		int times = 0;
		int index = s.indexOf(search, 0);
		while (index > 0) {
			index = s.indexOf(search, index + 1);
			++times;
		}
		return times;
	}

}
