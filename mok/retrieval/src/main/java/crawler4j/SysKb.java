package crawler4j;

/**
 * Static class where it's possible to find all the information to make the
 * software work.
 * 
 * @author Giulio Crestani
 * @author Gianluca Spadazzi
 * @version 1.0
 *
 */

public final class SysKb {

	// link da cui fare il crawling
	private static String linkCrawling = "https://it.wikipedia.org/";

	// info di rete
	private static String ipNormal = "127.0.0.1";
	private static String ipReserve = "127.0.0.1";
	private static int portNormal = 4445;
	private static int portReserve = 4444;

	private static boolean stopped = false;

	// dati database online
	private static String serverString = "jdbc:mysql://sql2.freemysqlhosting.net:3306/sql288779";
	private static String dbName = "sql288779";
	private static String dbPass = "yX4*wU2%";
	// dove salvo i dati in locale
	private static String savedData = "/Users/ste/Documents/Crestani-Spadazzi/CrawlerCommander/data/";
	// path wordnet sul pc
	private static String wordnet = "/Users/ste/Documents/Crestani-Spadazzi/WordNet-3.0/dict/";
	// numero di crawler concorrenti (puoi cercare in pi� domini
	// concorrentemente volendo, magari wikipedia e un altro sito per esempio.
	// ma nel nostro caso non serve)
	private static int numCrawlers = 1;

	// da mandare alla central unit di riserva
	private static String keyword;
	private static boolean syn;
	private static int number;

	// se cerco con la cu di riserva mi termino
	private static boolean isReserve = false;

	private SysKb() {
	}

	/**
	 * Getter of the field that indicates if the research is terminated or not.
	 * 
	 * @return Boolean that indicates if the research is terminated (true) or
	 *         not (false).
	 */
	public static boolean isStopped() {
		return stopped;
	}

	/**
	 * Set the field that indicates if the research is terminated or not.
	 * 
	 * @param stop
	 *            Parameter that indicates if the research is terminated or not.
	 */
	public static void setStopped(final boolean stop) {
		stopped = stop;
	}

	/**
	 * Getter of the string for connect to database online.
	 * 
	 * @return String for connect to database online.
	 */
	public static String serverString() {
		return serverString;
	}

	/**
	 * Getter of the database name
	 * 
	 * @return Database name.
	 */
	public static String dbName() {
		return dbName;
	}

	/**
	 * Getter of the database password.
	 * 
	 * @return Database password.
	 */
	public static String dbPass() {
		return dbPass;
	}

	/**
	 * Getter of the path of the folder on the local PC that is used to to
	 * create the files needed to complete its operations
	 * 
	 * @return Path of the folder on your local PC used by crawler4j.
	 */
	public static String savedData() {
		return savedData;
	}

	/**
	 * Getter of the path on local pc of the WordNet database.
	 * 
	 * @return path on local pc of the WordNet database.
	 */
	public static String wordnet() {
		return wordnet;
	}

	/**
	 * Getter in the number of concurrent crawlers (not used at the time, used
	 * to set the research on multiple domains at once)
	 * 
	 * @return Number of crawlers.
	 */
	public static int numCrawlers() {
		return numCrawlers;
	}

	/**
	 * Getter of the research keyword.
	 * 
	 * @return Research keyword.
	 */
	public static String getKeyword() {
		return keyword;
	}

	/**
	 * Set the research keyword.
	 * 
	 * @param keyword1
	 *            Research keyword.
	 */
	public static void setKeyword(final String keyword1) {
		SysKb.keyword = keyword1;
	}

	/**
	 * It returns a boolean that indicates if the research is carried out with
	 * or without synonyms.
	 * 
	 * @return Boolean that indicates if the research is carried out with or
	 *         without synonyms.
	 */
	public static boolean isSyn() {
		return syn;
	}

	/**
	 * Set boolean that indicates if the research is carried out with or without
	 * synonyms.
	 * 
	 * @param syn1
	 *            Boolean that indicates if the research is carried out with or
	 *            without synonyms.
	 */
	public static void setSyn(final boolean syn1) {
		SysKb.syn = syn1;
	}

	/**
	 * Getter of the number of results to search.
	 * 
	 * @return Number of results to search.
	 */
	public static int getNumber() {
		return number;
	}

	/**
	 * Set the number of results to search.
	 * 
	 * @param number1
	 *            Number of results to search.
	 */
	public static void setNumber(final int number1) {
		SysKb.number = number1;
	}

	/**
	 * Getter of the boolean that indicates if is reserve Central Unit or not.
	 * 
	 * @return Boolean that indicates if is reserve Central Unit or not.
	 */
	public static boolean isReserve() {
		return isReserve;
	}

	/**
	 * Set the boolean that indicates if is reserve Central Unit or not.
	 * 
	 * @param isReserve1
	 *            Boolean that indicates if is reserve Central Unit or not.
	 */
	public static void setReserve(final boolean isReserve1) {
		SysKb.isReserve = isReserve1;
	}

	/**
	 * Getter of the main Central Unit IP.
	 * 
	 * @return Main Central Unit IP.
	 */
	public static String getIpNormal() {
		return ipNormal;
	}

	/**
	 * Set the main Central Unit IP.
	 * 
	 * @param ipNormal1
	 *            Main Central Unit IP.
	 */
	public static void setIpNormal(final String ipNormal1) {
		SysKb.ipNormal = ipNormal1;
	}

	/**
	 * Getter of the reserve Central Unit IP.
	 * 
	 * @return Reserve Central Unit IP.
	 */
	public static String getIpReserve() {
		return ipReserve;
	}

	/**
	 * Set the reserve Central Unit IP.
	 * 
	 * @param ipReserve1
	 *            Reserve Central Unit IP.
	 */
	public static void setIpReserve(final String ipReserve1) {
		SysKb.ipReserve = ipReserve1;
	}

	/**
	 * Getter of the main Central Unit port number.
	 * 
	 * @return Main Central Unit port number.
	 */
	public static int getPortNormal() {
		return portNormal;
	}

	/**
	 * Set the main Central Unit port number.
	 * 
	 * @param portNormal1
	 *            Main Central Unit port number.
	 */
	public static void setPortNormal(final int portNormal1) {
		SysKb.portNormal = portNormal1;
	}

	/**
	 * Getter of the reserve Central Unit port number.
	 * 
	 * @return Reserve Central Unit port number.
	 */
	public static int getPortReserve() {
		return portReserve;
	}

	/**
	 * Set the reserve Central Unit port number.
	 * 
	 * @param portReserve1
	 *            Reserve Central Unit port number.
	 */
	public static void setPortReserve(final int portReserve1) {
		SysKb.portReserve = portReserve1;
	}

	/**
	 * Getter of the link where do the crawling.
	 * 
	 * @return Link where do the crawling.
	 */
	public static String getLinkCrawling() {
		return linkCrawling;
	}

	/**
	 * Setta il link da cui fare il crawling.
	 * 
	 * @param linkCrawling1
	 *            Link da cui fare il crawling.
	 */
	public static void setLinkCrawling(final String linkCrawling1) {
		SysKb.linkCrawling = linkCrawling1;
	}

}
