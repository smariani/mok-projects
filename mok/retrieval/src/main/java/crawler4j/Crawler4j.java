package crawler4j;

import interfaces.ICrawler;

/**
 * Represents Crawler4j crawler that does interact with their controller in
 * which there is all the logic.
 * 
 * @author Giulio Crestani 
 * @author Gianluca Spadazzi
 * @version 1.0
 *
 */

public class Crawler4j extends Thread implements ICrawler {
	private String keyword;
	private boolean withSynonyms;

	/**
	 * Constructor that initialize fields.
	 */
	public Crawler4j() {
		this.keyword = "";
		this.withSynonyms = false;
	}

	@Override
	public void run() {
		Controller c = new Controller(this.keyword);
		try {
			if (this.withSynonyms) {
				// RICERCA CON SINONIMI
				c.startWithSynonyms();
			} else {
				// RICERCA SENZA SINONIMI
				c.launch();
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}

	@Override
	public void receiveTask(final String keyword1,
			final boolean withSynonims) {
		this.keyword = keyword1;
		this.withSynonyms = withSynonims;

	}

}
