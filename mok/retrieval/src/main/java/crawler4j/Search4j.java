package crawler4j;

import interfaces.crawler4j.ISearch4j;
import java.io.File;
import java.util.ArrayList;
import edu.uci.ics.crawler4j.crawler.CrawlConfig;
import edu.uci.ics.crawler4j.crawler.CrawlController;
import edu.uci.ics.crawler4j.fetcher.PageFetcher;
import edu.uci.ics.crawler4j.robotstxt.RobotstxtConfig;
import edu.uci.ics.crawler4j.robotstxt.RobotstxtServer;

/**
 * Executes crawling of the pages.
 * 
 * @author Giulio Crestani
 * @author Gianluca Spadazzi
 * @version 1.0
 *
 */

public class Search4j implements ISearch4j {

	private static String keyword;
	private static ArrayList<String> listWords;
	/**
	 * Crawler4j controller.
	 */
	private static CrawlController controller;

	/**
	 * Constructor relative to research without synonyms that initializes
	 * internal fields.
	 * 
	 * @param keyword1
	 *            Research keyword.
	 */
	public Search4j(final String keyword1) {
		Search4j.keyword = keyword1;
		listWords = new ArrayList<String>();
		controller = null;
	}

	/**
	 * Constructor relative to research with synonyms that initializes the field
	 * relative to the lists of synonyms.
	 * 
	 * @param listWords1
	 *            Represents the list of synonyms of the specified keyword.
	 */
	// con sinonimi
	public Search4j(final ArrayList<String> listWords1) {
		Search4j.listWords = listWords1;
	}

	/**
	 * Getter of the search's keyword.
	 * 
	 * @return Search's keyword.
	 */
	public static String keyword() {
		return keyword;
	}

	/**
	 * Getter of the synonyms' keyword's list.
	 * 
	 * @return Synonyms' keyword's list.
	 */
	public static ArrayList<String> listWords() {
		return listWords;
	}

	public void startCrawling() throws Exception {
		String crawlStorageFolder = SysKb.savedData();
		CrawlConfig config = new CrawlConfig();
		config.setCrawlStorageFolder(crawlStorageFolder);

		PageFetcher pageFetcher = new PageFetcher(config);
		RobotstxtConfig robotstxtConfig = new RobotstxtConfig();
		RobotstxtServer robotstxtServer = new RobotstxtServer(robotstxtConfig, pageFetcher);
		controller = new CrawlController(config, pageFetcher, robotstxtServer);

		controller.addSeed(SysKb.getLinkCrawling());

		deleteFolder(new File(SysKb.savedData()));

		controller.start(Fetcher.class, SysKb.numCrawlers()); // numero di
																// crawler
		Thread.currentThread().interrupt();
	}

	/**
	 * Cleans the folder where crawler4j creates files it uses to carry out its
	 * tasks. Should clean it every research to avoid errors.
	 * 
	 * @param folder
	 */
	// pulire la cartella dei file di crawler4j. dopo un po' di ricerche da
	// errore, mentre se la pulisci no
	private static void deleteFolder(final File folder) {
		File[] files = folder.listFiles();
		if (files != null) { // some JVMs return null for empty dirs
			for (File f : files) {
				if (f.isDirectory()) {
					deleteFolder(f);
				} else {
					f.delete();
				}
			}
		}
		folder.delete();
	}

	/**
	 * Finish the crawling.
	 */
	public static void terminate() {
		controller.shutdown();
		controller.waitUntilFinish();
	}

}
