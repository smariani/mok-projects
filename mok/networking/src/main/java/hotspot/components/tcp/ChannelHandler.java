package hotspot.components.tcp;

import io.netty.channel.ChannelHandler.Sharable;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;
import java.io.IOException;
import model.payload.TransientCompartment;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 *
 * Netty channel handler implementation As sharable, it must be thread-safe.
 *
 * Used by TCPServer
 *
 *
 * @author Giacomo Dradi (giacomo.dradi@studio.unibo.it)
 *
 */
@Sharable
class ChannelHandler extends SimpleChannelInboundHandler<String> {

    private final TCPHotspot hotspot;
    private final Log log = LogFactory.getLog(ChannelHandler.class);
    private final ObjectMapper mapper;

    /**
     *
     * @param hotspot
     * @param mapper
     */
    ChannelHandler(final TCPHotspot hotspot, final ObjectMapper mapper) {
        this.hotspot = hotspot;
        this.mapper = mapper;
    }

    @Override
    public void channelActive(final ChannelHandlerContext ctx) throws Exception {
        super.channelActive(ctx);
        if (this.log.isInfoEnabled()) {
            this.log.info("Opened channel bound to: "
                    + ctx.channel().remoteAddress());
        }
    }

    @Override
    public void channelInactive(final ChannelHandlerContext ctx)
            throws Exception {
        /*
         * TODO Handle disconnections here?
         */
        super.channelInactive(ctx);
        this.hotspot.compartmentDisconnected(null, ctx.channel());
        if (this.log.isInfoEnabled()) {
            this.log.info("Closed channel bound to: "
                    + ctx.channel().remoteAddress());
        }
    }

    /**
     * Called when a message is decoded (that is after a "\n")
     */
    @Override
    public void channelRead0(final ChannelHandlerContext ctx, final String json) {

        // Decode JSON to Compartment
        try {
            final TransientCompartment tc = this.mapper.readValue(json,
                    TransientCompartment.class);
            if (tc.isLeaving()) {
                this.hotspot.compartmentDisconnected(tc.getCompartment(),
                        ctx.channel());
            } else {
                this.hotspot.compartmentConnected(tc.getCompartment(),
                        ctx.channel());
            }
            return;
        } catch (final IOException e) {
            e.printStackTrace();
        }

        /*
         * JSON failed to parse
         */
        if (this.log.isFatalEnabled()) {
            this.log.fatal("failed to parse json object: " + json);
        }
    }

    @Override
    public void exceptionCaught(final ChannelHandlerContext ctx,
            final Throwable cause) {
        // cause.printStackTrace();
        ctx.close();
    }

}
