package hotspot.components.tcp;

import interfaces.model.ICompartment;
import io.netty.bootstrap.Bootstrap;
import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.Channel;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelPipeline;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioServerSocketChannel;
import io.netty.channel.socket.nio.NioSocketChannel;
import io.netty.handler.codec.LineBasedFrameDecoder;
import io.netty.handler.codec.string.StringDecoder;
import io.netty.handler.codec.string.StringEncoder;
import io.netty.util.concurrent.GenericFutureListener;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import model.Compartments;
import model.payload.TransientCompartment;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import com.fasterxml.jackson.annotation.JsonAutoDetect.Visibility;
import com.fasterxml.jackson.annotation.PropertyAccessor;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 *
 * @author Giacomo Dradi (giacomo.dradi@studio.unibo.it)
 *
 */
public class TCPHotspot {

    public static final String DEF_MAX_JSON_STRING_LENGTH = "100000";

    public static final String DEF_TCP_PORT = "8092";
    public static final String MAX_JSON_STRING_LENGTH_KEY = "MAX_JSON_STRING_LENGTH";
    public static final String TCP_PORT_KEY = "TCP_PORT";

    /**
     * Decides if two compartments are neighbours
     *
     * @param compartment1
     *            the first compartment to test
     * @param compartment2
     *            the second compartment to test
     * @return {@code true} if neighbours, {@code false} otherwise
     */
    private static boolean areNeighbours(final ICompartment compartment1,
            final ICompartment compartment2) {
        // TODO TCPHotspot.areNeighbours
        return true;
    }

    private Bootstrap bootstrap;

    private final Map<Channel, ICompartment> connections;

    private final Log log = LogFactory.getLog(TCPHotspot.class);

    private final ObjectMapper mapper;
    private NioEventLoopGroup nioGroup;
    private Properties properties;
    private ServerBootstrap serverBootstrap;

    /**
     * @param filepath
     *            Properties file path
     *
     */
    public TCPHotspot(final String filepath) {
        this.mapper = new ObjectMapper().setVisibility(PropertyAccessor.FIELD,
                Visibility.ANY);
        this.connections = new Hashtable<Channel, ICompartment>();
        this.parseProperties(filepath);
    }

    /**
     * Called by ChannelHandler when a new compartment connects
     *
     *
     * @param newCompartment
     *            the new compartment connected
     * @param channel
     *            the channel supporting the connection
     */
    public void compartmentConnected(final ICompartment newCompartment,
            final Channel channel) {

        /*
         * Add new compartment to the local list of compartments, replace old
         * one if already existing
         */
        Set<Channel> channels = this.connections.keySet();
        for (final Channel c : channels) {
            if (this.connections.get(c).getID().equals(newCompartment.getID())) {
                if (this.log.isInfoEnabled()) {
                    this.log.info("Removed old compartment "
                            + this.connections.get(c));
                }
                this.connections.remove(c);
                break;
            }
        }
        if (this.log.isInfoEnabled()) {
            this.log.info("Added new compartment: " + newCompartment);
        }
        this.connections.put(channel, newCompartment);

        /*
         * Send neighbourhood compartments to newCompartment + Send
         * newCompartment to already connected neighbouring compartments
         */
        final List<ICompartment> fullListToSend = new ArrayList<ICompartment>();
        channels = this.connections.keySet();
        for (final Channel c : channels) {
            if (this.connections.get(c) != newCompartment
                    && TCPHotspot.areNeighbours(this.connections.get(c),
                            newCompartment)) {
                fullListToSend.add(this.connections.get(c));
                this.sendCompartmentNotification(false, newCompartment,
                        this.connections.get(c));
            }
        }
        try {
            final String json = this.mapper
                    .writeValueAsString(new Compartments(fullListToSend));
            channel.writeAndFlush(json + "\n");
            if (this.log.isInfoEnabled()) {
                this.log.info("Sent #" + fullListToSend.size()
                        + " neighbour(s) to compartment: " + newCompartment);
            }
        } catch (final JsonProcessingException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

    }

    /**
     * Called by ChannelHandler when a compartment disconnects
     *
     *
     * @param compartment
     *            the compartment disconnected
     * @param channel
     *            the channel supporting the connection
     */
    public void compartmentDisconnected(final ICompartment compartment,
            final Channel channel) {

        ICompartment leaving = compartment;
        Set<Channel> channels;
        /*
         * Remove old compartment from the local list of compartments
         */
        if (leaving == null) {
            leaving = this.connections.remove(channel);
            if (leaving != null && this.log.isInfoEnabled()) {
                this.log.info("Removed disconnected compartment "
                        + this.connections.get(channel));
            }
        } else {
            channels = this.connections.keySet();
            for (final Channel c : channels) {
                if (this.connections.get(c).getID().equals(leaving.getID())) {
                    if (this.log.isInfoEnabled()) {
                        this.log.info("Removed disconnected compartment "
                                + leaving);
                    }
                    this.connections.remove(c);
                    break;
                }
            }
        }

        if (leaving != null) {
            channels = this.connections.keySet();
            for (final Channel c : channels) {
                if (this.connections.get(c) != leaving
                        && TCPHotspot.areNeighbours(this.connections.get(c),
                                leaving)) {
                    this.sendCompartmentNotification(true, leaving,
                            this.connections.get(c));
                }
            }
        }

    }

    /**
     * Disposes Netty instances
     */
    public void dispose() {
        this.nioGroup.shutdownGracefully();
        if (this.log.isInfoEnabled()) {
            this.log.info("Disposed");
        }
    }

    /**
     * Initialises a TCP Netty server and client
     *
     * Messages are handled through TCP using a LineBasedFrameDecoder, i.e a
     * message is considered received and forwarded to channelHanndler after a
     * "\n".
     *
     */
    public void init() {
        this.nioGroup = new NioEventLoopGroup();
        this.serverBootstrap = new ServerBootstrap();
        this.serverBootstrap.group(this.nioGroup)
        .channel(NioServerSocketChannel.class)
        .childHandler(new ChannelInitializer<SocketChannel>() {

            private final StringDecoder DECODER = new StringDecoder();
            private final StringEncoder ENCODER = new StringEncoder();
            private final ChannelHandler HANDLER = new ChannelHandler(
                    TCPHotspot.this, TCPHotspot.this.mapper);

            @Override
            public void initChannel(final SocketChannel ch)
                    throws Exception {
                final ChannelPipeline p = ch.pipeline();
                p.addLast(new LineBasedFrameDecoder(
                                Integer.parseInt((String) TCPHotspot.this.properties
                                .get(TCPHotspot.MAX_JSON_STRING_LENGTH_KEY))));
                p.addLast(this.DECODER);
                p.addLast(this.ENCODER);
                p.addLast(this.HANDLER);
            }
        });
        this.bootstrap = new Bootstrap();
        this.bootstrap.group(this.nioGroup).channel(NioSocketChannel.class)
        .handler(new ChannelInitializer<SocketChannel>() {

            private final StringDecoder DECODER = new StringDecoder();
            private final StringEncoder ENCODER = new StringEncoder();
            private final ChannelHandler HANDLER = new ChannelHandler(
                    TCPHotspot.this, TCPHotspot.this.mapper);

            @Override
            public void initChannel(final SocketChannel ch) {
                final ChannelPipeline pipeline = ch.pipeline();
                pipeline.addLast(new LineBasedFrameDecoder(
                                Integer.parseInt((String) TCPHotspot.this.properties
                                .get(TCPHotspot.MAX_JSON_STRING_LENGTH_KEY))));
                pipeline.addLast(this.DECODER);
                pipeline.addLast(this.ENCODER);
                pipeline.addLast(this.HANDLER);
            }
        });
        this.serverBootstrap.bind(
                Integer.parseInt((String) TCPHotspot.this.properties
                        .get(TCPHotspot.TCP_PORT_KEY))).syncUninterruptibly();
        if (this.log.isInfoEnabled()) {
            this.log.info("Up & running with properties:");
            for (final Object k : this.properties.keySet()) {
                this.log.info("\t " + (String) k + "="
                        + this.properties.getProperty((String) k));
            }
        }
    }

    /**
     * Set Hotspot properties
     *
     * @param filePath
     *            Properties file path
     *
     */
    private void parseProperties(final String filePath) {
        this.properties = new Properties();
        this.properties.put(TCPHotspot.TCP_PORT_KEY, TCPHotspot.DEF_TCP_PORT);
        this.properties.put(TCPHotspot.MAX_JSON_STRING_LENGTH_KEY,
                TCPHotspot.DEF_MAX_JSON_STRING_LENGTH);
        final InputStream in = this.getClass().getClassLoader()
                .getResourceAsStream(filePath);
        if (in == null) {
            if (this.log.isWarnEnabled()) {
                this.log.warn("<hotspot.properties> file NOT found. Setting defaults.");
            }
        } else {
            try {
                this.properties.load(in);
            } catch (final IOException e) {
                if (this.log.isErrorEnabled()) {
                    this.log.error("Error while parsing <hotspot.properties> file. Setting defaults.");
                }
                e.printStackTrace();
            }
        }
    }

    /**
     * Sends notification of a joined/left compartment to an already connected
     * compartment
     *
     * @param compartmentToSend
     *            the joined/left compartment
     * @param compartmentToNotify
     *            the compartment to notify
     */
    private void sendCompartmentNotification(final boolean isLeaving,
            final ICompartment compartmentToSend,
            final ICompartment compartmentToNotify) {
        final GenericFutureListener<ChannelFuture> futureListener = future -> {
            // Connection refused
            if (!future.isSuccess() && !future.isCancelled()) {
                if (this.log.isWarnEnabled()) {
                    this.log.warn("Cannot connect to " + compartmentToNotify
                            + " due to " + future.cause());
                }
            }
            // If connected
            else {
                try {
                    final String json = TCPHotspot.this.mapper
                            .writeValueAsString(new TransientCompartment(
                                    isLeaving, compartmentToSend));
                    future.channel().writeAndFlush(json + "\n");
                    if (this.log.isInfoEnabled()) {
                        if (isLeaving) {
                            this.log.info("Sent leaving compartment: "
                                    + compartmentToSend + " to compartment: "
                                    + compartmentToNotify);
                        } else {
                            this.log.info("Sent joining compartment: "
                                    + compartmentToSend + " to compartment: "
                                    + compartmentToNotify);
                        }
                    }
                    future.channel().close();
                } catch (final JsonProcessingException e) {
                    e.printStackTrace();
                }
            }
        };
        final ChannelFuture connectionFuture = this.bootstrap
                .connect(compartmentToNotify.getAddress(),
                        compartmentToNotify.getPort());
        connectionFuture.addListener(futureListener);
    }

}
