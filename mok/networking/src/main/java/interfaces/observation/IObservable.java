package interfaces.observation;

/**
 *
 * Allows observer to be added.
 *
 *
 * @author Giacomo Dradi (giacomo.dradi@studio.unibo.it)
 *
 */
public interface IObservable {

    /**
     *
     * @param observer
     *            Observer to add
     */
    void addObserver(ICompartmentObserver observer);

}
