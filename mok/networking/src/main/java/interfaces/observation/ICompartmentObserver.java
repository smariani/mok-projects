package interfaces.observation;

import interfaces.model.ICompartment;
import interfaces.model.IMembrane;
import interfaces.model.payload.IAck;
import interfaces.model.payload.IDiffusingMolecule;
import interfaces.model.payload.IDiffusingMolecules;
import interfaces.model.payload.IDiffusingSearchReply;
import interfaces.model.payload.IDiffusingTrace;
import interfaces.model.payload.IDiffusingTraces;

/**
 *
 * Observer to add to a local compartment instance. It contains callback that
 * are called by different threads.
 *
 *
 * @author Giacomo Dradi (giacomo.dradi@studio.unibo.it)
 *
 */
public interface ICompartmentObserver {

    /**
     * Called when a single molecule diffusion ends.
     *
     * @param result
     *            {@code true} if diffusion succeeded, {@code false} otherwise
     * @param ack
     *            the ID of the data sent (i.e: molecule, molecule list)
     * @param membrane
     *            the membrane through which diffusion happened
     */
    void onDiffusionDone(boolean result, IAck ack, IMembrane membrane);

    /**
     * Called when a compartment join into MoK system() ends
     *
     * @param result
     *            {@code true} if at least one well-known server answered,
     *            {@code false} otherwise
     */
    void onJoinDone(boolean result);

    /**
     * @param succeeded
     */
    void onLeaveDone(boolean succeeded);

    /**
     * Called when a molecule is received
     *
     * @param diffusingMolecule
     *            the molecule received
     * @param membrane
     *            the membrane through which diffusion happened
     */
    void onMoleculeReceived(IDiffusingMolecule diffusingMolecule,
            IMembrane membrane);

    /**
     * Called when a list of molecules is recevied
     *
     * @param diffusingMolecules
     *            the molecules received
     * @param membrane
     *            the membrane through which diffusion happened
     */
    void onMoleculesReceived(IDiffusingMolecules diffusingMolecules,
            IMembrane membrane);

    /**
     * Called when a new compartment is received. If the new compartment was
     * already in the neighbourhood, it will replace the old one
     *
     * @param compartment
     *            the new compartment received
     */
    void onNewCompartmentReceived(ICompartment compartment);

    /**
     * @param compartment
     */
    void onOldCompartmentLeft(ICompartment compartment);

    /**
     * @param diffusingSearchReply
     * @param membrane
     */
    void onSearchReplyReceived(IDiffusingSearchReply diffusingSearchReply,
            IMembrane membrane);

    /**
     * Called when a trace is received
     *
     * @param diffusingTrace
     *            the trace received
     * @param membrane
     *            the trace through which diffusion happened
     */
    void onTraceReceived(IDiffusingTrace diffusingTrace, IMembrane membrane);

    /**
     * Called when a list of traces is recevied
     *
     * @param diffusingTraces
     *            the traces received
     * @param membrane
     *            the membrane through which diffusion happened
     */
    void onTracesReceived(IDiffusingTraces diffusingTraces, IMembrane membrane);

}
