package interfaces.model;

import java.util.List;

/**
 *
 * List of compartments sent by well-known server to just-connected MoK
 * compartment. These compartments belong to that compartment neighbourhood.
 *
 *
 * @author Giacomo Dradi (giacomo.dradi@studio.unibo.it)
 *
 */
public interface ICompartments {

    /**
     *
     * @return the remote compartments
     */
    List<ICompartment> getRemoteCompartments();

}
