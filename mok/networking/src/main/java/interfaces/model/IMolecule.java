package interfaces.model;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

/**
 *
 * MoK Molecule This is just a placeholder for a real MoK Molecule
 * implementation
 *
 *
 * @author Giacomo Dradi (giacomo.dradi@studio.unibo.it)
 *
 */
@JsonDeserialize(as = model.Molecule.class)
public interface IMolecule {

    /**
     *
     * @return the molecule current concentration
     */
    int getConcentration();

    /**
     *
     * @return Molecule's data (placeholder)
     */
    String getContent();

    /**
     *
     * @return the molecule similarity
     */
    double getSimilarity();

    /**
     *
     * Set the molecule's concentration
     * 
     * @param concentration
     *            the new concentration
     */
    void setConcentration(int concentration);
}
