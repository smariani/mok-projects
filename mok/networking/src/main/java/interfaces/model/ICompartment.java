package interfaces.model;

import java.util.List;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

/**
 *
 * MoK Compartment
 *
 *
 * @author Giacomo Dradi (giacomo.dradi@studio.unibo.it
 *
 */
@JsonDeserialize(as = model.Compartment.class)
public interface ICompartment extends IHost {

    /**
     *
     * @return the UNIQUE compartment ID
     */
    String getID();

    /**
     *
     * @return the compartment properties
     */
    List<IProperty<?>> getProperties();

}
