package interfaces.model;

import java.util.List;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

/**
 *
 * @author Luca Santonastasi (luca.santonastasi@studio.unibo.it)
 * @author Matteo Fattori (matteo.fattori5@studio.unibo.it)
 *
 */

@JsonDeserialize(as = model.Trace.class)
public interface ITrace {

    String getInfo();

    /**
     *
     * @return a list of properties
     */
    List<IProperty<?>> getProperties();

}
