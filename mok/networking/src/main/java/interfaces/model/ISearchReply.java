package interfaces.model;

import java.util.List;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

/**
 *
 * @author Luca Santonastasi (luca.santonastasi@studio.unibo.it)
 * @author Matteo Fattori (matteo.fattori5@studio.unibo.it)
 *
 *         data structure used for ResponseKnowledge(of communication project)
 */
@JsonDeserialize(as = model.SearchReply.class)
public interface ISearchReply {

    List<IMolecule> getMolecules();

    List<IProperty<?>> getProperties();
}
