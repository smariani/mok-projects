package interfaces.model;

import java.util.List;

/**
 *
 * A membrane is a 1-on-1 channel between the local MoK compartment and a remote
 * compartment. It has some membrane-related properties (i.e. diffusion rate,
 * speed)
 *
 *
 * @author Giacomo Dradi (giacomo.dradi@studio.unibo.it)
 *
 */
public interface IMembrane {

    /**
     * Delays diffusion based on distance value
     */
    void delayDiffusionBasedOnDistance();

    /**
     *
     * @return the membrane properties
     */
    List<IProperty<?>> getProperties();

    /**
     *
     * @return the membrane remote compartment
     */
    ICompartment getRemoteCompartment();

}
