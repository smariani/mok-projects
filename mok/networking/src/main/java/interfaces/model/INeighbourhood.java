package interfaces.model;

import java.util.List;

/**
 *
 * A compartment's neighbourhood is a list of membranes. The compartment can
 * diffuse molecules through them.
 *
 *
 * @author Giacomo Dradi (giacomo.dradi@studio.unibo.it)
 *
 */
public interface INeighbourhood {

    /**
     *
     * @return Membranes list
     */
    List<IMembrane> getMembranes();

}
