package interfaces.model;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

/**
 *
 * A generic property. It can be diffusion speed, rates, etc. Compartments and
 * membranes holds properties
 *
 *
 * @author Giacomo Dradi (giacomo.dradi@studio.unibo.it)
 * @param <T>
 *            Type of property
 *
 */

@JsonDeserialize(as = model.Property.class)
public interface IProperty<T> {

    /**
     *
     * @return the property value (placeholder)
     */
    T getValue();

}
