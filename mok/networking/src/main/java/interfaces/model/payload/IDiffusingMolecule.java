package interfaces.model.payload;

import interfaces.model.IMolecule;

/**
 *
 * MoK Molecule with ID
 *
 *
 * @author Giacomo Dradi (giacomo.dradi@studio.unibo.it)
 *
 */
public interface IDiffusingMolecule {

    /**
     *
     * @return the ID given to the diffusing molecule
     */
    String getID();

    /**
     *
     * @return the diffusing molecule
     */
    IMolecule getMolecule();
}
