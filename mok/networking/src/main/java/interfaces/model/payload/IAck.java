package interfaces.model.payload;

/**
 *
 * ACK sent back by compartments
 *
 *
 * @author Giacomo Dradi (giacomo.dradi@studio.unibo.it)
 *
 */
public interface IAck {

    /**
     *
     * @return the ack value
     */
    String getValue();

}
