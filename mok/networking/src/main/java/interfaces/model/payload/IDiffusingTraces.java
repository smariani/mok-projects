package interfaces.model.payload;

import interfaces.model.ITrace;
import java.util.List;

/**
 *
 * @author Luca Santonastasi (luca.santonastasi@studio.unibo.it)
 * @author Matteo Fattori (matteo.fattori5@studio.unibo.it)
 *
 */

public interface IDiffusingTraces {

    /**
     *
     * @return the ID given to the diffusing trace
     */
    String getID();

    /**
     *
     * @return a list of diffusing traces
     */
    List<ITrace> getTraces();

}
