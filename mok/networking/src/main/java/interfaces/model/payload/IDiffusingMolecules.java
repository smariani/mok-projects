package interfaces.model.payload;

import interfaces.model.IMolecule;
import java.util.List;

/**
 *
 * List of molecules to diffuse with LIST ID
 *
 *
 * @author Giacomo Dradi (giacomo.dradi@studio.unibo.it)
 *
 */
public interface IDiffusingMolecules {

    /**
     *
     * @return ID of molecule list
     */
    String getID();

    /**
     *
     * @return molecules list
     */
    List<IMolecule> getMolecules();

}
