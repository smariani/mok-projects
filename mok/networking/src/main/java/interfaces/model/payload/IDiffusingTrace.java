package interfaces.model.payload;

import interfaces.model.ITrace;

/**
 *
 * @author Luca Santonastasi (luca.santonastasi@studio.unibo.it)
 * @author Matteo Fattori (matteo.fattori5@studio.unibo.it)
 *
 */

public interface IDiffusingTrace {

    /**
     *
     * @return the ID given to the diffusing trace
     */
    String getID();

    /**
     *
     * @return the diffusing trace
     */
    ITrace getTrace();

}
