package interfaces.model.payload;

import interfaces.model.ISearchReply;

/**
 *
 * @author Luca Santonastasi (luca.santonastasi@studio.unibo.it)
 * @author Matteo Fattori (matteo.fattori5@studio.unibo.it)
 *
 */
public interface IDiffusingSearchReply {

    /**
     *
     * @return the ID given to the diffusing search reply
     */
    String getID();

    /**
     *
     * @return the trace of diffusing search reply
     */
    ISearchReply getSearchReply();
}
