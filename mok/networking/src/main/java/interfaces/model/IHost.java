package interfaces.model;

/**
 *
 * Generic Internet Host
 *
 *
 * @author Giacomo Dradi (giacomo.dradi@studio.unibo.it)
 *
 */
public interface IHost {

    /**
     *
     * @return the host IP address
     */
    String getAddress();

    /**
     *
     * @return the host TCP port
     */
    int getPort();

}
