package interfaces.peer.components;

import interfaces.model.IMembrane;
import interfaces.model.payload.IAck;
import interfaces.model.payload.IDiffusingMolecules;
import interfaces.model.payload.IDiffusingSearchReply;
import interfaces.model.payload.IDiffusingTraces;
import io.netty.channel.Channel;
import model.payload.DiffusingMoleculeWrapper;
import model.payload.DiffusingMoleculesWrapper;
import model.payload.DiffusingSearchReplyWrapper;
import model.payload.DiffusingTraceWrapper;
import model.payload.DiffusingTracesWrapper;

/**
 *
 * Subcomponent of a LocalCompartment. It establishes a Netty server to which
 * other compartments can connect and a Netty client if a diffusion to a new
 * compartment needs to be done.
 *
 * Membranes and Channel are coupled into connection. Already opened connection
 * are stored for future use.
 *
 *
 * @author Giacomo Dradi (giacomo.dradi@studio.unibo.it)
 *
 */
public interface IPeerHandler {

    /**
     * Diffuses a single search reply through a membrane.
     *
     * Check if the membrane is already connected (using the ConnectionPool
     * HashMap) - if so, goes on calling sendSearchReply - otherwise, try to
     * connect and listen to the connection future.
     *
     * If connection future fails, tells local compartment to fail. If succeed,
     * store the pair membrane-channel in the hashmap and goes on.
     *
     * @param diffusingSearchReply
     *            the search reply to diffuse
     * @param membrane
     *            the membrane through which to diffuse
     */
    public void diffuseSearchReply(IDiffusingSearchReply diffusingSearchReply,
            IMembrane membrane);

    /**
     * Called when an ack is received. An ACK is received from the remote
     * compartment when diffusion succeeded.
     *
     * Ack received Called by ChannelHandler
     *
     * Check ConnectionPool HashMap to know the source of the ACK. If the source
     * is in the current neighbourhood, notify the localCompartment
     *
     *
     * @param ack
     *            the ack received
     * @param channel
     *            the Netty channel used to receive
     */
    void ackReceived(IAck ack, Channel channel);

    /**
     * Diffuses a group of molecules through a membrane. Single molecule
     * diffusion is considered a special case where the group of molecules only
     * has one molecule
     *
     * Check if the membrane is already connected (using the ConnectionPool
     * HashMap) - if so, goes on calling sendMolecule - otherwise, try to
     * connect and listen to the connection future.
     *
     * If connection future fails, tells local compartment to fail. If succeed,
     * store the pair membrane-channel in the hashmap and goes on.
     *
     *
     * @param diffusingMolecules
     *            the molecules to diffuse
     * @param membrane
     *            the membrane through which to diffuse
     */
    void diffuseMolecules(IDiffusingMolecules diffusingMolecules,
            IMembrane membrane);

    /**
     * Diffuses a group of traces through a membrane. Single trace diffusion is
     * considered a special case where the group of traces only has one trace
     *
     * Check if the membrane is already connected (using the ConnectionPool
     * HashMap) - if so, goes on calling sendTrace - otherwise, try to connect
     * and listen to the connection future.
     *
     * If connection future fails, tells local compartment to fail. If succeed,
     * store the pair membrane-channel in the hashmap and goes on.
     *
     *
     * @param diffusingTraces
     *            the traces to diffuse
     * @param membrane
     *            the membrane through which to diffuse
     */
    void diffuseTraces(IDiffusingTraces diffusingTraces, IMembrane membrane);

    /**
     * Disposes the Netty instance
     */
    void dispose();

    /**
     * Creates a Netty server. This must be overwritten in the client subclass
     *
     * Initialize a TCP Netty server and client
     *
     * Messages are handled through TCP using a LineBasedFrameDecoder, i.e a
     * message is considered received and forwarded to channelHanndler after a
     * "\n".
     *
     */
    void init();

    /**
     * Called when a molecule is received
     *
     * Called by ChannelHandler when a molecule or a molecules list is received
     *
     * Check if source ID is valid, then send a MoleculeAck back using the same
     * channel.
     *
     * @param diffusingMolecule
     *            the diffused molecule
     * @param channel
     *            the Netty channel trhough which diffusion happened
     */
    void moleculeReceived(DiffusingMoleculeWrapper diffusingMolecule,
            Channel channel);

    /**
     * Called when molecules are received
     *
     * @param diffusingMolecules
     *            the diffused molecules
     * @param channel
     *            the Netty channel trhough which diffusion happened
     */
    void moleculesReceived(DiffusingMoleculesWrapper diffusingMolecules,
            Channel channel);

    /**
     * Called when a search reply is received
     *
     * @param diffusingSearchReply
     *            the data structure containing a trace and a molecule
     * @param channel
     *            the Netty channel trhough which diffusion happened
     */
    void searchReplyReceived(DiffusingSearchReplyWrapper diffusingSearchReply,
            Channel channel);

    /**
     * Called when a trace is received
     *
     * Called by ChannelHandler when a trace or a traces list is received
     *
     * Check if source ID is valid, then send a TraceAck back using the same
     * channel.
     *
     * @param diffusingTrace
     *            the diffused trace
     * @param channel
     *            the Netty channel trhough which diffusion happened
     */
    void traceReceived(DiffusingTraceWrapper diffusingTrace, Channel channel);

    /**
     * Called when traces are received
     *
     * @param diffusingTraces
     *            the diffused molecules
     * @param channel
     *            the Netty channel trhough which diffusion happened
     */
    void tracesReceived(DiffusingTracesWrapper diffusingTraces, Channel channel);

}
