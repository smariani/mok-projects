package interfaces.peer.components;

import interfaces.model.ICompartment;
import interfaces.model.IMembrane;
import interfaces.model.IMolecule;
import interfaces.model.INeighbourhood;
import interfaces.model.ISearchReply;
import interfaces.model.ITrace;
import interfaces.observation.IObservable;
import java.util.List;

/**
 *
 * Component used to interact with MoK Network. Uses a callback mechanism to
 * notify when operations ended.
 *
 * @author Giacomo Dradi (giacomo.dradi@studio.unibo.it)
 *
 */
public interface ILocalCompartment extends IObservable, ICompartment {

    /**
     * Diffuses a molecule to the whole neighbourhood
     *
     * @param diffusingMolecule
     *            molecule to diffuse
     */
    void broadcastMolecule(IMolecule diffusingMolecule);

    /**
     * Diffuses a group of molecules to the whole neighbourhood
     *
     * @param diffusingMolecules
     *            the molecules to diffuse
     */
    void broadcastMolecules(List<IMolecule> diffusingMolecules);

    /**
     * Diffuses a trace to the whole neighbourhood
     *
     * @param diffusingTrace
     *            trace to diffuse
     */
    void broadcastTrace(ITrace diffusingTrace);

    /**
     * Diffuses a group of trace to the whole neighbourhood
     *
     * @param diffusingTraces
     *            the traces to diffuse
     */
    void broadcastTraces(List<ITrace> diffusingTraces);

    /**
     * Diffuses a molecule through a single membrane. This is meaningful only
     * after joinMoK() completed successfully.
     *
     * @param diffusingMolecule
     *            the molecule to diffuse
     * @param membrane
     *            the membrane through which to diffuse
     */
    void diffuseMolecule(IMolecule diffusingMolecule, IMembrane membrane);

    /**
     * Diffuses a group of molecules through a single membrane. This is
     * meaningful only after joinMoK() completed successfully.
     *
     * @param diffusingMolecules
     *            the group of molecules to diffuse
     * @param membrane
     *            the membrane through which to diffuse
     */
    void diffuseMolecules(List<IMolecule> diffusingMolecules, IMembrane membrane);

    /**
     * Diffuses a search reply through a single membrane. This is meaningful
     * only after joinMoK() completed successfully.
     *
     * @param diffusingSearchReply
     *            the search reply to diffuse
     * @param membrane
     *            the membrane through which to diffuse
     */
    void diffuseSearchReply(ISearchReply diffusingSearchReply,
            IMembrane membrane);

    /**
     * Diffuses a trace through a single membrane. This is meaningful only after
     * joinMoK() completed successfully.
     *
     * @param diffusingTrace
     *            the trace to diffuse
     * @param membrane
     *            the membrane through which to diffuse
     */
    void diffuseTrace(ITrace diffusingTrace, IMembrane membrane);

    /**
     * Diffuses a group of traces through a single membrane. This is meaningful
     * only after joinMoK() completed successfully.
     *
     * @param diffusingTraces
     *            the group of traces to diffuse
     * @param membrane
     *            the membrane through which to diffuse
     */
    void diffuseTraces(List<ITrace> diffusingTraces, IMembrane membrane);

    /**
     *
     * @return the compartment
     */
    ICompartment getCompartment();

    /**
     * This is meaningful only after joinMoK() completed successfully.
     *
     * @return the current neighbourhood
     */
    INeighbourhood getNeighborhood();

    /**
     * This must be called first. When join is completed, it possible to diffuse
     * molecules through membranes to neighbourhood.
     */
    void joinMoK();

    /**
     * Leaves MoK
     */
    void leaveMoK();

    /**
     * Diffuses a molecule through multiple membranes. This is meaningful only
     * after joinMoK() completed successfully.
     *
     * @param diffusingMolecule
     *            the molecule to diffuse
     * @param membranes
     *            list of membranes through which to diffuse
     */
    void multicastMolecule(IMolecule diffusingMolecule,
            List<IMembrane> membranes);

    /**
     * Diffuses a group of molecules through multiple membranes. This is
     * meaningful only after joinMoK() completed successfully.
     *
     * @param diffusingMolecules
     *            molecules to diffuse
     * @param membranes
     *            list of membranes to use to diffuse
     */
    void multicastMolecules(List<IMolecule> diffusingMolecules,
            List<IMembrane> membranes);

    /**
     * Diffuses a trace through multiple membranes. This is meaningful only
     * after joinMoK() completed successfully.
     *
     * @param diffusingTrace
     *            the trace to diffuse
     * @param membranes
     *            list of membranes through which to diffuse
     */
    void multicastTrace(ITrace diffusingTrace, List<IMembrane> membranes);

    /**
     * Diffuses a group of traces through multiple membranes. This is meaningful
     * only after joinMoK() completed successfully.
     *
     * @param diffusingTraces
     *            traces to diffuse
     * @param membranes
     *            list of membranes to use to diffuse
     */
    void multicastTraces(List<ITrace> diffusingTraces, List<IMembrane> membranes);
}
