package interfaces.peer.components;

import interfaces.model.ICompartments;
import java.net.SocketAddress;

/**
 *
 * Subcomponent of a LocalCompartment. It communicates with well-known hotspots
 * by:
 *
 * 1) connecting 2) sending his own features (ID, port, address, property list)
 * 3) receiving back a list of compartment
 *
 * This is done for every hotspot. At the end, all compartments are merged to
 * prevent duplicates and are given to local compartment.
 *
 *
 * @author Giacomo Dradi (giacomo.dradi@studio.unibo.it)
 *
 */
public interface IHotspotHandler {

    /**
     * Called when a compartments list sent by an hotspot is received. Since
     * different hotspots are supposed to send a list of compartments, there
     * could be some duplicates. Using an HashMap to store compartments with
     * compartment's ID as key prevents duplicates.
     *
     * @param compartments
     *            the list of compartments received
     * @param remoteAddress
     *            the TCP/IP address of the hotspot
     */
    void compartmentsReceived(ICompartments compartments,
            SocketAddress remoteAddress);

    /**
     * Connects to hotspots. Start connection to all hotspots and listen to the
     * connection futures.
     *
     * Some integers are used to keep track of: - number of connection futures
     * ended. - number of successfully established connection - number of
     * hotspots that sent a list of compartments
     *
     */
    void connect();

    /**
     *
     */
    void disconnect();

    /**
     * Disposes the Netty client
     */
    void dispose();

    /**
     * Creates a Netty client
     */
    void init();

}
