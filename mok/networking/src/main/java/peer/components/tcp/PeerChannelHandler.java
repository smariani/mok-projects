package peer.components.tcp;

import interfaces.model.IMembrane;
import interfaces.peer.components.IPeerHandler;
import io.netty.channel.ChannelHandler.Sharable;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;
import java.io.IOException;
import model.payload.Ack;
import model.payload.DiffusingMoleculeWrapper;
import model.payload.DiffusingMoleculesWrapper;
import model.payload.DiffusingSearchReplyWrapper;
import model.payload.DiffusingTraceWrapper;
import model.payload.DiffusingTracesWrapper;
import model.payload.TransientCompartment;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import peer.components.LocalCompartment;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 *
 * Netty channel handler implementation As sharable, it must be thread-safe.
 *
 * Used by PeerHandler
 *
 *
 * @author Giacomo Dradi (giacomo.dradi@studio.unibo.it)
 *
 */
@Sharable
class PeerChannelHandler extends SimpleChannelInboundHandler<String> {

    static private Log log = LogFactory.getLog(PeerChannelHandler.class);
    private final LocalCompartment localCompartment;
    private final ObjectMapper mapper;
    private final IPeerHandler peerHandler;

    /**
     *
     * @param peerHandler
     * @param localCompartment
     * @param mapper
     */
    PeerChannelHandler(final IPeerHandler peerHandler,
            final LocalCompartment localCompartment, final ObjectMapper mapper) {
        this.peerHandler = peerHandler;
        this.mapper = mapper;
        this.localCompartment = localCompartment;
    }

    /**
     * Called when a new channel is created PeerHandler stores every new channel
     * and his related membrane in the ConnectionPool HashMap as soon as the
     * channel is created.
     *
     */
    @Override
    public void channelActive(final ChannelHandlerContext ctx) throws Exception {
        super.channelActive(ctx);
    }

    /**
     * When a channel is closed (i.e. leaveJoin(), disconnection) its entry in
     * the ConnectionPool HashMap is removed.
     *
     */
    @Override
    public void channelInactive(final ChannelHandlerContext ctx)
            throws Exception {
        IMembrane membrane = null;
        for (int i = 0; i < this.localCompartment.getNeighborhood()
                .getMembranes().size(); i++) {
            membrane = this.localCompartment.getNeighborhood().getMembranes()
                    .get(i);
            if (this.localCompartment.getConnectionPool().remove(membrane,
                    ctx.channel())) {
                // this.localCompartment.getNeighborhood().getMembranes()
                // .remove(i);
                if (PeerChannelHandler.log.isInfoEnabled()) {
                    PeerChannelHandler.log.info("Removed connection with: "
                            + membrane.getRemoteCompartment());
                }
                break;
            }
        }
        super.channelInactive(ctx);
    }

    /**
     * Called when a message is decoded (that is after a "\n") Try to
     * deserialize the JSON string in:
     *
     * - Ack (must be checked before molecule, otherwise JACKSON fails) -
     * SourcedMolecule - SourcedMoleculeList
     *
     * notify to PeerHandler about what it received (if deserialized rightfully)
     *
     */
    @Override
    public void channelRead0(final ChannelHandlerContext ctx, final String json) {

        // Decode ACK
        try {
            final Ack ack = this.mapper.readValue(json, Ack.class);
            this.peerHandler.ackReceived(ack, ctx.channel());
            return;
        } catch (final IOException e) {
            // Nothing
        }

        // Decode Molecule
        try {
            final DiffusingMoleculeWrapper molecule = this.mapper.readValue(
                    json, DiffusingMoleculeWrapper.class);
            this.peerHandler.moleculeReceived(molecule, ctx.channel());
            return;
        } catch (final IOException e) {
            // Nothing
        }

        // Decode MoleculeList
        try {
            final DiffusingMoleculesWrapper moleculeList = this.mapper
                    .readValue(json, DiffusingMoleculesWrapper.class);
            this.peerHandler.moleculesReceived(moleculeList, ctx.channel());
            return;
        } catch (final IOException e) {
            // Nothing
        }

        // Decode Trace
        try {
            final DiffusingTraceWrapper trace = this.mapper.readValue(json,
                    DiffusingTraceWrapper.class);
            this.peerHandler.traceReceived(trace, ctx.channel());
            return;
        } catch (final IOException e) {
            // Nothing
        }

        // Decode Traces
        try {
            final DiffusingTracesWrapper traces = this.mapper.readValue(json,
                    DiffusingTracesWrapper.class);
            this.peerHandler.tracesReceived(traces, ctx.channel());
            return;
        } catch (final IOException e) {
            // Nothing
        }

        // Decode Search Reply
        try {
            final DiffusingSearchReplyWrapper searchReply = this.mapper
                    .readValue(json, DiffusingSearchReplyWrapper.class);
            this.peerHandler.searchReplyReceived(searchReply, ctx.channel());
            return;
        } catch (final IOException e) {
            // Nothing
        }

        // Decode JSON to TransientCompartment
        try {
            final TransientCompartment tc = this.mapper.readValue(json,
                    TransientCompartment.class);
            this.localCompartment.compartmentReceived(tc, ctx.channel()
                    .remoteAddress());
            return;
        } catch (final IOException e) {
            // Nothing
        }

        /*
         * JSON failed to parse
         */
        if (PeerChannelHandler.log.isWarnEnabled()) {
            PeerChannelHandler.log.warn("Failed to parse json object: " + json);
        }
    }

    @Override
    public void exceptionCaught(final ChannelHandlerContext ctx,
            final Throwable cause) {
        // cause.printStackTrace();
        ctx.close();
    }

}
