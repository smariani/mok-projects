package peer.components.tcp;

import interfaces.model.IMembrane;
import interfaces.model.payload.IAck;
import interfaces.model.payload.IDiffusingMolecules;
import interfaces.model.payload.IDiffusingSearchReply;
import interfaces.model.payload.IDiffusingTraces;
import interfaces.peer.components.IPeerHandler;
import io.netty.bootstrap.Bootstrap;
import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.Channel;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelPipeline;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioServerSocketChannel;
import io.netty.channel.socket.nio.NioSocketChannel;
import io.netty.handler.codec.LineBasedFrameDecoder;
import io.netty.handler.codec.string.StringDecoder;
import io.netty.handler.codec.string.StringEncoder;
import io.netty.util.concurrent.GenericFutureListener;
import model.DiffusingMolecule;
import model.DiffusingMolecules;
import model.DiffusingSearchReply;
import model.DiffusingTrace;
import model.DiffusingTraces;
import model.payload.Ack;
import model.payload.DiffusingMoleculeWrapper;
import model.payload.DiffusingMoleculesWrapper;
import model.payload.DiffusingSearchReplyWrapper;
import model.payload.DiffusingTraceWrapper;
import model.payload.DiffusingTracesWrapper;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import peer.components.LocalCompartment;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 *
 * @author Giacomo Dradi (giacomo.dradi@studio.unibo.it)
 *
 */
public class PeerHandler implements IPeerHandler {

    private Bootstrap bootstrap;
    private final LocalCompartment localCompartment;
    private final Log log = LogFactory.getLog(PeerHandler.class);
    private final ObjectMapper mapper;
    private final int MAX_JSON_STRING_LENGTH;
    private NioEventLoopGroup nioGroup;
    private ServerBootstrap serverBootstrap;

    /**
     * Builds a peer (compartment)
     *
     * @param localCompartment
     *            the local MoK compartment this handler works for
     * @param mapper
     *            the JSON object mapper (Jackson library)
     * @param MAX_JSON_STRING_LENGTH
     *            maximum JSON string length
     */
    public PeerHandler(final LocalCompartment localCompartment,
            final ObjectMapper mapper, final int MAX_JSON_STRING_LENGTH) {
        this.localCompartment = localCompartment;
        this.mapper = mapper;
        this.MAX_JSON_STRING_LENGTH = MAX_JSON_STRING_LENGTH;
    }

    @Override
    public void ackReceived(final IAck ack, final Channel channel) {
        final IMembrane membrane = this.checkChannelValidity(channel);
        if (membrane == null) {
            if (this.log.isWarnEnabled()) {
                this.log.warn("Received " + ack + " from unknown host: "
                        + channel.remoteAddress() + ". Ignored.");
            }
        } else {
            this.localCompartment.onDiffusionEnded(true, ack, membrane);
        }
    }

    @Override
    public void diffuseMolecules(
            final IDiffusingMolecules diffusingMoleculeList,
            final IMembrane membrane) {
        final GenericFutureListener<ChannelFuture> futureListener = future -> {
            // Connection refused
            if (!future.isSuccess() && !future.isCancelled()) {
                if (this.log.isErrorEnabled()) {
                    this.log.error("Cannot connect to "
                            + membrane.getRemoteCompartment() + " due to "
                            + future.cause());
                }
                PeerHandler.this.localCompartment.onDiffusionEnded(false,
                        new Ack(diffusingMoleculeList.getID()), membrane);
            }
            // If connected
            else {
                if (PeerHandler.this.localCompartment.getConnectionPool()
                        .putIfAbsent(membrane, future.channel()) == null) {
                    if (this.log.isInfoEnabled()) {
                        this.log.info("Cached connection with: "
                                + membrane.getRemoteCompartment());
                    }
                }
                PeerHandler.this.sendMolecules(
                        (DiffusingMolecules) diffusingMoleculeList,
                        future.channel(), membrane);
            }
        };
        new Thread(
                () -> {
                    membrane.delayDiffusionBasedOnDistance();
                    final Channel channel = PeerHandler.this.localCompartment
                            .getConnectionPool().get(membrane);
                    if (channel == null) {
                        final ChannelFuture connectionFuture = PeerHandler.this.bootstrap.connect(
                                membrane.getRemoteCompartment().getAddress(),
                                membrane.getRemoteCompartment().getPort());
                        connectionFuture.addListener(futureListener);
                    } else {
                        PeerHandler.this.sendMolecules(
                                (DiffusingMolecules) diffusingMoleculeList,
                                channel, membrane);
                    }
                }, "Handler > diffuseMolecules()").start();
    }

    @Override
    public void diffuseSearchReply(
            final IDiffusingSearchReply diffusingSearchReply,
            final IMembrane membrane) {
        final GenericFutureListener<ChannelFuture> futureListener = future -> {
            // Connection refused
            if (!future.isSuccess() && !future.isCancelled()) {
                if (this.log.isErrorEnabled()) {
                    this.log.error("Cannot connect to "
                            + membrane.getRemoteCompartment() + " due to "
                            + future.cause());
                }
                PeerHandler.this.localCompartment.onDiffusionEnded(false,
                        new Ack(diffusingSearchReply.getID()), membrane);
            }
            // If connected
            else {
                if (PeerHandler.this.localCompartment.getConnectionPool()
                        .putIfAbsent(membrane, future.channel()) == null) {
                    if (this.log.isInfoEnabled()) {
                        this.log.info("Cached connection with: "
                                + membrane.getRemoteCompartment());
                    }
                }
                PeerHandler.this.sendSearchReply(
                        (DiffusingSearchReply) diffusingSearchReply,
                        future.channel(), membrane);
            }
        };
        new Thread(
                () -> {
                    membrane.delayDiffusionBasedOnDistance();
                    final Channel channel = PeerHandler.this.localCompartment
                            .getConnectionPool().get(membrane);
                    if (channel == null) {
                        final ChannelFuture connectionFuture = PeerHandler.this.bootstrap.connect(
                                membrane.getRemoteCompartment().getAddress(),
                                membrane.getRemoteCompartment().getPort());
                        connectionFuture.addListener(futureListener);
                    } else {
                        PeerHandler.this.sendSearchReply(
                                (DiffusingSearchReply) diffusingSearchReply,
                                channel, membrane);
                    }
                }, "Handler > diffuseMolecules()").start();

    }

    @Override
    public void diffuseTraces(final IDiffusingTraces diffusingTrace,
            final IMembrane membrane) {
        final GenericFutureListener<ChannelFuture> futureListener = future -> {
            // Connection refused
            if (!future.isSuccess() && !future.isCancelled()) {
                if (this.log.isErrorEnabled()) {
                    this.log.error("Cannot connect to "
                            + membrane.getRemoteCompartment() + " due to "
                            + future.cause());
                }
                PeerHandler.this.localCompartment.onDiffusionEnded(false,
                        new Ack(diffusingTrace.getID()), membrane);
            }
            // If connected
            else {
                if (PeerHandler.this.localCompartment.getConnectionPool()
                        .putIfAbsent(membrane, future.channel()) == null) {
                    if (this.log.isInfoEnabled()) {
                        this.log.info("Cached connection with: "
                                + membrane.getRemoteCompartment());
                    }
                }
                PeerHandler.this.sendTraces((DiffusingTraces) diffusingTrace,
                        future.channel(), membrane);
            }
        };
        new Thread(
                () -> {
                    membrane.delayDiffusionBasedOnDistance();
                    final Channel channel = PeerHandler.this.localCompartment
                            .getConnectionPool().get(membrane);
                    if (channel == null) {
                        final ChannelFuture connectionFuture = PeerHandler.this.bootstrap.connect(
                                membrane.getRemoteCompartment().getAddress(),
                                membrane.getRemoteCompartment().getPort());
                        connectionFuture.addListener(futureListener);
                    } else {
                        PeerHandler.this.sendTraces(
                                (DiffusingTraces) diffusingTrace, channel,
                                membrane);
                    }
                }, "Handler > diffuseTraces()").start();

    }

    @Override
    public void dispose() {
        this.nioGroup.shutdownGracefully();
        if (this.log.isInfoEnabled()) {
            this.log.info("Disposed");
        }
    }

    @Override
    public void init() {
        this.nioGroup = new NioEventLoopGroup();
        this.serverBootstrap = new ServerBootstrap();
        this.serverBootstrap.group(this.nioGroup)
        .channel(NioServerSocketChannel.class)
        .childHandler(new ChannelInitializer<SocketChannel>() {

            private final StringDecoder DECODER = new StringDecoder();
            private final StringEncoder ENCODER = new StringEncoder();
            private final PeerChannelHandler HANDLER = new PeerChannelHandler(
                    PeerHandler.this,
                    PeerHandler.this.localCompartment,
                    PeerHandler.this.mapper);

            @Override
            public void initChannel(final SocketChannel ch)
                    throws Exception {
                final ChannelPipeline p = ch.pipeline();
                p.addLast(new LineBasedFrameDecoder(
                        PeerHandler.this.MAX_JSON_STRING_LENGTH));
                p.addLast(this.DECODER);
                p.addLast(this.ENCODER);
                p.addLast(this.HANDLER);
            }
        });
        this.bootstrap = new Bootstrap();
        this.bootstrap.group(this.nioGroup).channel(NioSocketChannel.class)
        .handler(new ChannelInitializer<SocketChannel>() {

            private final StringDecoder DECODER = new StringDecoder();
            private final StringEncoder ENCODER = new StringEncoder();
            private final PeerChannelHandler HANDLER = new PeerChannelHandler(
                    PeerHandler.this,
                    PeerHandler.this.localCompartment,
                    PeerHandler.this.mapper);

            @Override
            public void initChannel(final SocketChannel ch) {
                final ChannelPipeline pipeline = ch.pipeline();
                pipeline.addLast(new LineBasedFrameDecoder(
                        PeerHandler.this.MAX_JSON_STRING_LENGTH));
                pipeline.addLast(this.DECODER);
                pipeline.addLast(this.ENCODER);
                pipeline.addLast(this.HANDLER);
            }
        });
        this.serverBootstrap.bind(this.localCompartment.getPort())
        .syncUninterruptibly();
        if (this.log.isInfoEnabled()) {
            this.log.info("Initialised");
        }
    }

    @Override
    public void moleculeReceived(
            final DiffusingMoleculeWrapper diffusingMolecule,
            final Channel channel) {
        final IMembrane membrane = this.checkRemoteIDValidity(
                diffusingMolecule.getSenderID(), channel);
        if (membrane == null) {
            if (this.log.isWarnEnabled()) {
                this.log.warn("Received " + diffusingMolecule
                        + " from unknown host: " + channel.remoteAddress()
                        + ". Ignored.");
            }
        } else {
            // Send ack back
            try {
                final String JSON = this.mapper.writeValueAsString(new Ack(
                        diffusingMolecule.getID()));
                channel.writeAndFlush(JSON + "\n");
                this.localCompartment.onMoleculeReceived(diffusingMolecule,
                        membrane);
            } catch (final JsonProcessingException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void moleculesReceived(
            final DiffusingMoleculesWrapper diffusingMolecules,
            final Channel channel) {
        final IMembrane membrane = this.checkRemoteIDValidity(
                diffusingMolecules.getSenderID(), channel);
        if (membrane == null) {
            if (this.log.isWarnEnabled()) {
                this.log.warn("Received molecules from unknown host: "
                        + channel.remoteAddress() + ". Ignored.");
            }
        } else {
            // Send ack back
            try {
                final String JSON = this.mapper.writeValueAsString(new Ack(
                        diffusingMolecules.getID()));
                channel.writeAndFlush(JSON + "\n");
                this.localCompartment.onMoleculesReceived(diffusingMolecules,
                        membrane);
            } catch (final JsonProcessingException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * this function handle the response data structure reception, the structure
     * contains a trace with the information about forwarding and a molecule
     * sended by a compartment to another one who requested that knowledge.
     */
    @Override
    public void searchReplyReceived(
            final DiffusingSearchReplyWrapper diffusingSearchReply,
            final Channel channel) {
        final IMembrane membrane = this.checkRemoteIDValidity(
                diffusingSearchReply.getSenderID(), channel);
        if (membrane == null) {
            if (this.log.isWarnEnabled()) {
                this.log.warn("Received " + diffusingSearchReply
                        + " from unknown host: " + channel.remoteAddress()
                        + ". Ignored.");
            }
        } else {
            // Send ack back
            try {
                final String JSON = this.mapper.writeValueAsString(new Ack(
                        diffusingSearchReply.getID()));
                channel.writeAndFlush(JSON + "\n");
                this.localCompartment.onSearchReplyReceived(
                        diffusingSearchReply, membrane);
            } catch (final JsonProcessingException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void traceReceived(final DiffusingTraceWrapper diffusingTrace,
            final Channel channel) {
        final IMembrane membrane = this.checkRemoteIDValidity(
                diffusingTrace.getSenderID(), channel);
        if (membrane == null) {
            if (this.log.isWarnEnabled()) {
                this.log.warn("Received " + diffusingTrace
                        + " from unknown host: " + channel.remoteAddress()
                        + ". Ignored.");
            }
        } else {
            // Send ack back
            try {
                final String JSON = this.mapper.writeValueAsString(new Ack(
                        diffusingTrace.getID()));
                channel.writeAndFlush(JSON + "\n");
                this.localCompartment.onTraceReceived(diffusingTrace, membrane);
            } catch (final JsonProcessingException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void tracesReceived(final DiffusingTracesWrapper diffusingTraces,
            final Channel channel) {
        final IMembrane membrane = this.checkRemoteIDValidity(
                diffusingTraces.getSenderID(), channel);
        if (membrane == null) {
            if (this.log.isWarnEnabled()) {
                this.log.warn("Received traces from unknown host: "
                        + channel.remoteAddress() + ". Ignored.");
            }
        } else {
            // Send ack back
            try {
                final String JSON = this.mapper.writeValueAsString(new Ack(
                        diffusingTraces.getID()));
                channel.writeAndFlush(JSON + "\n");
                this.localCompartment.onTracesReceived(diffusingTraces,
                        membrane);
            } catch (final JsonProcessingException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     *
     * @param channel
     * @return
     */
    private IMembrane checkChannelValidity(final Channel channel) {
        IMembrane membrane = null;
        for (int i = 0; i < this.localCompartment.getNeighborhood()
                .getMembranes().size(); i++) {
            membrane = this.localCompartment.getNeighborhood().getMembranes()
                    .get(i);
            if (this.localCompartment.getConnectionPool().get(membrane) == channel) {
                return membrane;
            }
        }
        return null;
    }

    /**
     * ConnectionPool HashMap cannot be check to know molecule source, because
     * this molecule could be sent from a just-created channel (i.e. first send
     * to a new compartment).
     *
     * If the channel is not found in the ConnectionPool, it is added with the
     * membrane of the compartment with the same ID as the source of the
     * molecule
     *
     */
    private IMembrane checkRemoteIDValidity(final String compartmentID,
            final Channel channel) {
        IMembrane membrane = null;
        for (int i = 0; i < this.localCompartment.getNeighborhood()
                .getMembranes().size(); i++) {
            membrane = this.localCompartment.getNeighborhood().getMembranes()
                    .get(i);
            if (membrane.getRemoteCompartment().getID().equals(compartmentID)) {
                if (this.localCompartment.getConnectionPool().putIfAbsent(
                        membrane, channel) == null) {
                    if (this.log.isInfoEnabled()) {
                        this.log.info("Cached connection with: "
                                + membrane.getRemoteCompartment());
                    }
                }
                return membrane;
            }
        }
        return null;
    }

    /**
     * Physically send the serialized molecule list as JSON string. This
     * compartment ID is added to molecule's data, so that if this is a new
     * connection, the receiver can know who sent it.
     *
     * @param diffusingMolecules
     *            molecule to send
     * @param channel
     *            channel to use
     * @param membrane
     *            membrane related to channel
     */
    private void sendMolecules(final DiffusingMolecules diffusingMolecules,
            final Channel channel, final IMembrane membrane) {
        // Single molecule
        if (diffusingMolecules.getMolecules().size() == 1) {
            try {
                final String moleculeJSON = this.mapper
                        .writeValueAsString(new DiffusingMoleculeWrapper(
                                this.localCompartment.getID(),
                                new DiffusingMolecule(diffusingMolecules
                                        .getID(), diffusingMolecules
                                        .getMolecules().get(0))));
                channel.writeAndFlush(moleculeJSON + "\n");
            } catch (final JsonProcessingException e) {
                e.printStackTrace();
                this.localCompartment.onDiffusionEnded(false, new Ack(
                        diffusingMolecules.getID()), membrane);
            }
        }
        // Molecules list
        else {
            try {
                final String moleculesJSON = this.mapper
                        .writeValueAsString(new DiffusingMoleculesWrapper(
                                this.localCompartment.getID(),
                                diffusingMolecules));
                channel.writeAndFlush(moleculesJSON + "\n");
            } catch (final JsonProcessingException e) {
                e.printStackTrace();
                this.localCompartment.onDiffusionEnded(false, new Ack(
                        diffusingMolecules.getID()), membrane);
            }
        }
    }

    /**
     * Physically send the serialized response as JSON string. This compartment
     * ID is added to trace's data, so that if this is a new connection, the
     * receiver can know who sent it.
     *
     * @param diffusingSearchReply
     *            search reply to send
     * @param channel
     *            channel to use
     * @param membrane
     *            membrane related to channel
     */
    private void sendSearchReply(
            final DiffusingSearchReply diffusingSearchReply,
            final Channel channel, final IMembrane membrane) {
        try {
            final String searchReplyJSON = this.mapper
                    .writeValueAsString(new DiffusingSearchReplyWrapper(
                            this.localCompartment.getID(),
                            new DiffusingSearchReply(diffusingSearchReply
                                    .getID(), diffusingSearchReply
                                    .getSearchReply())));
            channel.writeAndFlush(searchReplyJSON + "\n");
        } catch (final JsonProcessingException e) {
            e.printStackTrace();
            this.localCompartment.onDiffusionEnded(false, new Ack(
                    diffusingSearchReply.getID()), membrane);
        }
    }

    /**
     * Physically send the serialized trace list as JSON string. This
     * compartment ID is added to trace's data, so that if this is a new
     * connection, the receiver can know who sent it.
     *
     * @param diffusingTrace
     *            trace to send
     * @param channel
     *            channel to use
     * @param membrane
     *            membrane related to channel
     */
    private void sendTraces(final DiffusingTraces diffusingTraces,
            final Channel channel, final IMembrane membrane) {
        // Single trace
        if (diffusingTraces.getTraces().size() == 1) {
            try {
                final String traceJSON = this.mapper
                        .writeValueAsString(new DiffusingTraceWrapper(
                                this.localCompartment.getID(),
                                new DiffusingTrace(diffusingTraces.getID(),
                                        diffusingTraces.getTraces().get(0))));
                channel.writeAndFlush(traceJSON + "\n");
            } catch (final JsonProcessingException e) {
                e.printStackTrace();
                this.localCompartment.onDiffusionEnded(false, new Ack(
                        diffusingTraces.getID()), membrane);
            }
        }
        // Traces list
        else {
            try {
                final String tracesJSON = this.mapper
                        .writeValueAsString(new DiffusingTracesWrapper(
                                this.localCompartment.getID(), diffusingTraces));
                channel.writeAndFlush(tracesJSON + "\n");
            } catch (final JsonProcessingException e) {
                e.printStackTrace();
                this.localCompartment.onDiffusionEnded(false, new Ack(
                        diffusingTraces.getID()), membrane);
            }
        }
    }

}
