package peer.components.tcp;

import interfaces.peer.components.IHotspotHandler;
import io.netty.channel.ChannelHandler.Sharable;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;
import java.io.IOException;
import model.Compartments;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 *
 * Netty channel handler implementation As sharable, it must be thread-safe.
 *
 *
 * @author Giacomo Dradi (giacomo.dradi@studio.unibo.it)
 *
 */
@Sharable
class HotspotChannelHandler extends SimpleChannelInboundHandler<String> {

    private static final String TAG = "["
            + HotspotChannelHandler.class.getSimpleName() + "] ";
    private final IHotspotHandler hotspotHandler;
    private final ObjectMapper mapper;

    /**
     *
     * @param thisRef
     * @param mapper
     */
    HotspotChannelHandler(final IHotspotHandler thisRef,
            final ObjectMapper mapper) {
        this.hotspotHandler = thisRef;
        this.mapper = mapper;
    }

    /**
     * Called when a message is decoded (that is after a "\n") Deserialize JSON
     * string received to a CompartmentList instance and notify to
     * HotspotClient.
     *
     */
    @Override
    public void channelRead0(final ChannelHandlerContext ctx, final String json) {

        // Decode JSON to CompartmentList
        try {
            final Compartments compartmentList = this.mapper.readValue(json,
                    Compartments.class);
            this.hotspotHandler.compartmentsReceived(compartmentList, ctx
                    .channel().remoteAddress());
            return;
        } catch (final IOException e) {
            // Nothing
        }

        /*
         * JSON failed to parse
         */
        System.out.println(HotspotChannelHandler.TAG
                + "failed to parse json object: ");
        System.out.println(HotspotChannelHandler.TAG + json);
    }

    @Override
    public void exceptionCaught(final ChannelHandlerContext ctx,
            final Throwable cause) {
        // cause.printStackTrace();
        ctx.close();
    }

}
