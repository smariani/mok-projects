package peer.components.tcp;

import interfaces.model.ICompartment;
import interfaces.model.ICompartments;
import interfaces.model.IHost;
import interfaces.peer.components.IHotspotHandler;
import io.netty.bootstrap.Bootstrap;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelOption;
import io.netty.channel.ChannelPipeline;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioSocketChannel;
import io.netty.handler.codec.LineBasedFrameDecoder;
import io.netty.handler.codec.string.StringDecoder;
import io.netty.handler.codec.string.StringEncoder;
import io.netty.util.concurrent.GenericFutureListener;
import java.io.IOException;
import java.net.SocketAddress;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Stream;
import model.Host;
import model.payload.TransientCompartment;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import peer.components.LocalCompartment;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 *
 * @author Giacomo Dradi (giacomo.dradi@studio.unibo.it)
 *
 */
public class HotspotHandler implements IHotspotHandler {

    private Bootstrap bootstrap = new Bootstrap();
    private final int CONNECTION_TIMEOUT;
    private List<IHost> hotspots;
    private final String hotspotsFile;
    private final LocalCompartment localCompartment;
    private final Log log = LogFactory.getLog(HotspotHandler.class);
    private final ObjectMapper mapper;
    private final int MAX_JSON_STRING_LENGTH;
    private AtomicInteger nConnectionsDone;
    private AtomicInteger nHotspotsReplies;
    private NioEventLoopGroup nioGroup;
    private AtomicInteger nSuccessfulConnections;
    private ConcurrentHashMap<String, ICompartment> receivedCompartments;

    /**
     * Builds the handler of the well-known hotspots
     *
     * @param localCompartment
     *            the local MoK compartment this handler is working for
     * @param mapper
     *            the JSON object mapper (Jackson library)
     * @param hotspotsFile
     *            the path of the file hotspots.list
     * @param MAX_JSON_STRING_LENGTH
     *            maximum JSON string length
     * @param CONNECTION_TIMEOUT
     *            connection timeout in millis
     */
    public HotspotHandler(final LocalCompartment localCompartment,
            final ObjectMapper mapper, final String hotspotsFile,
            final int MAX_JSON_STRING_LENGTH, final int CONNECTION_TIMEOUT) {
        this.localCompartment = localCompartment;
        this.mapper = mapper;
        this.hotspotsFile = hotspotsFile;
        this.MAX_JSON_STRING_LENGTH = MAX_JSON_STRING_LENGTH;
        this.CONNECTION_TIMEOUT = CONNECTION_TIMEOUT;
    }

    @Override
    public void compartmentsReceived(final ICompartments compartments,
            final SocketAddress remoteAddress) {
        if (this.log.isInfoEnabled()) {
            this.log.info("#" + compartments.getRemoteCompartments().size()
                    + " compartment(s) received from hotspot: " + remoteAddress);
        }
        for (int i = 0; i < compartments.getRemoteCompartments().size(); i++) {
            this.receivedCompartments.putIfAbsent(compartments
                    .getRemoteCompartments().get(i).getID(), compartments
                    .getRemoteCompartments().get(i));
        }
        this.nHotspotsReplies.incrementAndGet();
        this.checkAllConnectionsDone();
    }

    @Override
    public void connect() {
        this.hotspots = this.parseHotspotsFile();
        if (this.hotspots == null) {
            if (this.log.isFatalEnabled()) {
                this.log.fatal("Wrong hotspots file path, failing join()..");
            }
            this.localCompartment.onJoinEnded(false, null);
            return;
        }
        this.nConnectionsDone = new AtomicInteger(0);
        this.nSuccessfulConnections = new AtomicInteger(0);
        this.nHotspotsReplies = new AtomicInteger(0);
        this.receivedCompartments = new ConcurrentHashMap<String, ICompartment>();
        final GenericFutureListener<ChannelFuture> futureListener = future -> {
            // FAILED
            if (!future.isSuccess() && !future.isCancelled()) {
                if (this.log.isErrorEnabled()) {
                    this.log.error("Cannot connect to hotspot due to: "
                            + future.cause());
                }
            }
            // SUCCEDED
            else {
                if (this.log.isInfoEnabled()) {
                    this.log.info("Connected to hotspot: "
                            + future.channel().remoteAddress());
                    this.log.info("Sending compartment: "
                            + HotspotHandler.this.localCompartment
                            .getCompartment());
                }
                final String json = HotspotHandler.this.mapper
                        .writeValueAsString(new TransientCompartment(false,
                                HotspotHandler.this.localCompartment
                                .getCompartment()));
                future.channel().writeAndFlush(json + "\n");
                HotspotHandler.this.nSuccessfulConnections.incrementAndGet();
            }
            HotspotHandler.this.nConnectionsDone.incrementAndGet();
            HotspotHandler.this.checkAllConnectionsDone();
        };
        // Start connecting to all servers
        for (int i = 0; i < this.hotspots.size(); i++) {
            this.bootstrap.connect(this.hotspots.get(i).getAddress(),
                    this.hotspots.get(i).getPort()).addListener(futureListener);
        }
        // Start timeout counter
        new Thread((Runnable) () -> {
            try {
                Thread.sleep(HotspotHandler.this.CONNECTION_TIMEOUT);
                if (HotspotHandler.this.nHotspotsReplies.get() > 0) {
                    return;
                }
                HotspotHandler.this.localCompartment.onJoinEnded(false, null);
            } catch (final InterruptedException e) {
                e.printStackTrace();
            }
        }, "Handler > CONNECTION_TIMEOUT").start();
    }

    /*
     * (non-Javadoc)
     * @see interfaces.peer.components.IHotspotHandler#disconnect()
     */
    @Override
    public void disconnect() {
        this.nConnectionsDone = new AtomicInteger(0);
        this.nSuccessfulConnections = new AtomicInteger(0);
        this.nHotspotsReplies = new AtomicInteger(0);
        // this.receivedCompartments = new ConcurrentHashMap<String,
        // ICompartment>();
        final GenericFutureListener<ChannelFuture> futureListener = future -> {
            // FAILED
            if (!future.isSuccess() && !future.isCancelled()) {
                if (this.log.isErrorEnabled()) {
                    this.log.error("Cannot connect to hotspot due to: "
                            + future.cause());
                }
            }
            // SUCCEDED
            else {
                if (this.log.isInfoEnabled()) {
                    this.log.info("Connected to hotspot: "
                            + future.channel().remoteAddress());
                    this.log.info("Sending compartment: "
                            + HotspotHandler.this.localCompartment
                            .getCompartment());
                }
                final String json = HotspotHandler.this.mapper
                        .writeValueAsString(new TransientCompartment(true,
                                HotspotHandler.this.localCompartment
                                .getCompartment()));
                future.channel().writeAndFlush(json + "\n");
                HotspotHandler.this.nSuccessfulConnections.incrementAndGet();
            }
            HotspotHandler.this.nConnectionsDone.incrementAndGet();
            HotspotHandler.this.checkAllDisconnectionsDone();
        };
        // Start connecting to all servers
        for (int i = 0; i < this.hotspots.size(); i++) {
            this.bootstrap.connect(this.hotspots.get(i).getAddress(),
                    this.hotspots.get(i).getPort()).addListener(futureListener);
        }
        // Start timeout counter
        new Thread((Runnable) () -> {
            try {
                Thread.sleep(HotspotHandler.this.CONNECTION_TIMEOUT);
                if (HotspotHandler.this.nHotspotsReplies.get() > 0) {
                    return;
                }
                HotspotHandler.this.localCompartment.onLeaveEnded(false);
            } catch (final InterruptedException e) {
                e.printStackTrace();
            }
        }, "Handler > DISCONNECTION_TIMEOUT").start();
    }

    /**
     * Destroy the Netty client
     */
    @Override
    public void dispose() {
        this.nioGroup.shutdownGracefully();
        if (this.log.isInfoEnabled()) {
            this.log.info("Disposed");
        }
    }

    /**
     * Initialize a TCP Netty client
     *
     * Messages are handled through TCP using a LineBasedFrameDecoder, i.e a
     * message is considered received and forwarded to channelHanndler after a
     * "\n".
     *
     */
    @Override
    public void init() {
        this.nioGroup = new NioEventLoopGroup();
        this.bootstrap = new Bootstrap();
        this.bootstrap
        .group(this.nioGroup)
        .channel(NioSocketChannel.class)
        .option(ChannelOption.CONNECT_TIMEOUT_MILLIS,
                this.CONNECTION_TIMEOUT)
                .handler(new ChannelInitializer<SocketChannel>() {

                    private final StringDecoder DECODER = new StringDecoder();
                    private final StringEncoder ENCODER = new StringEncoder();
                    private final HotspotChannelHandler HANDLER = new HotspotChannelHandler(
                            HotspotHandler.this, HotspotHandler.this.mapper);

                    @Override
                    public void initChannel(final SocketChannel ch) {
                        final ChannelPipeline pipeline = ch.pipeline();
                        pipeline.addLast(new LineBasedFrameDecoder(
                                HotspotHandler.this.MAX_JSON_STRING_LENGTH));
                        pipeline.addLast(this.DECODER);
                        pipeline.addLast(this.ENCODER);
                        pipeline.addLast(this.HANDLER);
                    }
                });
        if (this.log.isInfoEnabled()) {
            this.log.info("Initialised");
        }
    }

    /**
     * If no connection with hotspots were made, joining failed. If all
     * connected hotspots sent their list, joining succeded.
     *
     * Connection timeout must be set as property. After
     *
     * If one or more hotspot connected, but no list is received, the onJoin
     * callback will never be called.
     *
     */
    private void checkAllConnectionsDone() {
        if (this.nConnectionsDone.get() == this.hotspots.size()) {
            if (this.nSuccessfulConnections.get() == 0) {
                // No hotspots connected
                this.localCompartment.onJoinEnded(false, null);
            } else if (this.nHotspotsReplies.get() == this.nSuccessfulConnections
                    .get()) {
                // All connected hotspots answered
                this.localCompartment.onJoinEnded(true,
                        this.receivedCompartments);
            }
        }
    }

    private void checkAllDisconnectionsDone() {
        if (this.nConnectionsDone.get() == this.hotspots.size()) {
            if (this.nSuccessfulConnections.get() == 0) {
                // No hotspots connected
                this.localCompartment.onLeaveEnded(false);
            } else if (this.nHotspotsReplies.get() == this.nSuccessfulConnections
                    .get()) {
                // All connected hotspots answered
                this.localCompartment.onLeaveEnded(true);
            }
        }
    }

    /**
     * Parse hotspots form file
     *
     * @return hotspots list, or null if error
     */
    private List<IHost> parseHotspotsFile() {
        final List<IHost> hotspotsList = new ArrayList<IHost>();
        final Path path = Paths.get(this.hotspotsFile);
        try (Stream<String> lines = Files.lines(path)) {
            lines.forEach(s -> {
                try {
                    hotspotsList.add(this.mapper.readValue(s, Host.class));
                } catch (final Exception e) {
                    if (this.log.isErrorEnabled()) {
                        this.log.error("Cannot parse host: " + s);
                    }
                }
            });
            lines.close();
            return hotspotsList;
        } catch (final IOException ex) {
            return null;
        }
    }

}
