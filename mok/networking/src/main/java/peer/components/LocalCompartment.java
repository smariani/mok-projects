package peer.components;

import interfaces.model.ICompartment;
import interfaces.model.IMembrane;
import interfaces.model.IMolecule;
import interfaces.model.INeighbourhood;
import interfaces.model.ISearchReply;
import interfaces.model.ITrace;
import interfaces.model.Protocol;
import interfaces.model.payload.IAck;
import interfaces.model.payload.IDiffusingMolecule;
import interfaces.model.payload.IDiffusingMolecules;
import interfaces.model.payload.IDiffusingTrace;
import interfaces.model.payload.IDiffusingTraces;
import interfaces.observation.ICompartmentObserver;
import interfaces.peer.components.IHotspotHandler;
import interfaces.peer.components.ILocalCompartment;
import interfaces.peer.components.IPeerHandler;
import io.netty.channel.Channel;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.net.InetAddress;
import java.net.SocketAddress;
import java.net.UnknownHostException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicLong;
import model.Compartment;
import model.DiffusingMolecules;
import model.DiffusingSearchReply;
import model.DiffusingTraces;
import model.Membrane;
import model.Neighbourhood;
import model.payload.Ack;
import model.payload.TransientCompartment;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import peer.components.tcp.HotspotHandler;
import peer.components.tcp.PeerHandler;
import com.fasterxml.jackson.annotation.JsonAutoDetect.Visibility;
import com.fasterxml.jackson.annotation.PropertyAccessor;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 *
 * This LocalCompartment implementation is based on the EVENT-DRIVEN model.
 *
 * Thread-safety is granted by using thread-safe data structures in this
 * component and his subcomponents - HotspotHandler - PeerHandler
 *
 * Non-overwritten methods present in this object are called by subcomponents
 * above as callback. Then, according to observer pattern, they forward the
 * callback to all observers registered.
 *
 * If running multple LocalCompartment from the same machine, they must be run
 * from different directories.
 *
 * Configuration files must be in the same directory of every LocalCompartment.
 *
 *
 * @author Giacomo Dradi (giacomo.dradi@studio.unibo.it)
 *
 */
public class LocalCompartment extends Compartment implements ILocalCompartment {

    public static final String DEF_JOIN_TIMEOUT = "5000";
    public static final String DEF_MAX_JSON_STRING_LENGTH = "100000";
    public static final String JOIN_TIMEOUT_KEY = "JOIN_TIMEOUT";
    public static final String MAX_JSON_STRING_LENGTH_KEY = "MAX_JSON_STRING_LENGTH";

    /**
     * Generate a new UUID or restore previous one
     *
     * @return LocalCompartment UUID
     */
    private static String getUUID() {
        final Path path = Paths.get("./uuid.txt");
        try {
            final String UUID = Files.readAllLines(path).get(0);
            if (UUID != null) {
                return UUID;
            }
        } catch (final IOException e) {
            /*
             * Go on generating the UUID..
             */
        }
        final String newUUID = UUID.randomUUID().toString();
        PrintWriter writer;
        try {
            writer = new PrintWriter("./uuid.txt", "UTF-8");
            writer.println(newUUID);
            writer.close();
        } catch (final Exception e) {
            e.printStackTrace();
        }
        return newUUID;
    }

    private ConcurrentMap<IMembrane, Channel> connectionsPool;

    private final AtomicLong diffCounter;

    private IHotspotHandler hotspotHandler;
    private final AtomicBoolean joinStarted = new AtomicBoolean(false);
    private final AtomicBoolean joinSucceeded = new AtomicBoolean(false);
    private final Log log = LogFactory.getLog(LocalCompartment.class);
    private INeighbourhood neighborhood;
    private final List<ICompartmentObserver> observers = new ArrayList<ICompartmentObserver>();
    private IPeerHandler peerHandler;
    private Properties properties;

    /**
     * Creates a local MoK compartment
     *
     * @param protocol
     *            the network protocol to be used
     * @param port
     *            the TCP port where the compartment is reachable
     * @param peerPropertiesFile
     *            the file where to parse peer properties from
     * @param hotspotsFile
     *            the file to parse hotspots properties from (e.g., their
     *            well-known TCP/IP address)
     * @param compartmentFile
     *            the file where to parse compartment properties from
     * @throws UnknownHostException
     *             if the given hotspot address is not valid
     */
    public LocalCompartment(final Protocol protocol, final int port,
            final String peerPropertiesFile, final String hotspotsFile,
            final String compartmentFile) throws UnknownHostException {
        super(LocalCompartment.getUUID(), null, -1, compartmentFile);
        this.diffCounter = new AtomicLong(0);
        final ObjectMapper mapper = new ObjectMapper().setVisibility(
                PropertyAccessor.FIELD, Visibility.ANY);
        this.parseProperties(peerPropertiesFile);
        if (protocol == Protocol.TCP_IP) {
            super.address = InetAddress.getLocalHost().getHostAddress();
            super.port = port;
            this.connectionsPool = new ConcurrentHashMap<IMembrane, Channel>();
            this.hotspotHandler = new HotspotHandler(this, mapper,
                    hotspotsFile, Integer.parseInt((String) this.properties
                            .get(LocalCompartment.MAX_JSON_STRING_LENGTH_KEY)),
                    Integer.parseInt((String) this.properties
                            .get(LocalCompartment.JOIN_TIMEOUT_KEY)));
            this.peerHandler = new PeerHandler(this, mapper,
                    Integer.parseInt((String) this.properties
                            .get(LocalCompartment.MAX_JSON_STRING_LENGTH_KEY)));
        }
    }

    @Override
    public void addObserver(final ICompartmentObserver observer) {
        this.observers.add(observer);
    }

    @Override
    public void broadcastMolecule(final IMolecule molecule) {
        if (!this.joinSucceeded.get()) {
            final String id = new StringBuffer(this.getID()).append("_D_")
                    .append(this.diffCounter.getAndIncrement()).toString();
            this.onDiffusionEnded(false, new Ack(id), null);
            return;
        }
        final List<IMembrane> membranes = new ArrayList<IMembrane>();
        for (int i = 0; i < this.getNeighborhood().getMembranes().size(); i++) {
            membranes.add(this.getNeighborhood().getMembranes().get(i));
        }
        this.multicastMolecule(molecule, membranes);
    }

    @Override
    public void broadcastMolecules(final List<IMolecule> molecules) {
        if (!this.joinSucceeded.get()) {
            final String id = new StringBuffer(this.getID()).append("_D_")
                    .append(this.diffCounter.getAndIncrement()).toString();
            this.onDiffusionEnded(false, new Ack(id), null);
            return;
        }
        final List<IMembrane> membranes = new ArrayList<IMembrane>();
        for (int i = 0; i < this.getNeighborhood().getMembranes().size(); i++) {
            membranes.add(this.getNeighborhood().getMembranes().get(i));
        }
        this.multicastMolecules(molecules, membranes);
    }

    @Override
    public void broadcastTrace(final ITrace diffusingTrace) {
        if (!this.joinSucceeded.get()) {
            final String id = new StringBuffer(this.getID()).append("_D_")
                    .append(this.diffCounter.getAndIncrement()).toString();
            this.onDiffusionEnded(false, new Ack(id), null);
            return;
        }
        final List<IMembrane> membranes = new ArrayList<IMembrane>();
        for (int i = 0; i < this.getNeighborhood().getMembranes().size(); i++) {
            membranes.add(this.getNeighborhood().getMembranes().get(i));
        }
        this.multicastTrace(diffusingTrace, membranes);
    }

    @Override
    public void broadcastTraces(final List<ITrace> diffusingTraces) {
        if (!this.joinSucceeded.get()) {
            final String id = new StringBuffer(this.getID()).append("_D_")
                    .append(this.diffCounter.getAndIncrement()).toString();
            this.onDiffusionEnded(false, new Ack(id), null);
            return;
        }
        final List<IMembrane> membranes = new ArrayList<IMembrane>();
        for (int i = 0; i < this.getNeighborhood().getMembranes().size(); i++) {
            membranes.add(this.getNeighborhood().getMembranes().get(i));
        }
        this.multicastTraces(diffusingTraces, membranes);

    }

    /**
     * Called when a compartment notification is received by an hotspot. This
     * handle notifications of new compartments connected to neighbourhood and
     * old compartments leaving MoK
     *
     * @param tc
     *            the compartment who joined/left the neighbourhood
     * @param remoteAddress
     *            the sender hotspot that notified
     */
    public void compartmentReceived(final TransientCompartment tc,
            final SocketAddress remoteAddress) {
        synchronized (this.neighborhood) {
            for (int i = 0; i < this.neighborhood.getMembranes().size(); i++) {
                final ICompartment rc = this.neighborhood.getMembranes().get(i)
                        .getRemoteCompartment();
                if (this.log.isDebugEnabled()) {
                    this.log.debug("Known neighbour : " + i + " is: " + rc
                            + " (ID = " + rc.getID() + ")");
                }
                if (rc.getID().equals(tc.getCompartment().getID())) {
                    final IMembrane m;
                    m = this.neighborhood.getMembranes().remove(i);
                    if (this.log.isInfoEnabled()) {
                        this.log.info("Removed old membrane: " + m);
                    }
                    break;
                }
            }
            final IMembrane membrane;
            if (!tc.isLeaving()) {
                membrane = new Membrane(tc.getCompartment());
                this.neighborhood.getMembranes().add(membrane);
                if (this.log.isInfoEnabled()) {
                    this.log.info("Added membrane: " + membrane);
                }
            }
        }
        if (!tc.isLeaving()) {
            new Thread(
                    () -> {
                        for (int i = 0; i < LocalCompartment.this.observers.size(); i++) {
                            LocalCompartment.this.observers.get(i)
                                    .onNewCompartmentReceived(
                                            tc.getCompartment());
                        }
                    }, "Handler > onNewCompartmentReceived()").start();
        } else {
            new Thread(
                    () -> {
                        for (int i = 0; i < LocalCompartment.this.observers.size(); i++) {
                            LocalCompartment.this.observers.get(i)
                                    .onOldCompartmentLeft(tc.getCompartment());
                        }
                    }, "Handler > onOldCompartmentLeft()").start();
        }
    }

    @Override
    public void diffuseMolecule(final IMolecule molecule,
            final IMembrane membrane) {
        final String id = new StringBuffer(this.getID()).append("_D_")
                .append(this.diffCounter.getAndIncrement()).toString();
        if (!this.joinSucceeded.get()) {
            this.onDiffusionEnded(false, new Ack(id), membrane);
            return;
        }
        final List<IMolecule> molecules = new ArrayList<IMolecule>();
        molecules.add(molecule);
        LocalCompartment.this.peerHandler.diffuseMolecules(
                new DiffusingMolecules(id, molecules), membrane);
    }

    @Override
    public void diffuseMolecules(final List<IMolecule> molecules,
            final IMembrane membrane) {
        final String id = new StringBuffer(this.getID()).append("_D_")
                .append(this.diffCounter.getAndIncrement()).toString();
        final IDiffusingMolecules dms = new DiffusingMolecules(id, molecules);
        if (!this.joinSucceeded.get()) {
            this.onDiffusionEnded(false, new Ack(dms.getID()), membrane);
            return;
        }
        this.peerHandler.diffuseMolecules(dms, membrane);
    }

    @Override
    public void diffuseSearchReply(final ISearchReply diffusingSearchReply,
            final IMembrane membrane) {
        final String id = new StringBuffer(this.getID()).append("_D_")
                .append(this.diffCounter.getAndIncrement()).toString();
        if (!this.joinSucceeded.get()) {
            this.onDiffusionEnded(false, new Ack(id), membrane);
            return;
        }
        LocalCompartment.this.peerHandler.diffuseSearchReply(
                new DiffusingSearchReply(id, diffusingSearchReply), membrane);
    }

    @Override
    public void diffuseTrace(final ITrace diffusingTrace,
            final IMembrane membrane) {
        final String id = new StringBuffer(this.getID()).append("_D_")
                .append(this.diffCounter.getAndIncrement()).toString();
        if (!this.joinSucceeded.get()) {
            this.onDiffusionEnded(false, new Ack(id), membrane);
            return;
        }
        final List<ITrace> traces = new ArrayList<ITrace>();
        traces.add(diffusingTrace);
        LocalCompartment.this.peerHandler.diffuseTraces(new DiffusingTraces(id,
                traces), membrane);

    }

    @Override
    public void diffuseTraces(final List<ITrace> diffusingTraces,
            final IMembrane membrane) {
        final String id = new StringBuffer(this.getID()).append("_D_")
                .append(this.diffCounter.getAndIncrement()).toString();
        final IDiffusingTraces dms = new DiffusingTraces(id, diffusingTraces);
        if (!this.joinSucceeded.get()) {
            this.onDiffusionEnded(false, new Ack(dms.getID()), membrane);
            return;
        }
        this.peerHandler.diffuseTraces(dms, membrane);

    }

    @Override
    public ICompartment getCompartment() {
        return new Compartment(this.getID(), this.address, this.port,
                super.getProperties());
    }

    /**
     * Gets the pool of active connections
     *
     * @return the pool of active connections
     */
    public ConcurrentMap<IMembrane, Channel> getConnectionPool() {
        return this.connectionsPool;
    }

    /**
     * Gets the hotspots handler
     *
     * @return the hotspots handler
     */
    public IHotspotHandler getHotspotHandler() {
        return this.hotspotHandler;
    }

    @Override
    public INeighbourhood getNeighborhood() {
        return this.neighborhood;
    }

    @Override
    public void joinMoK() {
        if (this.joinStarted.compareAndSet(false, true)) {
            this.hotspotHandler.init();
            this.peerHandler.init();
            this.hotspotHandler.connect();
        }
    }

    @Override
    public void leaveMoK() {
        /*
         * TODO Notify hotspots
         */
        if (this.joinStarted.get()) {
            this.hotspotHandler.disconnect();
            // this.hotspotHandler.dispose();
            this.peerHandler.dispose();
            this.joinStarted.set(false);
            this.joinSucceeded.set(false);
        }
    }

    @Override
    public void multicastMolecule(final IMolecule molecule,
            final List<IMembrane> membranes) {
        for (int i = 0; i < membranes.size(); i++) {
            this.diffuseMolecule(molecule, membranes.get(i));
        }
    }

    @Override
    public void multicastMolecules(final List<IMolecule> molecules,
            final List<IMembrane> membranes) {
        for (int i = 0; i < membranes.size(); i++) {
            this.diffuseMolecules(molecules, membranes.get(i));
        }
    }

    @Override
    public void multicastTrace(final ITrace diffusingTrace,
            final List<IMembrane> membranes) {
        for (int i = 0; i < membranes.size(); i++) {
            this.diffuseTrace(diffusingTrace, membranes.get(i));
        }

    }

    @Override
    public void multicastTraces(final List<ITrace> diffusingTraces,
            final List<IMembrane> membranes) {
        for (int i = 0; i < membranes.size(); i++) {
            this.diffuseTraces(diffusingTraces, membranes.get(i));
        }

    }

    /**
     * Called by subcomponents when a diffusion has ended. It forwards the
     * result to registered observers.
     *
     * @param succeeded
     *            whether diffusion succeded
     * @param ack
     *            the ID of the entity diffused
     * @param membrane
     *            the membrane through which diffusion took place
     */
    public void onDiffusionEnded(final boolean succeeded, final IAck ack,
            final IMembrane membrane) {
        new Thread(() -> {
            for (int i = 0; i < LocalCompartment.this.observers.size(); i++) {
                LocalCompartment.this.observers.get(i).onDiffusionDone(
                        succeeded, ack, membrane);
            }
        }, "Handler > onDiffusionEnded()").start();
    }

    /**
     * Called by subcomponents when join ended. It forwards the result to
     * registered observers.
     *
     * If joining failed, leaveMoK() is called, otherwise only HotspotClient is
     * destroyed.
     *
     * @param succeeded
     *            joining result
     * @param compartmentMap
     *            map of compartments (key is compartment ID)
     */
    public void onJoinEnded(final boolean succeeded,
            final ConcurrentMap<String, ICompartment> compartmentMap) {
        if (!this.joinStarted.get()) {
            return;
        }
        if (succeeded) {
            final List<IMembrane> membranes = new CopyOnWriteArrayList<IMembrane>();
            for (final Map.Entry<String, ICompartment> entry : compartmentMap
                    .entrySet()) {
                final ICompartment compartment = entry.getValue();
                membranes.add(new Membrane(compartment));
            }
            this.neighborhood = new Neighbourhood(membranes);
            if (this.log.isInfoEnabled()) {
                this.log.info("Created #" + membranes.size() + " membrane(s)");
            }
            // this.hotspotHandler.dispose();
            this.joinSucceeded.set(true);
        } else {
            this.leaveMoK();
        }
        new Thread(() -> {
            for (int i = 0; i < LocalCompartment.this.observers.size(); i++) {
                LocalCompartment.this.observers.get(i).onJoinDone(succeeded);
            }
        }, "Handler > onJoinEnded()").start();
    }

    /**
     * @param succeeded
     *            leaving result
     */
    public void onLeaveEnded(final boolean succeeded) {
        this.hotspotHandler.dispose();
        new Thread(() -> {
            for (int i = 0; i < LocalCompartment.this.observers.size(); i++) {
                LocalCompartment.this.observers.get(i).onLeaveDone(succeeded);
            }
        }, "Handler > onLeaveEnded()").start();
    }

    /**
     * Called by subcomponents when a molecule is received. It forwards the
     * result to registered observers.
     *
     * @param diffusingMolecule
     *            the molecule received
     * @param membrane
     *            the membrane through which diffusion happened
     */
    public void onMoleculeReceived(final IDiffusingMolecule diffusingMolecule,
            final IMembrane membrane) {
        new Thread(() -> {
            for (int i = 0; i < LocalCompartment.this.observers.size(); i++) {
                LocalCompartment.this.observers.get(i).onMoleculeReceived(
                        diffusingMolecule, membrane);
            }
        }, "Handler > onMoleculeReceived()").start();
    }

    /**
     * Called by sub-components when molecules are received. It forwards the
     * result to registered observers.
     *
     * @param diffusingMolecules
     *            the molecules received
     * @param membrane
     *            the membrane through which diffusion happened
     */
    public void onMoleculesReceived(
            final IDiffusingMolecules diffusingMolecules,
            final IMembrane membrane) {
        new Thread(() -> {
            for (int i = 0; i < LocalCompartment.this.observers.size(); i++) {
                LocalCompartment.this.observers.get(i).onMoleculesReceived(
                        diffusingMolecules, membrane);
            }
        }, "Handler > onMoleculesReceived()").start();
    }

    public void onSearchReplyReceived(
            final DiffusingSearchReply diffusingSearchReply,
            final IMembrane membrane) {
        new Thread(() -> {
            for (int i = 0; i < LocalCompartment.this.observers.size(); i++) {
                LocalCompartment.this.observers.get(i).onSearchReplyReceived(
                        diffusingSearchReply, membrane);
            }
        }, "Handler > onSearchReplyReceived()").start();
    }

    /**
     * Called by subcomponents when a trace is received. It forwards the result
     * to registered observers.
     *
     * @param diffusingTrace
     *            the trace received
     * @param membrane
     *            the membrane through which diffusion happened
     */
    public void onTraceReceived(final IDiffusingTrace diffusingTrace,
            final IMembrane membrane) {
        new Thread(() -> {
            for (int i = 0; i < LocalCompartment.this.observers.size(); i++) {
                LocalCompartment.this.observers.get(i).onTraceReceived(
                        diffusingTrace, membrane);
            }
        }, "Handler > onTraceReceived()").start();
    }

    /**
     * Called by sub-components when traces are received. It forwards the result
     * to registered observers.
     *
     * @param diffusingTraces
     *            the traces received
     * @param membrane
     *            the membrane through which diffusion happened
     */
    public void onTracesReceived(final IDiffusingTraces diffusingTraces,
            final IMembrane membrane) {
        new Thread(() -> {
            for (int i = 0; i < LocalCompartment.this.observers.size(); i++) {
                LocalCompartment.this.observers.get(i).onTracesReceived(
                        diffusingTraces, membrane);
            }
        }, "Handler > onTracesReceived()").start();
    }

    private void parseProperties(final String filePath) {
        this.properties = new Properties();
        this.properties.put(LocalCompartment.JOIN_TIMEOUT_KEY,
                LocalCompartment.DEF_JOIN_TIMEOUT);
        this.properties.put(LocalCompartment.MAX_JSON_STRING_LENGTH_KEY,
                LocalCompartment.DEF_MAX_JSON_STRING_LENGTH);
        final InputStream in = this.getClass().getClassLoader()
                .getResourceAsStream(filePath);
        if (in == null) {
            if (this.log.isWarnEnabled()) {
                this.log.warn("<peer.properties> file NOT found. Setting defaults.");
            }
        } else {
            try {
                this.properties.load(in);
            } catch (final IOException e) {
                if (this.log.isErrorEnabled()) {
                    this.log.error("Error while parsing <peer.properties> file. Setting defaults.");
                }
                e.printStackTrace();
            }
        }
    }

}
