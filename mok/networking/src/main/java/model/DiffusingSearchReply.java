package model;

import interfaces.model.ISearchReply;
import interfaces.model.payload.IDiffusingSearchReply;

/**
 *
 * @author Luca Santonastasi (luca.santonastasi@studio.unibo.it)
 * @author Matteo Fattori (matteo.fattori5@studio.unibo.it)
 *
 */
public class DiffusingSearchReply implements IDiffusingSearchReply {

    private String id;
    private ISearchReply searchReply;

    /**
     * Empty constructor needed by Java JSON library (Jackson)
     */
    public DiffusingSearchReply() {
    }

    public DiffusingSearchReply(final String id, final ISearchReply sr) {
        this.id = id;
        this.searchReply = sr;
    }

    @Override
    public String getID() {
        return this.id;
    }

    @Override
    public ISearchReply getSearchReply() {
        return this.searchReply;
    }
}
