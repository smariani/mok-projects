package model;

import interfaces.model.ICompartment;
import interfaces.model.IMembrane;
import interfaces.model.IProperty;
import java.util.ArrayList;

/**
 *
 * @author Giacomo Dradi (giacomo.dradi@studio.unibo.it)
 *
 */
public class Membrane implements IMembrane {

    private final ArrayList<IProperty<?>> propertyList;
    private final ICompartment remoteCompartment;

    public Membrane(final ICompartment remoteCompartment) {
        this.remoteCompartment = remoteCompartment;
        this.propertyList = new ArrayList<>();
        this.defineMembraneProperties();
    }

    @Override
    public void delayDiffusionBasedOnDistance() {
        // TODO Membrane.delayDiffusionBasedOnDistance
        final int delayMillis = ((Distance) this.propertyList.get(0))
                .getValue() * 1000;
        try {
            Thread.sleep(delayMillis);
        } catch (final InterruptedException e) {
            e.printStackTrace();
        }
    }

    @Override
    public ArrayList<IProperty<?>> getProperties() {
        return this.propertyList;
    }

    @Override
    public ICompartment getRemoteCompartment() {
        return this.remoteCompartment;
    }

    @Override
    public String toString() {
        return "Membrane { " + this.remoteCompartment + " }";
    }

    private void defineMembraneProperties() {
        /*
         * TODO Algorithm to calculate distance and other membrane properties,
         * using localCompartment properties list and remoteCompartment
         * properties list. Suppose FIRST property is distance (rate
         * approximation)
         */
        this.propertyList.add(new Distance(1));
    }

}
