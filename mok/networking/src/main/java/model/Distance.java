package model;

import interfaces.model.IProperty;

/**
 *
 * @author Giacomo Dradi (giacomo.dradi@studio.unibo.it)
 *
 */
public class Distance implements IProperty<Integer> {

    private final int value;

    /**
     *
     * @param value
     *            the integer distance value
     */
    public Distance(final int value) {
        this.value = value;
    }

    @Override
    public Integer getValue() {
        return this.value;
    }

}
