package model;

import interfaces.model.ICompartment;
import interfaces.model.ICompartments;
import java.util.List;

/**
 *
 * @author Giacomo Dradi (giacomo.dradi@studio.unibo.it)
 *
 */
public class Compartments implements ICompartments {

    private List<ICompartment> remoteCompartments;

    /**
     * Empty constructor needed by Java JSON library (Jackson)
     */
    public Compartments() {
    }

    /**
     *
     * @param remoteCompartments
     *            the remote compartments
     */
    public Compartments(final List<ICompartment> remoteCompartments) {
        this.remoteCompartments = remoteCompartments;
    }

    @Override
    public List<ICompartment> getRemoteCompartments() {
        return this.remoteCompartments;
    }

}
