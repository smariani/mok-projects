package model;

import interfaces.model.IMolecule;
import interfaces.model.IProperty;
import interfaces.model.ISearchReply;
import java.util.List;

/**
 *
 * @author Luca Santonastasi (luca.santonastasi@studio.unibo.it)
 * @author Matteo Fattori (matteo.fattori5@studio.unibo.it)
 *
 */
public class SearchReply implements ISearchReply {

    private List<IMolecule> molecules;
    private List<IProperty<?>> properties;

    /**
     * Empty constructor needed by Java JSON library (Jackson)
     */
    public SearchReply() {
    }

    public SearchReply(final List<IProperty<?>> properties,
            final List<IMolecule> molecules) {
        this.properties = properties;
        this.molecules = molecules;
    }

    @Override
    public List<IMolecule> getMolecules() {
        return this.molecules;
    }

    @Override
    public List<IProperty<?>> getProperties() {
        return this.properties;
    }

    @Override
    public String toString() {
        return "Search Reply [" + this.molecules + " , " + this.properties
                + "]";
    }
}
