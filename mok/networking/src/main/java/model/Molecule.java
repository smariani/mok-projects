package model;

import interfaces.model.IMolecule;

/**
 *
 * @author Giacomo Dradi (giacomo.dradi@studio.unibo.it)
 *
 */
public class Molecule implements IMolecule {

    private int concentration;
    private String content;
    private double similarity;

    /**
     * Empty constructor needed by Java JSON library (Jackson)
     */
    public Molecule() {
    }

    /**
     *
     * @param content
     *            the content of the molecule
     */
    public Molecule(final String content) {
        this.content = content;
        this.similarity = 0;
        this.concentration = 0;
    }

    public Molecule(final String content, final int concentration,
            final double similarity) {
        this.content = content;
        this.concentration = concentration;
        this.similarity = similarity;
    }

    @Override
    public int getConcentration() {
        return this.concentration;
    }

    @Override
    public String getContent() {
        return this.content;
    }

    @Override
    public double getSimilarity() {
        return this.similarity;
    }

    @Override
    public void setConcentration(final int concentration) {
        this.concentration = concentration;
    }

    @Override
    public String toString() {
        return "< " + this.content + " > #" + this.concentration + " (fmok: "
                + this.similarity + ")";
    }
}
