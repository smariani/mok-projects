package model;

import interfaces.model.IMolecule;
import interfaces.model.payload.IDiffusingMolecule;

/**
 *
 * @author Giacomo Dradi (giacomo.dradi@studio.unibo.it)
 *
 */
public class DiffusingMolecule implements IDiffusingMolecule {

    private String id;
    private IMolecule molecule;

    /**
     * Empty constructor needed by Java JSON library (Jackson)
     */
    public DiffusingMolecule() {
    }

    /**
     *
     * @param id
     *            the ID given to the diffusing molecule
     * @param molecule
     *            the diffusing molecule
     */
    public DiffusingMolecule(final String id, final IMolecule molecule) {
        this.id = id;
        this.molecule = molecule;
    }

    @Override
    public String getID() {
        return this.id;
    }

    @Override
    public IMolecule getMolecule() {
        return this.molecule;
    }

    @Override
    public String toString() {
        return new StringBuilder().append("DiffusingMolecule #")
                .append(this.id).append(" > ").append(this.molecule).toString();
    }

}
