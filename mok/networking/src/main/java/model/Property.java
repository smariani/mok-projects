package model;

import interfaces.model.IProperty;

/**
 *
 * @author Luca Santonastasi (luca.santonastasi@studio.unibo.it)
 * @author Matteo Fattori (matteo.fattori5@studio.unibo.it)
 *
 * @param <T>
 *            generic parameter type
 *
 */

public class Property<T> implements IProperty<T> {

    private T value;

    public Property() {

    }

    public Property(final T value) {
        this.value = value;
    }

    @Override
    public T getValue() {
        return this.value;
    }

}
