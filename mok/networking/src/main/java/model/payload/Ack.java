package model.payload;

import interfaces.model.payload.IAck;

/**
 *
 * @author Giacomo Dradi (giacomo.dradi@studio.unibo.it)
 *
 */
public class Ack implements IAck {

    private String value;

    /**
     * Empty constructor needed by Java JSON library (Jackson)
     */
    public Ack() {
    }

    /**
     *
     * @param value
     *            the value of the ack
     */
    public Ack(final String value) {
        this.value = value;
    }

    @Override
    public String getValue() {
        return this.value;
    }

    @Override
    public String toString() {
        return "Ack[" + this.value + "]";
    }

}
