package model.payload;

import model.DiffusingTrace;

/**
 *
 * @author Luca Santonastasi (luca.santonastasi@studio.unibo.it)
 * @author Matteo Fattori (matteo.fattori5@studio.unibo.it)
 *
 */

public class DiffusingTraceWrapper extends DiffusingTrace {

    private String senderID;

    /**
     * Empty constructor needed by Java JSON library (Jackson)
     */
    public DiffusingTraceWrapper() {
    }

    /**
     *
     * @param senderID
     *            the ID of the sender compartment
     * @param diffusingTrace
     *            the diffusing trace
     */
    public DiffusingTraceWrapper(final String senderID,
            final DiffusingTrace diffusingTrace) {
        super(diffusingTrace.getID(), diffusingTrace.getTrace());
        this.senderID = senderID;
    }

    /**
     *
     * @return the ID of the sender compartment
     */
    public String getSenderID() {
        return this.senderID;
    }

}
