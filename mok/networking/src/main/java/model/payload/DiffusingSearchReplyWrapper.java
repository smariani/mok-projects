package model.payload;

import model.DiffusingSearchReply;

/**
 *
 * @author Luca Santonastasi (luca.santonastasi@studio.unibo.it)
 * @author Matteo Fattori (matteo.fattori5@studio.unibo.it)
 *
 */

public class DiffusingSearchReplyWrapper extends DiffusingSearchReply {

    private String senderID;

    /**
     * Empty constructor needed by Java JSON library (Jackson)
     */
    public DiffusingSearchReplyWrapper() {
    }

    /**
     *
     * @param senderID
     *            the ID of the sender compartment
     * @param diffusingSearchReply
     *            the diffusing search reply
     */
    public DiffusingSearchReplyWrapper(final String senderID,
            final DiffusingSearchReply diffusingSearchReply) {
        super(diffusingSearchReply.getID(), diffusingSearchReply
                .getSearchReply());
        this.senderID = senderID;
    }

    /**
     *
     * @return the ID of the sender compartment
     */
    public String getSenderID() {
        return this.senderID;
    }
}
