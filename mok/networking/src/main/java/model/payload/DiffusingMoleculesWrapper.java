package model.payload;

import model.DiffusingMolecules;

/**
 *
 * @author Giacomo Dradi (giacomo.dradi@studio.unibo.it)
 *
 */
public class DiffusingMoleculesWrapper extends DiffusingMolecules {

    private String senderID;

    /**
     * Empty constructor needed by Java JSON library (Jackson)
     */
    public DiffusingMoleculesWrapper() {
    }

    /**
     *
     * @param senderID
     *            the ID of the sender compartment
     * @param diffusingMolecules
     *            the diffusing molecules
     */
    public DiffusingMoleculesWrapper(final String senderID,
            final DiffusingMolecules diffusingMolecules) {
        super(diffusingMolecules.getID(), diffusingMolecules.getMolecules());
        this.senderID = senderID;
    }

    /**
     *
     * @return the ID of the sender compartment
     */
    public String getSenderID() {
        return this.senderID;
    }

}
