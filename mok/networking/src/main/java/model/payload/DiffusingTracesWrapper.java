package model.payload;

import model.DiffusingTraces;

/**
 *
 * @author Luca Santonastasi (luca.santonastasi@studio.unibo.it)
 * @author Matteo Fattori (matteo.fattori5@studio.unibo.it)
 *
 */

public class DiffusingTracesWrapper extends DiffusingTraces {

    private String senderID;

    /**
     * Empty constructor needed by Java JSON library (Jackson)
     */
    public DiffusingTracesWrapper() {
    }

    /**
     *
     * @param senderID
     *            the ID of the sender compartment
     * @param diffusingTraces
     *            the diffusing traces
     */
    public DiffusingTracesWrapper(final String senderID,
            final DiffusingTraces diffusingTraces) {
        super(diffusingTraces.getID(), diffusingTraces.getTraces());
        this.senderID = senderID;
    }

    /**
     *
     * @return the ID of the sender compartment
     */
    public String getSenderID() {
        return this.senderID;
    }

}
