/**
 * Created by Stefano Mariani on 22/giu/2015 (mailto: s.mariani@unibo.it)
 */
/*
 * Copyright 1999-2014 Alma Mater Studiorum - Universita' di Bologna
 *
 * This file is part of MoK <http://mok.apice.unibo.it>.
 *
 *    MoK is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU Lesser General Public License as published
 *    by the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    MoK is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU Lesser General Public License for more details.
 *
 *    You should have received a copy of the GNU Lesser General Public License
 *    along with MoK.  If not, see
 *    <https://www.gnu.org/licenses/lgpl.html>.
 *
 */
package model.payload;

import interfaces.model.ICompartment;
import interfaces.model.payload.ITransientCompartment;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * @author Stefano Mariani (mailto: s.mariani@unibo.it)
 *
 */
public class TransientCompartment implements ITransientCompartment {

    private ICompartment compartment;
    private boolean isLeaving;

    /**
     * Empty constructor needed by Java JSON library (Jackson)
     */
    public TransientCompartment() {

    }

    /**
     *
     * @param l
     *            whether the compartment is joining or leaving MoK
     * @param c
     *            the joining/leaving compartment
     */
    public TransientCompartment(final boolean l, final ICompartment c) {
        this.isLeaving = l;
        this.compartment = c;
    }

    /*
     * (non-Javadoc)
     * @see model.payload.ITransientCompartment#getCompartment()
     */
    @Override
    public ICompartment getCompartment() {
        return this.compartment;
    }

    /*
     * (non-Javadoc)
     * @see model.payload.ITransientCompartment#isLeaving()
     */
    @Override
    @JsonProperty("isLeaving")
    public boolean isLeaving() {
        return this.isLeaving;
    }

    /*
     * (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder()
                .append("TransientCompartment: ").append(this.compartment)
                .append(", ");
        if (this.isLeaving) {
            sb.append("leaving");
        } else {
            sb.append("joining");
        }
        return sb.toString();
    }

}
