package model.payload;

import model.DiffusingMolecule;

/**
 *
 * @author Giacomo Dradi (giacomo.dradi@studio.unibo.it)
 *
 */
public class DiffusingMoleculeWrapper extends DiffusingMolecule {

    private String senderID;

    /**
     * Empty constructor needed by Java JSON library (Jackson)
     */
    public DiffusingMoleculeWrapper() {
    }

    /**
     *
     * @param senderID
     *            the ID of the sender compartment
     * @param diffusingMolecule
     *            the diffusing molecule
     */
    public DiffusingMoleculeWrapper(final String senderID,
            final DiffusingMolecule diffusingMolecule) {
        super(diffusingMolecule.getID(), diffusingMolecule.getMolecule());
        this.senderID = senderID;
    }

    /**
     *
     * @return the ID of the sender compartment
     */
    public String getSenderID() {
        return this.senderID;
    }

}
