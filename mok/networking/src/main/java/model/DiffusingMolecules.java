package model;

import interfaces.model.IMolecule;
import interfaces.model.payload.IDiffusingMolecules;
import java.util.List;

/**
 *
 * @author Giacomo Dradi (giacomo.dradi@studio.unibo.it)
 *
 */
public class DiffusingMolecules implements IDiffusingMolecules {

    private String id;
    private List<IMolecule> molecules;

    /**
     * Empty constructor needed by Java JSON library (Jackson)
     */
    public DiffusingMolecules() {
    }

    /**
     *
     * @param id
     *            the ID given to the diffusing molecules
     * @param molecules
     *            the diffusing molecules
     */
    public DiffusingMolecules(final String id, final List<IMolecule> molecules) {
        this.id = id;
        this.molecules = molecules;
    }

    @Override
    public String getID() {
        return this.id;
    }

    @Override
    public List<IMolecule> getMolecules() {
        return this.molecules;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("DiffusingMolecules #").append(this.id).append(" {\n");
        for (int i = 0; i < this.molecules.size(); i++) {
            sb.append("\t").append(this.molecules.get(i)).append("\n");
        }
        sb.append("}\n");
        return sb.substring(0, sb.length() - 1);
    }

}
