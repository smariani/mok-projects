package model;

import interfaces.model.IProperty;
import interfaces.model.ITrace;
import java.util.List;

/**
 *
 * @author Luca Santonastasi (luca.santonastasi@studio.unibo.it)
 * @author Matteo Fattori (matteo.fattori5@studio.unibo.it)
 *
 */

public class Trace implements ITrace {

    private String info;
    private List<IProperty<?>> proper;

    public Trace() {

    }

    public Trace(final String info, final List<IProperty<?>> proper) {
        this.info = info;
        this.proper = proper;
    }

    @Override
    public String getInfo() {
        return this.info;
    }

    @Override
    public List<IProperty<?>> getProperties() {
        return this.proper;
    }

    @Override
    public String toString() {
        return "Trace [" + this.info + " , " + this.proper + "]";
    }

}
