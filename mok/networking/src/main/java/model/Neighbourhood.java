package model;

import interfaces.model.IMembrane;
import interfaces.model.INeighbourhood;
import java.util.List;

/**
 *
 * @author Giacomo Dradi (giacomo.dradi@studio.unibo.it)
 *
 */
public class Neighbourhood implements INeighbourhood {

    private final List<IMembrane> membranes;

    public Neighbourhood(final List<IMembrane> membraneList) {
        this.membranes = membraneList;
    }

    @Override
    public List<IMembrane> getMembranes() {
        return this.membranes;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("Neighborhood {\n");
        for (int i = 0; i < this.membranes.size(); i++) {
            sb.append("\t").append(this.membranes.get(i)).append("\n");
        }
        sb.append("}\n");
        return sb.substring(0, sb.length() - 1);
    }

}
