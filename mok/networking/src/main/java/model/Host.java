package model;

import interfaces.model.IHost;

/**
 *
 * @author Giacomo Dradi (giacomo.dradi@studio.unibo.it)
 *
 */
public class Host implements IHost {

    protected String address;
    protected int port;

    /**
     * Empty constructor needed by Java JSON library (Jackson)
     */
    public Host() {
    }

    /**
     *
     * @param address
     *            the IP address where this host is reachable
     * @param port
     *            the TCP port where this host is reachable
     */
    public Host(final String address, final int port) {
        this.port = port;
        this.address = address;
    }

    @Override
    public String getAddress() {
        return this.address;
    }

    @Override
    public int getPort() {
        return this.port;
    }

    @Override
    public String toString() {
        return this.address + ":" + this.port;
    }

}
