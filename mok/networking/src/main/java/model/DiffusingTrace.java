package model;

import interfaces.model.ITrace;
import interfaces.model.payload.IDiffusingTrace;

/**
 *
 * @author Luca Santonastasi (luca.santonastasi@studio.unibo.it)
 * @author Matteo Fattori (matteo.fattori5@studio.unibo.it)
 *
 */

public class DiffusingTrace implements IDiffusingTrace {

    private String id;
    private ITrace trace;

    /**
     * Empty constructor needed by Java JSON library (Jackson)
     */
    public DiffusingTrace() {
    }

    /**
     *
     * @param id
     *            the ID given to the diffusing trace
     * @param trace
     *            the diffusing trace
     */
    public DiffusingTrace(final String id, final ITrace trace) {
        this.id = id;
        this.trace = trace;
    }

    /*
     * (non-Javadoc)
     * @see interfaces.model.payload.IDiffusingTrace#getID()
     */
    @Override
    public String getID() {
        return this.id;
    }

    /*
     * (non-Javadoc)
     * @see interfaces.model.payload.IDiffusingTrace#getTrace()
     */
    @Override
    public ITrace getTrace() {
        return this.trace;
    }

    @Override
    public String toString() {
        return new StringBuilder().append("DiffusingTrace #").append(this.id)
                .append(" > ").append(this.trace).toString();
    }

}
