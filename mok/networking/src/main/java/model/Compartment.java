package model;

import interfaces.model.ICompartment;
import interfaces.model.IProperty;
import java.util.List;

/**
 *
 * @author Giacomo Dradi (giacomo.dradi@studio.unibo.it)
 *
 */
public class Compartment extends Host implements ICompartment {

    private String id;
    private List<IProperty<?>> properties;

    /**
     * Empty constructor needed by Java JSON library (Jackson)
     */
    public Compartment() {
        super();
    }

    /**
     * @param string
     *            the ID of the compartment
     * @param address
     *            the IP address where it is reachable
     * @param port
     *            the TCP port where it is rechable
     * @param properties
     *            the compartment properties
     */
    public Compartment(final String string, final String address,
            final int port, final List<IProperty<?>> properties) {
        super(address, port);
        this.id = string;
        this.properties = properties;
    }

    /**
     *
     * @param string
     *            the ID of the compartment
     * @param address
     *            the IP address where it is reachable
     * @param port
     *            the TCP port where it is rechable
     * @param propertiesFile
     *            the compartment properties file
     */
    public Compartment(final String string, final String address,
            final int port, final String propertiesFile) {
        super(address, port);
        this.id = string;
        this.properties = this.parseProperties(propertiesFile);
    }

    @Override
    public String getID() {
        return this.id;
    }

    @Override
    public List<IProperty<?>> getProperties() {
        return this.properties;
    }

    @Override
    public String toString() {
        return "Compartment @ " + super.toString();
    }

    /**
     * @param propertiesFile
     * @return
     */
    @SuppressWarnings("static-method")
    private List<IProperty<?>> parseProperties(final String propertiesFile) {
        // TODO Compartment.parseProperties
        return null;
    }

}
