package model;

import interfaces.model.ITrace;
import interfaces.model.payload.IDiffusingTraces;
import java.util.List;

/**
 *
 * @author Luca Santonastasi (luca.santonastasi@studio.unibo.it)
 * @author Matteo Fattori (matteo.fattori5@studio.unibo.it)
 *
 */

public class DiffusingTraces implements IDiffusingTraces {

    private String id;
    private List<ITrace> traces;

    /**
     * Empty constructor needed by Java JSON library (Jackson)
     */
    public DiffusingTraces() {
    }

    /**
     *
     * @param id
     *            the ID given to the diffusing traces
     * @param traces
     *            the list of diffusing traces
     */
    public DiffusingTraces(final String id, final List<ITrace> traces) {
        this.id = id;
        this.traces = traces;
    }

    @Override
    public String getID() {
        return this.id;
    }

    @Override
    public List<ITrace> getTraces() {
        return this.traces;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("DiffusingTraces #").append(this.id).append(" {\n");
        for (int i = 0; i < this.traces.size(); i++) {
            sb.append("\t").append(this.traces.get(i)).append("\n");
        }
        sb.append("}\n");
        return sb.substring(0, sb.length() - 1);
    }

}
