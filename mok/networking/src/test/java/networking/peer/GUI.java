package networking.peer;

import interfaces.model.IMembrane;
import interfaces.model.IMolecule;
import interfaces.peer.components.ILocalCompartment;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.UIManager;
import model.Molecule;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class GUI extends JFrame {

    static private Log log = LogFactory.getLog(GUI.class);
    private static final long serialVersionUID = 1L;

    public GUI(final String title, final ILocalCompartment localCompartment) {
        super(title);

        try {
            UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
        } catch (final Exception e) {
            // Nothing
        }

        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        final JPanel p = new JPanel(new GridBagLayout());
        final GridBagConstraints c = new GridBagConstraints();

        final JButton jm = new JButton("Join MoK");
        c.fill = GridBagConstraints.HORIZONTAL;
        c.gridx = 0;
        c.gridy = 0;
        c.weightx = 0.5;
        p.add(jm, c);
        final JButton lm = new JButton("Leave MoK");
        c.gridx = 1;
        c.gridy = 0;
        p.add(lm, c);
        final JButton dm = new JButton("Diffuse molecule");
        c.gridx = 0;
        c.gridy = 1;
        p.add(dm, c);
        final JButton dms = new JButton("Diffuse molecules");
        c.gridx = 1;
        c.gridy = 1;
        p.add(dms, c);
        final JButton mm = new JButton("Multicast molecule");
        c.gridx = 0;
        c.gridy = 2;
        p.add(mm, c);
        final JButton mms = new JButton("Multicast molecules");
        c.gridx = 1;
        c.gridy = 2;
        p.add(mms, c);
        final JButton bm = new JButton("Broadcast molecule");
        c.gridx = 0;
        c.gridy = 3;
        p.add(bm, c);
        final JButton bms = new JButton("Broadcast molecules");
        c.gridx = 1;
        c.gridy = 3;
        p.add(bms, c);
        final JButton ln = new JButton("Log neighbourhood");
        c.gridx = 0;
        c.gridy = 4;
        c.gridwidth = 2;
        p.add(ln, c);

        final ActionListener listener = ev -> {

            if (ev.getSource() == jm) {
                localCompartment.joinMoK();
            } else if (ev.getSource() == lm) {
                localCompartment.leaveMoK();
            } else if (ev.getSource() == ln) {
                if (GUI.log.isInfoEnabled()) {
                    int i = 0;
                    if (localCompartment.getNeighborhood() != null) {
                        for (final IMembrane m : localCompartment
                                .getNeighborhood().getMembranes()) {
                            GUI.log.info(i + ") " + m);
                            i++;
                        }
                    }
                }
            } else if (ev.getSource() == bm) {
                localCompartment.broadcastMolecule(new Molecule("H2O"));
            } else if (ev.getSource() == bms) {
                final List<IMolecule> molecules = new ArrayList<IMolecule>();
                molecules.add(new Molecule("H2O"));
                molecules.add(new Molecule("H2O2"));
                molecules.add(new Molecule("H2O3"));
                localCompartment.broadcastMolecules(molecules);
            } else {

                if (localCompartment.getNeighborhood() != null
                        && localCompartment.getNeighborhood().getMembranes()
                                .size() != 0) {

                    final String[] possibleValues = new String[localCompartment
                            .getNeighborhood().getMembranes().size()];
                    int i = 0;
                    for (final IMembrane m : localCompartment.getNeighborhood()
                            .getMembranes()) {
                        possibleValues[i] = i + ") " + m.toString();
                        i++;
                    }

                    if (ev.getSource() == dm) {

                        final Object selectedValue = JOptionPane
                                .showInputDialog(null, "Choose ONE membrane:",
                                        "Membrane Selection",
                                        JOptionPane.INFORMATION_MESSAGE, null,
                                        possibleValues, possibleValues[0]);
                        if (selectedValue != null) {
                            final int membraneNumber = Integer
                                    .valueOf(((String) selectedValue)
                                            .substring(0, 1));
                            localCompartment.diffuseMolecule(
                                    new Molecule("H2O"), localCompartment
                                            .getNeighborhood().getMembranes()
                                            .get(membraneNumber));
                        }

                    } else if (ev.getSource() == dms) {

                        final Object selectedValue = JOptionPane
                                .showInputDialog(null, "Choose a membrane:",
                                        "Membrane Selection",
                                        JOptionPane.INFORMATION_MESSAGE, null,
                                        possibleValues, possibleValues[0]);
                        if (selectedValue != null) {
                            final int membraneNumber = Integer
                                    .valueOf(((String) selectedValue)
                                            .substring(0, 1));
                            final List<IMolecule> molecules = new ArrayList<IMolecule>();
                            molecules.add(new Molecule("H2O"));
                            molecules.add(new Molecule("H2O2"));
                            molecules.add(new Molecule("H2O3"));
                            localCompartment
                                    .diffuseMolecules(
                                            molecules,
                                            localCompartment.getNeighborhood()
                                                    .getMembranes()
                                                    .get(membraneNumber));
                        }

                    } else if (ev.getSource() == mm) {

                        final JList<String> list = new JList<String>(
                                possibleValues);
                        JOptionPane.showMessageDialog(null, list,
                                "Choose ONE OR MORE membranes:",
                                JOptionPane.INFORMATION_MESSAGE);
                        if (!list.getSelectedValuesList().isEmpty()) {
                            final ArrayList<IMembrane> membranes = new ArrayList<IMembrane>();
                            for (final String s : list.getSelectedValuesList()) {
                                membranes
                                        .add(localCompartment
                                                .getNeighborhood()
                                                .getMembranes()
                                                .get(Integer.valueOf(s
                                                        .substring(0, 1))));
                            }
                            localCompartment.multicastMolecule(new Molecule(
                                    "H2O"), membranes);
                        }

                    } else if (ev.getSource() == mms) {

                        final List<IMolecule> molecules = new ArrayList<IMolecule>();
                        molecules.add(new Molecule("H2O"));
                        molecules.add(new Molecule("H2O2"));
                        molecules.add(new Molecule("H2O3"));

                        final JList<String> list = new JList<String>(
                                possibleValues);
                        JOptionPane.showMessageDialog(null, list,
                                "Choose ONE OR MORE membranes:",
                                JOptionPane.INFORMATION_MESSAGE);
                        if (!list.getSelectedValuesList().isEmpty()) {
                            final List<IMembrane> membranes = new ArrayList<IMembrane>();
                            for (final String s : list.getSelectedValuesList()) {
                                membranes
                                        .add(localCompartment
                                                .getNeighborhood()
                                                .getMembranes()
                                                .get(Integer.valueOf(s
                                                        .substring(0, 1))));
                            }
                            localCompartment.multicastMolecules(molecules,
                                    membranes);
                        }

                    }
                }

            }
        };

        jm.addActionListener(listener);
        lm.addActionListener(listener);
        dm.addActionListener(listener);
        dms.addActionListener(listener);
        mm.addActionListener(listener);
        mms.addActionListener(listener);
        bm.addActionListener(listener);
        bms.addActionListener(listener);
        ln.addActionListener(listener);

        this.getContentPane().add(p);
        this.pack();
        this.setResizable(true);
        this.setLocationByPlatform(true);

    }

    public void init() {
        this.setVisible(true);
    }

}
