package networking.peer;

import interfaces.model.ICompartment;
import interfaces.model.IMembrane;
import interfaces.model.Protocol;
import interfaces.model.payload.IAck;
import interfaces.model.payload.IDiffusingMolecule;
import interfaces.model.payload.IDiffusingMolecules;
import interfaces.model.payload.IDiffusingSearchReply;
import interfaces.model.payload.IDiffusingTrace;
import interfaces.model.payload.IDiffusingTraces;
import interfaces.observation.ICompartmentObserver;
import interfaces.peer.components.ILocalCompartment;
import java.net.UnknownHostException;
import javax.swing.JOptionPane;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import peer.components.LocalCompartment;

public class TestPeer {

    static private Log log = LogFactory.getLog(TestPeer.class);

    public static void main(final String[] args) {

        try {
            int port = 0;

            final String s = JOptionPane.showInputDialog(null,
                    "Type peer TCP port:", "TCP port input",
                    JOptionPane.QUESTION_MESSAGE);
            // String s = JOptionPane.showInputDialog("Type peer TCP port:");
            if (s != null && s.length() > 0) {
                port = Integer.parseInt(s);
            } else {
                return;
            }

            final ILocalCompartment localCompartment = new LocalCompartment(
                    Protocol.TCP_IP,
                    port, // Compartment PORT
                    "peer.properties", "hotspots.list",
                    "compartment.properties");

            final GUI gui = new GUI("MoK Peer @" + port, localCompartment);
            gui.init();

            localCompartment.addObserver(new ICompartmentObserver() {

                @Override
                public void onDiffusionDone(final boolean result,
                        final IAck ack, final IMembrane membrane) {
                    if (TestPeer.log.isInfoEnabled()) {
                        TestPeer.log.info("Diffusion: " + result + ", " + ack
                                + " through " + membrane);
                    }
                }

                @Override
                public void onJoinDone(final boolean result) {
                    if (TestPeer.log.isInfoEnabled()) {
                        TestPeer.log.info("Join: " + result + ", "
                                + localCompartment.getNeighborhood());
                    }
                }

                @Override
                public void onLeaveDone(final boolean result) {
                    if (TestPeer.log.isInfoEnabled()) {
                        TestPeer.log.info("Leave: " + result + ", "
                                + localCompartment.getNeighborhood());
                    }
                }

                @Override
                public void onMoleculeReceived(
                        final IDiffusingMolecule molecule,
                        final IMembrane membrane) {
                    if (TestPeer.log.isInfoEnabled()) {
                        TestPeer.log.info("Received: " + molecule + " through "
                                + membrane);
                    }
                }

                @Override
                public void onMoleculesReceived(
                        final IDiffusingMolecules moleculeList,
                        final IMembrane membrane) {
                    if (TestPeer.log.isInfoEnabled()) {
                        TestPeer.log.info("Received: " + moleculeList
                                + " through " + membrane);
                    }
                }

                @Override
                public void onNewCompartmentReceived(
                        final ICompartment compartment) {
                    if (TestPeer.log.isInfoEnabled()) {
                        TestPeer.log.info("Received: " + compartment);
                    }
                }

                @Override
                public void onOldCompartmentLeft(final ICompartment compartment) {
                    if (TestPeer.log.isInfoEnabled()) {
                        TestPeer.log.info("Left: " + compartment);
                    }
                }

                @Override
                public void onSearchReplyReceived(
                        final IDiffusingSearchReply diffusingSearchReply,
                        final IMembrane membrane) {
                    /*
                     * Not used here, but in 'communication' module
                     */

                }

                @Override
                public void onTraceReceived(
                        final IDiffusingTrace diffusingTrace,
                        final IMembrane membrane) {
                    /*
                     * Not used here, but in 'communication' module
                     */

                }

                @Override
                public void onTracesReceived(
                        final IDiffusingTraces diffusingTraces,
                        final IMembrane membrane) {
                    /*
                     * Not used here, but in 'communication' module
                     */

                }

            });

        } catch (final UnknownHostException e) {
            e.printStackTrace();
        }

    }
}
