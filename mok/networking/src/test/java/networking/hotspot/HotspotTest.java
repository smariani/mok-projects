package networking.hotspot;

import hotspot.components.tcp.TCPHotspot;

/**
 *
 * @author Giacomo Dradi (giacomo.dradi@studio.unibo.it)
 *
 */
public class HotspotTest {

    public static void main(final String[] args) {

        final TCPHotspot tcpServer = new TCPHotspot("hotspot.properties");
        tcpServer.init();

    }

}
