package test;

/**
 *
 * @author Luca Santonastasi (luca.santonastasi@studio.unibo.it)
 * @author Matteo Fattori (matteo.fattori5@studio.unibo.it)
 *
 */

import interfaces.IAgentCompartment;
import interfaces.model.IMembrane;
import interfaces.model.IMolecule;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.UIManager;
import model.Molecule;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import utils.LogUtils;

public class GUI extends JFrame {

    static private Log log = LogFactory.getLog(GUI.class);
    private static final long serialVersionUID = 1L;

    public GUI(final String title, final IAgentCompartment compartment) {
        super(title);

        try {
            UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
        } catch (final Exception e) {
            // Nothing
        }

        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        final JPanel p = new JPanel(new GridBagLayout());
        final GridBagConstraints c = new GridBagConstraints();

        final JButton jm = new JButton("Join MoK");
        c.fill = GridBagConstraints.HORIZONTAL;
        c.gridx = 0;
        c.gridy = 0;
        c.weightx = 0.5;
        p.add(jm, c);
        final JButton lm = new JButton("Leave MoK");
        c.gridx = 1;
        c.gridy = 0;
        p.add(lm, c);
        final JButton dm = new JButton("Diffuse molecule");
        c.gridx = 0;
        c.gridy = 1;
        p.add(dm, c);
        final JButton dms = new JButton("Diffuse molecules");
        c.gridx = 1;
        c.gridy = 1;
        p.add(dms, c);
        final JButton mm = new JButton("Multicast molecule");
        c.gridx = 0;
        c.gridy = 2;
        p.add(mm, c);
        final JButton mms = new JButton("Multicast molecules");
        c.gridx = 1;
        c.gridy = 2;
        p.add(mms, c);
        final JButton bm = new JButton("Broadcast molecule");
        c.gridx = 0;
        c.gridy = 3;
        p.add(bm, c);
        final JButton bms = new JButton("Broadcast molecules");
        c.gridx = 1;
        c.gridy = 3;
        p.add(bms, c);
        final JButton ln = new JButton("Log neighbourhood");
        c.gridx = 0;
        c.gridy = 4;
        c.gridwidth = 2;
        p.add(ln, c);

        final JButton add = new JButton("Add molecule");
        c.gridx = 0;
        c.gridy = 6;
        c.gridwidth = 1;
        p.add(add, c);
        final JLabel contentLabel = new JLabel("Content");
        c.gridx = 1;
        c.gridy = 5;
        p.add(contentLabel, c);
        final JTextField contentField = new JTextField("some content");
        c.gridx = 1;
        c.gridy = 6;
        p.add(contentField, c);
        final JLabel concLabelA = new JLabel("Concentration");
        c.gridx = 2;
        c.gridy = 5;
        p.add(concLabelA, c);
        final JTextField concFieldA = new JTextField("1");
        c.gridx = 2;
        c.gridy = 6;
        p.add(concFieldA, c);
        final JLabel fmokLabelA = new JLabel("fmok");
        c.gridx = 3;
        c.gridy = 5;
        p.add(fmokLabelA, c);
        final JTextField fmokFieldA = new JTextField("1");
        c.gridx = 3;
        c.gridy = 6;
        p.add(fmokFieldA, c);

        final JButton search = new JButton("Search");
        c.gridx = 0;
        c.gridy = 8;
        p.add(search, c);
        final JLabel fmokLabelS = new JLabel("fmok");
        c.gridx = 1;
        c.gridy = 7;
        p.add(fmokLabelS, c);
        final JTextField fmokFieldS = new JTextField("1");
        c.gridx = 1;
        c.gridy = 8;
        p.add(fmokFieldS, c);
        final JLabel concLabelS = new JLabel("Concentration");
        c.gridx = 2;
        c.gridy = 7;
        p.add(concLabelS, c);
        final JTextField concFieldS = new JTextField("1");
        c.gridx = 2;
        c.gridy = 8;
        p.add(concFieldS, c);
        final JLabel gradientLabel = new JLabel("Gradient horizon");
        c.gridx = 3;
        c.gridy = 7;
        p.add(gradientLabel, c);
        final JTextField gradientField = new JTextField("1");
        c.gridx = 3;
        c.gridy = 8;
        p.add(gradientField, c);

        final JButton showMolecules = new JButton("Log molecules");
        c.gridx = 0;
        c.gridy = 9;
        c.gridwidth = 2;
        p.add(showMolecules, c);

        final ActionListener listener = ev -> {

            if (ev.getSource() == jm) {
                compartment.joinMoK();
            } else if (ev.getSource() == lm) {
                compartment.leaveMoK();
            } else if (ev.getSource() == ln) {
                if (GUI.log.isInfoEnabled()) {
                    int i = 0;
                    if (compartment.getNeighborhood() != null) {
                        for (final IMembrane m : compartment.getNeighborhood()
                                .getMembranes()) {
                            GUI.log.info(i + ") " + m);
                            i++;
                        }
                    }
                }
            } else if (ev.getSource() == bm) {
                compartment.broadcastMolecule(new Molecule("H2O"));
            } else if (ev.getSource() == bms) {
                final List<IMolecule> molecules = new ArrayList<IMolecule>();
                molecules.add(new Molecule("H2O"));
                molecules.add(new Molecule("H2O2"));
                molecules.add(new Molecule("H2O3"));
                compartment.broadcastMolecules(molecules);
            } else {

                if (compartment.getNeighborhood() != null
                        && compartment.getNeighborhood().getMembranes().size() != 0) {

                    final String[] possibleValues = new String[compartment
                            .getNeighborhood().getMembranes().size()];
                    int i = 0;
                    for (final IMembrane m : compartment.getNeighborhood()
                            .getMembranes()) {
                        possibleValues[i] = i + ") " + m.toString();
                        i++;
                    }

                    if (ev.getSource() == dm) {

                        final Object selectedValue = JOptionPane
                                .showInputDialog(null, "Choose ONE membrane:",
                                        "Membrane Selection",
                                        JOptionPane.INFORMATION_MESSAGE, null,
                                        possibleValues, possibleValues[0]);
                        if (selectedValue != null) {
                            final int membraneNumber = Integer
                                    .valueOf(((String) selectedValue)
                                            .substring(0, 1));
                            compartment
                                    .diffuseMolecule(
                                            new Molecule("H2O"),
                                            compartment.getNeighborhood()
                                                    .getMembranes()
                                                    .get(membraneNumber));
                        }

                    } else if (ev.getSource() == dms) {

                        final Object selectedValue = JOptionPane
                                .showInputDialog(null, "Choose a membrane:",
                                        "Membrane Selection",
                                        JOptionPane.INFORMATION_MESSAGE, null,
                                        possibleValues, possibleValues[0]);
                        if (selectedValue != null) {
                            final int membraneNumber = Integer
                                    .valueOf(((String) selectedValue)
                                            .substring(0, 1));
                            final List<IMolecule> molecules = new ArrayList<IMolecule>();
                            molecules.add(new Molecule("H2O"));
                            molecules.add(new Molecule("H2O2"));
                            molecules.add(new Molecule("H2O3"));
                            compartment
                                    .diffuseMolecules(molecules, compartment
                                            .getNeighborhood().getMembranes()
                                            .get(membraneNumber));
                        }

                    } else if (ev.getSource() == mm) {

                        final JList<String> list = new JList<String>(
                                possibleValues);
                        JOptionPane.showMessageDialog(null, list,
                                "Choose ONE OR MORE membranes:",
                                JOptionPane.INFORMATION_MESSAGE);
                        if (!list.getSelectedValuesList().isEmpty()) {
                            final ArrayList<IMembrane> membranes = new ArrayList<IMembrane>();
                            for (final String s : list.getSelectedValuesList()) {
                                membranes
                                        .add(compartment
                                                .getNeighborhood()
                                                .getMembranes()
                                                .get(Integer.valueOf(s
                                                        .substring(0, 1))));
                            }
                            compartment.multicastMolecule(new Molecule("H2O"),
                                    membranes);
                        }

                    } else if (ev.getSource() == mms) {

                        final List<IMolecule> molecules = new ArrayList<IMolecule>();
                        molecules.add(new Molecule("H2O"));
                        molecules.add(new Molecule("H2O2"));
                        molecules.add(new Molecule("H2O3"));

                        final JList<String> list = new JList<String>(
                                possibleValues);
                        JOptionPane.showMessageDialog(null, list,
                                "Choose ONE OR MORE membranes:",
                                JOptionPane.INFORMATION_MESSAGE);
                        if (!list.getSelectedValuesList().isEmpty()) {
                            final List<IMembrane> membranes = new ArrayList<IMembrane>();
                            for (final String s : list.getSelectedValuesList()) {
                                membranes
                                        .add(compartment
                                                .getNeighborhood()
                                                .getMembranes()
                                                .get(Integer.valueOf(s
                                                        .substring(0, 1))));
                            }
                            compartment
                                    .multicastMolecules(molecules, membranes);
                        }

                    } else if (ev.getSource() == search) {
                        final List<List<String>> routes = compartment
                                .getRoutes();
                        Object selectedValue = null;
                        final double similarity = Double.parseDouble(fmokFieldS
                                .getText());
                        final int concentration = Integer.parseInt(concFieldS
                                .getText());
                        final int gradient = Integer.parseInt(gradientField
                                .getText());
                        if (!routes.isEmpty()) {
                            if (GUI.log.isDebugEnabled()) {
                                GUI.log.debug("Routing info found, switching to 'focussed' mode");
                            }
                            selectedValue = JOptionPane.showInputDialog(null,
                                    "Choose route:", "Route Selection",
                                    JOptionPane.INFORMATION_MESSAGE, null,
                                    routes.toArray(), routes.get(0));
                        }
                        if (selectedValue != null) {
                            // TODO This is ugly (depends on refactoring of
                            // routingInfoCache)
                            final List<String> route = ((List<String>) selectedValue);
                            if (GUI.log.isInfoEnabled()) {
                                GUI.log.info("Route selected: " + route);
                            }
                            compartment.searchRequest(route, "", similarity,
                                    concentration, route.size() - 1, 0,
                                    IAgentCompartment.MODE_FOCUSED);
                        } else {
                            if (GUI.log.isDebugEnabled()) {
                                GUI.log.debug("Routing info NOT found, default 'broadcast' mode");
                            }
                            compartment.searchRequest(new ArrayList<String>(),
                                    "", similarity, concentration, gradient, 0,
                                    IAgentCompartment.MODE_BROADCAST);
                        }
                    } else if (ev.getSource() == add) {
                        final String content = contentField.getText();
                        final int concentration = Integer.parseInt(concFieldA
                                .getText());
                        final double similarity = Double.parseDouble(fmokFieldA
                                .getText());
                        final IMolecule m = new Molecule(content,
                                concentration, similarity);
                        compartment.addMolecule(m);
                        if (GUI.log.isInfoEnabled()) {
                            GUI.log.info("Added molecule: " + m);
                        }
                    } else if (ev.getSource() == showMolecules) {
                        if (GUI.log.isInfoEnabled()) {
                            LogUtils.logMolecules(log,
                                    compartment.getMolecules());
                        }
                    }
                }

            }
        };

        jm.addActionListener(listener);
        lm.addActionListener(listener);
        dm.addActionListener(listener);
        dms.addActionListener(listener);
        mm.addActionListener(listener);
        mms.addActionListener(listener);
        bm.addActionListener(listener);
        bms.addActionListener(listener);
        ln.addActionListener(listener);
        search.addActionListener(listener);
        add.addActionListener(listener);
        showMolecules.addActionListener(listener);

        this.getContentPane().add(p);
        this.pack();
        this.setResizable(true);
        this.setLocationByPlatform(true);

    }

    public void init() {
        this.setVisible(true);
    }

}
