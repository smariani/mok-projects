package test;

import interfaces.IAgentCompartment;
import interfaces.observation.ICompartmentObserver;
import javax.swing.JOptionPane;
import mokcompartment.AgentCompartment;
import mokcompartment.CompartmentObserver;

public class TestPeer {

    public static void main(final String[] args) {

        int port = 0;

        final String s = JOptionPane.showInputDialog(null,
                "Type peer TCP port:", "TCP port input",
                JOptionPane.QUESTION_MESSAGE);
        if (s != null && s.length() > 0) {
            port = Integer.parseInt(s);
        } else {
            return;
        }

        final IAgentCompartment comp = new AgentCompartment(port);
        final ICompartmentObserver obs = new CompartmentObserver(comp);

        // TODO Decide what to bring into interface and what's only for tests
        final GUI gui = new GUI("MoK Peer @" + port, comp);
        gui.init();

        comp.addObserver(obs);
    }
}
