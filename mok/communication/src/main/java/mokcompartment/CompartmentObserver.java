package mokcompartment;

import interfaces.IAgentCompartment;
import interfaces.model.ICompartment;
import interfaces.model.IMembrane;
import interfaces.model.IMolecule;
import interfaces.model.IProperty;
import interfaces.model.payload.IAck;
import interfaces.model.payload.IDiffusingMolecule;
import interfaces.model.payload.IDiffusingMolecules;
import interfaces.model.payload.IDiffusingSearchReply;
import interfaces.model.payload.IDiffusingTrace;
import interfaces.model.payload.IDiffusingTraces;
import interfaces.observation.ICompartmentObserver;
import java.util.ArrayList;
import java.util.List;
import model.Molecule;
import model.Property;
import model.Trace;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 *
 * @author Luca Santonastasi (luca.santonastasi@studio.unibo.it)
 * @author Matteo Fattori (matteo.fattori5@studio.unibo.it)
 *
 */

public class CompartmentObserver implements ICompartmentObserver {

    static private Log log = LogFactory.getLog(CompartmentObserver.class);
    private final AgentCompartment agentCompartment;

    public CompartmentObserver(final IAgentCompartment agComp) {
        // TODO Ugly, create new interfaces as logically demanded by the nature
        // of the methods to provide
        this.agentCompartment = (AgentCompartment) agComp;
    }

    @Override
    public void onDiffusionDone(final boolean result, final IAck ack,
            final IMembrane membrane) {
        if (CompartmentObserver.log.isInfoEnabled()) {
            CompartmentObserver.log.info("Diffusion: " + result + ", " + ack
                    + " through " + membrane);
        }
    }

    @Override
    public void onJoinDone(final boolean result) {
        if (CompartmentObserver.log.isInfoEnabled()) {
            CompartmentObserver.log.info("Join: " + result + ", "
                    + this.agentCompartment.getNeighborhood());
        }
    }

    @Override
    public void onLeaveDone(final boolean result) {
        if (CompartmentObserver.log.isInfoEnabled()) {
            CompartmentObserver.log.info("Leave: " + result + ", "
                    + this.agentCompartment.getNeighborhood());
        }
    }

    @Override
    public void onMoleculeReceived(final IDiffusingMolecule molecule,
            final IMembrane membrane) {
        if (CompartmentObserver.log.isInfoEnabled()) {
            CompartmentObserver.log.info("Received: " + molecule + " through: "
                    + membrane);
        }
    }

    @Override
    public void onMoleculesReceived(final IDiffusingMolecules moleculeList,
            final IMembrane membrane) {
        if (CompartmentObserver.log.isInfoEnabled()) {
            CompartmentObserver.log.info("Received: " + moleculeList
                    + " through " + membrane);
        }
    }

    @Override
    public void onNewCompartmentReceived(final ICompartment compartment) {
        if (CompartmentObserver.log.isInfoEnabled()) {
            CompartmentObserver.log.info("Received: " + compartment);
        }
    }

    @Override
    public void onOldCompartmentLeft(final ICompartment compartment) {
        if (CompartmentObserver.log.isInfoEnabled()) {
            CompartmentObserver.log.info("Left: " + compartment);
        }
        this.agentCompartment.removePreferences(compartment.getID());
        this.agentCompartment.removeRoutingInfo(compartment.getID());
        this.agentCompartment.verifyTimeStampAndHit();
    }

    @Override
    public void onSearchReplyReceived(final IDiffusingSearchReply searchReply,
            final IMembrane membrane) {
        if (CompartmentObserver.log.isDebugEnabled()) {
            CompartmentObserver.log.debug("Search reply from: "
                    + membrane.getRemoteCompartment().getID());
        }
        final List<IMolecule> molecules = searchReply.getSearchReply()
                .getMolecules();
        final List<IProperty<?>> properties = searchReply.getSearchReply()
                .getProperties();

        // update preferences
        this.agentCompartment.setPreferences(membrane.getRemoteCompartment()
                .getID(), molecules.get(0).getSimilarity());
        final ArrayList<String> route = (ArrayList<String>) properties.get(0)
                .getValue();
        final int gradient = (int) properties.get(1).getValue();
        final double similarity = molecules.get(0).getSimilarity();

        if (gradient > 0 && route.size() > 0) {
            // forwarding
            this.agentCompartment.searchReply(route, membrane
                    .getRemoteCompartment().getID(), similarity, molecules,
                    gradient);
            if (CompartmentObserver.log.isInfoEnabled()) {
                CompartmentObserver.log.info("Forwarding: " + molecules
                        + " through: " + membrane + " (gradient = " + gradient
                        + ", route = " + route + ")");
            }
        } else {
            for (final IMolecule mol : molecules) {
                this.agentCompartment.addMolecule(mol);
                if (CompartmentObserver.log.isInfoEnabled()) {
                    CompartmentObserver.log.info("Received: " + mol + " from: "
                            + membrane + " (gradient = " + gradient + ")");
                }
            }
        }
    }

    // TODO Method has to be split to handle diverse traces
    @Override
    public void onTraceReceived(final IDiffusingTrace trace,
            final IMembrane membrane) {
        // TODO This is VERY ugly, better use Enum
        if (trace.getTrace().getInfo().equals("searchRequest")) {
            if (CompartmentObserver.log.isDebugEnabled()) {
                CompartmentObserver.log.debug("Trace: "
                        + trace.getTrace().getInfo() + " from: " + membrane);
            }
            // get the Request parameter
            final List<String> requestRoute = (List<String>) trace.getTrace()
                    .getProperties().get(0).getValue();
            final ArrayList<String> replyRoute = new ArrayList<String>();
            replyRoute.addAll(requestRoute);
            final double similarity = (double) trace.getTrace().getProperties()
                    .get(1).getValue();
            final int concentration = (int) trace.getTrace().getProperties()
                    .get(2).getValue();
            final int gradient = (int) trace.getTrace().getProperties().get(3)
                    .getValue();
            final int nHops = (int) trace.getTrace().getProperties().get(4)
                    .getValue();
            final int mode = (int) trace.getTrace().getProperties().get(5)
                    .getValue();

            // update Routing info
            if (requestRoute.size() > 0) {

                if (!requestRoute.get(0).equals(this.agentCompartment.getID())) {
                    final ArrayList<String> myRoute = new ArrayList<String>();
                    if (mode == IAgentCompartment.MODE_FOCUSED) {
                        // here i have the full list routing, but this node is
                        // partially interested
                        // maybe we need a function in MoKCompartment that tell
                        // us if this node is
                        // interested (by similarity for example)
                        for (int i = nHops - 1; i < requestRoute.size(); i++) {
                            myRoute.add(requestRoute.get(i));
                        }
                    } else {
                        // the first element is the compartment who sent the
                        // request first
                        myRoute.addAll(requestRoute);
                        myRoute.add(this.agentCompartment.getID());
                    }
                    if (CompartmentObserver.log.isInfoEnabled()) {
                        CompartmentObserver.log.info("Adding routing info: "
                                + myRoute.get(0) + " to: " + myRoute);
                    }
                    this.agentCompartment.addRoutingInfo(myRoute.get(0),
                            myRoute);
                }
            }

            // forwarding
            this.agentCompartment.searchRequest(requestRoute,
                    this.agentCompartment.getID(), similarity, concentration,
                    gradient, nHops, mode);

            // set compartment preference
            this.agentCompartment.setPreferences(membrane
                    .getRemoteCompartment().getID(), similarity);

            // if the request is focused and this is the first node of the
            // Routing List then reply
            if (mode == IAgentCompartment.MODE_FOCUSED
                    && requestRoute.get(0)
                            .equals(this.agentCompartment.getID())
                    || mode == IAgentCompartment.MODE_BROADCAST) {
                // verify the presence of needed knowledge
                final List<IMolecule> matchingMolecules = this.agentCompartment
                        .getMoleculesBySimilarity(similarity);

                if (matchingMolecules.size() > 0) {
                    // response
                    if (CompartmentObserver.log.isInfoEnabled()) {
                        CompartmentObserver.log
                                .info("Searched molecules found: "
                                        + matchingMolecules);
                    }
                    final List<IMolecule> molToSend = new ArrayList<IMolecule>();
                    matchingMolecules.forEach(m -> {
                        final IMolecule mol = new Molecule(m.getContent(),
                                concentration, similarity);
                        molToSend.add(new Molecule(m.getContent(),
                                this.agentCompartment.removeMolecule(mol),
                                similarity));
                    });
                    // need to reverse the list Routing for response
                    if (mode == IAgentCompartment.MODE_FOCUSED) {
                        replyRoute.clear();
                        // TODO Use utilities for reversing list
                        for (int i = requestRoute.size() - 1; i > 0; i--) {
                            replyRoute.add(requestRoute.get(i));
                        }
                        if (CompartmentObserver.log.isDebugEnabled()) {
                            CompartmentObserver.log
                                    .debug("Search request route  was: "
                                            + requestRoute
                                            + ", search reply route is: "
                                            + replyRoute);
                        }
                    }
                    this.agentCompartment.searchReply(replyRoute,
                            this.agentCompartment.getID(), similarity,
                            molToSend, nHops);
                }
            }
            // TODO This is VERY ugly too (see above)
        } else if (trace.getTrace().getInfo().equals("missingHop")) {

            final List<String> route = (List<String>) trace.getTrace()
                    .getProperties().get(0).getValue();
            final int gradient = (Integer) trace.getTrace().getProperties()
                    .get(1).getValue();
            final String compartmentToRemove = (String) trace.getTrace()
                    .getProperties().get(2).getValue();

            this.agentCompartment.removeRoutingInfo(compartmentToRemove);
            if (CompartmentObserver.log.isDebugEnabled()) {
                CompartmentObserver.log
                        .debug("Compartment to remove from route: "
                                + compartmentToRemove);
            }
            // forwarding
            if (gradient > 0) {
                IMembrane nextComp = null;
                for (final IMembrane membr : this.agentCompartment
                        .getNeighborhood().getMembranes()) {
                    if (membr.getRemoteCompartment().getID()
                            .equals(route.get(gradient + 1))) {
                        nextComp = membr;
                        if (CompartmentObserver.log.isDebugEnabled()) {
                            CompartmentObserver.log.debug("nextComp --> "
                                    + membr.getRemoteCompartment().getAddress()
                                    + ":"
                                    + membr.getRemoteCompartment().getPort());
                        }
                    }
                }
                if (nextComp != null) {
                    final List<IProperty<?>> properties = new ArrayList<IProperty<?>>();
                    properties.add(new Property<List<String>>(route));
                    properties.add(new Property<Integer>(route.size() - 1
                            - route.indexOf(this.agentCompartment.getID())));
                    properties.add(new Property<String>(compartmentToRemove));
                    CompartmentObserver.log.info("Forwarding to: " + nextComp
                            + " request to remove: " + compartmentToRemove);
                    this.agentCompartment.diffuseTrace(new Trace("missingHop",
                            properties), nextComp);
                }
            }
            // TODO This is VERY ugly too, again (see above)
        } else if (trace.getTrace().getInfo().equals("diffuseUpdates")) {

            final String uniqueID = (String) trace.getTrace().getProperties()
                    .get(0).getValue();
            final List<IMolecule> molecule = (List<IMolecule>) trace.getTrace()
                    .getProperties().get(1).getValue();
            final int concentration = (Integer) trace.getTrace()
                    .getProperties().get(2).getValue();
            final int gradient = (Integer) trace.getTrace().getProperties()
                    .get(3).getValue();
            final int mode = (Integer) trace.getTrace().getProperties().get(4)
                    .getValue();
            IMembrane nextComp = null;
            // forwarding
            if (gradient > 0) {

                // TODO Useless switch, mode is passed down and not used
                switch (mode) {
                    case AgentCompartment.MODE_RANDOM:
                        // TODO intentionally left blank
                        break;

                    case AgentCompartment.MODE_NEIGHBORHOOD:
                        for (final IMembrane m : this.agentCompartment
                                .getNeighborhood().getMembranes()) {
                            nextComp = m;
                            if (nextComp != null) {
                                this.agentCompartment
                                        .diffuseUpdates(uniqueID, molecule,
                                                concentration, mode, gradient);
                            }
                        }
                        break;

                    case AgentCompartment.MODE_FOCUSED:
                        for (final IMembrane m : this.agentCompartment
                                .getNeighborhood().getMembranes()) {
                            nextComp = m;
                            if (nextComp != null) {
                                this.agentCompartment
                                        .diffuseUpdates(uniqueID, molecule,
                                                concentration, mode, gradient);
                            }
                        }
                        break;

                    case AgentCompartment.MODE_PROBABILISTIC:
                        for (final IMembrane m : this.agentCompartment
                                .getNeighborhood().getMembranes()) {
                            nextComp = m;
                            if (nextComp != null) {
                                this.agentCompartment
                                        .diffuseUpdates(uniqueID, molecule,
                                                concentration, mode, gradient);
                            }
                        }
                        break;

                    default:
                        break;

                }
            }
        }
    }

    @Override
    public void onTracesReceived(final IDiffusingTraces diffusingTraces,
            final IMembrane membrane) {
        // TODO Auto-generated method stub
    }

}
