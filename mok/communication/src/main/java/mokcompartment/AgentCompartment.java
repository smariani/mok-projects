package mokcompartment;

import interfaces.IAgentCompartment;
import interfaces.model.ICompartment;
import interfaces.model.IMembrane;
import interfaces.model.IMolecule;
import interfaces.model.INeighbourhood;
import interfaces.model.IProperty;
import interfaces.model.ISearchReply;
import interfaces.model.ITrace;
import interfaces.model.Protocol;
import interfaces.observation.ICompartmentObserver;
import interfaces.peer.components.ILocalCompartment;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.stream.Collectors;
import model.Property;
import model.SearchReply;
import model.Trace;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import peer.components.LocalCompartment;

/**
 *
 * @author Luca Santonastasi (luca.santonastasi@studio.unibo.it)
 * @author Matteo Fattori (matteo.fattori5@studio.unibo.it)
 *
 */

public class AgentCompartment implements IAgentCompartment {

    static private Log log = LogFactory.getLog(AgentCompartment.class);
    /*
     * TODO Put in config file
     */
    public static final long MAX_TIME_STAMP_AGE = 1000 * 60 * 2; // 2 minutes
    public static final long MIN_DELAY_UPDATE = 100;
    public static final long MIN_CACHE_HITS = 5;
    public static final int PROBABILITY = 30;
    public static final int MODE_FOCUSED = 892;
    public static final int MODE_NEIGHBORHOOD = 891;
    public static final int MODE_PROBABILISTIC = 893;
    public static final int MODE_RANDOM = 890;

    private int currentUpdateID = 1;
    private List<String> duplicateUpdates;
    /*
     * Knowledge held by the Compartment
     */
    private List<IMolecule> molecules;
    private ILocalCompartment localCompartment;
    /*
     * need to save the neighborhood preferences (Compartment UUID, list of
     * similarity MoK Function)
     */
    private Map<String, List<Double>> preferences;

    /*
     * need to save the Routing information about a compartment and the
     * timestamp when inserted and the hit (Compartment UUID, list of
     * nodes(Routing List))
     */
    private Map<String, List<IProperty<?>>> routingInfoCache;

    /**
     * 
     * @param port
     */
    public AgentCompartment(final int port) {
        try {
            this.localCompartment = new LocalCompartment(Protocol.TCP_IP,
                    port, // Compartment PORT
                    "peer.properties", "hotspots.list",
                    "compartment.properties");
            this.preferences = new HashMap<String, List<Double>>();
            this.molecules = new CopyOnWriteArrayList<IMolecule>();
            this.routingInfoCache = new HashMap<String, List<IProperty<?>>>();
            this.duplicateUpdates = new ArrayList<String>();
        } catch (final UnknownHostException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void addObserver(final ICompartmentObserver observer) {
        this.localCompartment.addObserver(observer);
    }

    /**
     * 
     * @param compartmentID
     * @param route
     */
    public void addRoutingInfo(final String compartmentID,
            final List<String> route) {
        final long timeStamp = System.currentTimeMillis();
        final long hit = 1;
        /*
         * TODO Consider substituting with Java {@link java.util.Properties}.
         * I'd rather create an ad-hoc type, e.g. RoutingInfoCache:<route,
         * timestamp, #hit>.
         */
        final List<IProperty<?>> props = new ArrayList<IProperty<?>>();
        props.add(new Property<List<String>>(route));
        props.add(new Property<Long>(timeStamp));
        props.add(new Property<Long>(hit));
        this.routingInfoCache.put(compartmentID, props);
    }

    @Override
    public void broadcastMolecule(final IMolecule molecule) {
        this.localCompartment.broadcastMolecule(molecule);
    }

    @Override
    public void broadcastMolecules(final List<IMolecule> molecules) {
        this.localCompartment.broadcastMolecules(molecules);
    }

    @Override
    public void broadcastTrace(final ITrace trace) {
        this.localCompartment.broadcastTrace(trace);
    }

    @Override
    public void broadcastTraces(final List<ITrace> traces) {
        this.localCompartment.broadcastTraces(traces);
    }

    /**
     * 
     * @param mol
     * @return
     */
    public int removeMolecule(final IMolecule mol) {
        for (final IMolecule m : this.molecules) {
            if (m.getContent().equals(mol.getContent())) {
                if (m.getConcentration() > 1
                        && m.getConcentration() - mol.getConcentration() > 0) {
                    final int newc = m.getConcentration()
                            - mol.getConcentration();
                    m.setConcentration(newc);
                    return mol.getConcentration();
                }
                this.molecules.remove(m);
                return m.getConcentration();
            }
        }
        // TODO Ugly, better throw exception
        return -1;
    }

    @Override
    public void diffuseMolecule(final IMolecule molecule,
            final IMembrane membrane) {
        this.localCompartment.diffuseMolecule(molecule, membrane);
    }

    @Override
    public void diffuseMolecules(final List<IMolecule> molecules,
            final IMembrane membrane) {
        this.localCompartment.diffuseMolecules(molecules, membrane);
    }

    @Override
    public void diffuseSearchReply(final ISearchReply searchReply,
            final IMembrane membrane) {
        this.localCompartment.diffuseSearchReply(searchReply, membrane);
    }

    @Override
    public void diffuseTrace(final ITrace trace, final IMembrane membrane) {
        this.localCompartment.diffuseTrace(trace, membrane);
    }

    @Override
    public void diffuseTrace(final List<String> route, final ITrace trace,
            final String compartmentName, final double fmok,
            final long concentration, final long gradient) {
        // TODO Auto-generated method stub

    }

    @Override
    public void diffuseTraces(final List<ITrace> traces,
            final IMembrane membrane) {
        this.localCompartment.diffuseTraces(traces, membrane);
    }

    /*
     * (non-Javadoc)
     * @see interfaces.IAgentCompartment#diffuseUpdates(java.lang.String,
     * java.util.List, long, int, long)
     */
    @Override
    public void diffuseUpdates(final String uniqueID,
            final List<IMolecule> molecules, final long concentration,
            final int mode, final long gradient) {
        // gli uuid sono composti da 36 caratteri
        if (AgentCompartment.log.isDebugEnabled()) {
            AgentCompartment.log.debug("Request to diffuse updates received");
        }

        if (this.duplicateUpdates.contains(uniqueID)) {
            if (AgentCompartment.log.isInfoEnabled()) {
                AgentCompartment.log.info("Ignored duplicate update");
            }
        } else {

            final List<IProperty<?>> properties = new ArrayList<IProperty<?>>();
            final List<IMembrane> targets = new ArrayList<IMembrane>();

            switch (mode) {

                case MODE_RANDOM:
                    // System.out.println("UPDATE_MODE_RANDOM");
                    // for(int i = 0 ; i < molecule.size() ; i++ ){
                    // searchIfHaveToUpdates(molecule.get(i));
                    // }
                    // if(grad > 0) {
                    // properties.add(new Property<String>(uniqueID));
                    // properties.add(new
                    // Property<ArrayList<IMolecule>>(molecule));
                    // properties.add(new Property<Integer>(concentration));
                    // properties.add(new Property<Integer>(grad-1));
                    // properties.add(new Property<Integer>(mode));
                    //
                    // for(IMembrane node:
                    // this.localCompartment.getNeighborhood().getMembranes()) {
                    // if(canIUpdate()){
                    // toSend.add(node);
                    // }
                    // }
                    // System.out.println("[COMPARTMENT] toSend "+toSend);
                    // this.multicastTrace(new
                    // Trace("diffuseUpdates",properties), toSend);
                    // }
                    break;

                case MODE_NEIGHBORHOOD:
                    if (AgentCompartment.log.isDebugEnabled()) {
                        AgentCompartment.log
                                .debug("Update mode: neighbourhood");
                    }
                    for (int i = 0; i < molecules.size(); i++) {
                        this.checkUpdatesDue(molecules.get(i));
                    }
                    if (gradient > 0) {
                        properties.add(new Property<String>(uniqueID));
                        properties
                                .add(new Property<List<IMolecule>>(molecules));
                        properties.add(new Property<Long>(concentration));
                        properties.add(new Property<Integer>(1));
                        properties.add(new Property<Integer>(mode));

                        for (final IMembrane m : this.localCompartment
                                .getNeighborhood().getMembranes()) {
                            targets.add(m);
                        }
                        if (AgentCompartment.log.isInfoEnabled()) {
                            AgentCompartment.log.info("Diffusing updates to: "
                                    + targets);
                        }
                        this.multicastTrace(new Trace("diffuseUpdates",
                                properties), targets);
                    }
                    break;

                case MODE_FOCUSED:
                    if (AgentCompartment.log.isDebugEnabled()) {
                        AgentCompartment.log.debug("Update mode: focussed");
                    }

                    for (int i = 0; i < molecules.size(); i++) {
                        this.checkUpdatesDue(molecules.get(i));
                    }
                    if (gradient > 0) {
                        properties.add(new Property<String>(uniqueID));
                        properties
                                .add(new Property<List<IMolecule>>(molecules));
                        properties.add(new Property<Long>(concentration));
                        properties.add(new Property<Long>(gradient - 1));
                        properties.add(new Property<Integer>(mode));

                        for (final IMembrane m : this.localCompartment
                                .getNeighborhood().getMembranes()) {
                            targets.add(m);
                        }

                        if (AgentCompartment.log.isInfoEnabled()) {
                            AgentCompartment.log.info("Diffusing updates to: "
                                    + targets);
                        }
                        this.multicastTrace(new Trace("diffuseUpdates",
                                properties), targets);
                    }
                    break;

                case MODE_PROBABILISTIC:
                    if (AgentCompartment.log.isDebugEnabled()) {
                        AgentCompartment.log
                                .debug("Update mode: probabilistic");
                    }

                    if (AgentCompartment.shouldUpdate()) {
                        for (int i = 0; i < molecules.size(); i++) {
                            this.checkUpdatesDue(molecules.get(i));
                        }
                    }
                    if (gradient > 0) {
                        properties.add(new Property<String>(uniqueID));
                        properties
                                .add(new Property<List<IMolecule>>(molecules));
                        properties.add(new Property<Long>(concentration));
                        properties.add(new Property<Long>(gradient - 1));
                        properties.add(new Property<Integer>(mode));

                        for (final IMembrane node : this.localCompartment
                                .getNeighborhood().getMembranes()) {
                            targets.add(node);
                        }

                        if (AgentCompartment.log.isInfoEnabled()) {
                            AgentCompartment.log.info("Diffusing updates to: "
                                    + targets);
                        }
                        this.multicastTrace(new Trace("diffuseUpdates",
                                properties), targets);
                    }
                    break;
                default:
                    break;

            }

        }
    }

    @Override
    public String getAddress() {
        return this.localCompartment.getAddress();
    }

    @Override
    public ICompartment getCompartment() {
        return this.localCompartment.getCompartment();
    }

    /**
     * DiffuseUpdates Unique ID, available to others for future usage.
     * 
     * @return
     */
    public String getCurrentUpdatesID() {
        return this.getID() + "_U_" + this.currentUpdateID++;
    }

    @Override
    public String getID() {
        return this.localCompartment.getID();
    }

    /**
     * 
     * @param similarity
     * @return
     */
    public List<IMolecule> getMoleculesBySimilarity(final double similarity) {
        return this.molecules.stream()
                .filter(e -> e.getSimilarity() == similarity)
                .collect(Collectors.toList());
    }

    @Override
    public INeighbourhood getNeighborhood() {
        return this.localCompartment.getNeighborhood();
    }

    @Override
    public int getPort() {
        return this.localCompartment.getPort();
    }

    /**
     * For future usage by others.
     * 
     * @param compartmentID
     * @return
     */
    public List<Double> getPreferences(final String compartmentID) {
        if (this.preferences.containsKey(compartmentID)) {
            return this.preferences.get(compartmentID);
        }
        return Collections.emptyList();
    }

    @Override
    public List<IProperty<?>> getProperties() {
        return this.localCompartment.getProperties();
    }

    /**
     * 
     * @return
     */
    @Override
    public List<List<String>> getRoutes() {
        if (this.routingInfoCache.isEmpty()) {
            return Collections.emptyList();
        }
        final List<List<String>> routes = new ArrayList<>();
        for (final String s : this.routingInfoCache.keySet()) {
            final List<IProperty<?>> p = this.routingInfoCache.get(s);
            final List<String> route = (List<String>) p.get(0).getValue();
            routes.add(route);
        }
        return routes;
    }

    /**
     * 
     * @param mol
     */
    @Override
    public void addMolecule(final IMolecule mol) {
        boolean found = false;
        for (final IMolecule m : this.molecules) {
            if (m.getContent().equals(mol.getContent())) {
                // absorb knowledge
                m.setConcentration(m.getConcentration()
                        + mol.getConcentration());
                found = true;
                break;
            }
        }
        if (!found) {
            // someone gives me knowledge that i didn't know
            this.molecules.add(mol);
        }
    }

    @Override
    public void joinMoK() {
        this.localCompartment.joinMoK();
    }

    @Override
    public List<IMolecule> getMolecules() {
        return this.molecules;
    }

    @Override
    public void leaveMoK() {
        this.localCompartment.leaveMoK();
    }

    @Override
    public void multicastMolecule(final IMolecule molecule,
            final List<IMembrane> membranes) {
        this.localCompartment.multicastMolecule(molecule, membranes);
    }

    @Override
    public void multicastMolecules(final List<IMolecule> molecules,
            final List<IMembrane> membranes) {
        this.localCompartment.multicastMolecules(molecules, membranes);
    }

    @Override
    public void multicastTrace(final ITrace trace,
            final List<IMembrane> membranes) {
        this.localCompartment.multicastTrace(trace, membranes);

    }

    @Override
    public void multicastTraces(final List<ITrace> traces,
            final List<IMembrane> membranes) {
        this.localCompartment.multicastTraces(traces, membranes);

    }

    /**
     * 
     * @param compartmentID
     */
    public void removePreferences(final String compartmentID) {
        if (this.preferences.containsKey(compartmentID)) {
            this.preferences.remove(compartmentID);
        }
    }

    /**
     * 
     * @param compartmentID
     */
    public void removeRoutingInfo(final String compartmentID) {
        this.routingInfoCache.remove(compartmentID);
        for (final String s : this.routingInfoCache.keySet()) {
            final List<IProperty<?>> p = this.routingInfoCache.get(s);
            final List<String> listRouting = (List<String>) p.get(0).getValue();
            for (final String node : listRouting) {
                if (node.equals(compartmentID)) {
                    this.removeRoutingInfo(s);
                    break;
                }
            }
        }
    }

    @Override
    public void searchRequest(final List<String> route,
            final String compartmentName, final double fmok,
            final long concentration, final long gradient, final long nHop,
            final int mode) {
        if (AgentCompartment.log.isDebugEnabled()) {
            AgentCompartment.log.debug("Search request received");
        }
        if (gradient > 0) {
            if (mode == IAgentCompartment.MODE_BROADCAST) {
                if (AgentCompartment.log.isDebugEnabled()) {
                    AgentCompartment.log
                            .debug("Search request mode: broadcast");
                }
                route.add(this.localCompartment.getID());
                final List<IProperty<?>> properties = new ArrayList<IProperty<?>>();
                properties.add(new Property<List<String>>(route));
                properties.add(new Property<Double>(fmok));
                properties.add(new Property<Long>(concentration));
                properties.add(new Property<Long>(gradient - 1));
                properties.add(new Property<Long>(nHop + 1));
                properties.add(new Property<Integer>(mode));
                final List<IMembrane> targets = new ArrayList<IMembrane>();
                for (final IMembrane m : this.localCompartment
                        .getNeighborhood().getMembranes()) {
                    targets.add(m);
                    for (final String id : route) {
                        if (m.getRemoteCompartment().getID().equals(id)) {
                            // no need to send the request to the node who send
                            // it to me
                            targets.remove(m);
                        }
                    }
                }
                if (AgentCompartment.log.isInfoEnabled()) {
                    AgentCompartment.log.info("Forwarding search request to: "
                            + targets);
                }
                this.multicastTrace(new Trace("searchRequest", properties),
                        targets);
            } else if (mode == IAgentCompartment.MODE_FOCUSED) {
                if (AgentCompartment.log.isDebugEnabled()) {
                    AgentCompartment.log.debug("Search request mode: focussed");
                }
                if (route.size() > 0) {
                    if (nHop == route.size() - 1) {
                        if (AgentCompartment.log.isInfoEnabled()) {
                            AgentCompartment.log
                                    .info("I'm the target, no forwarding needed");
                        }
                        // TODO Ugly
                        return;
                    }
                    final String nextCompartment = route.get((int) (route
                            .size() - 1 - (nHop + 1)));
                    if (AgentCompartment.log.isDebugEnabled()) {
                        AgentCompartment.log
                                .debug("Forwarding search request to: "
                                        + nextCompartment);
                    }
                    final List<IMembrane> neighbourhood = this.localCompartment
                            .getNeighborhood().getMembranes();
                    IMembrane target = null;
                    for (final IMembrane m : neighbourhood) {
                        if (m.getRemoteCompartment().getID()
                                .equals(nextCompartment)) {
                            target = m;
                        }
                    }
                    if (target != null) {
                        this.updateHit(route.get(0));
                        final List<IProperty<?>> properties = new ArrayList<IProperty<?>>();
                        properties.add(new Property<List<String>>(route));
                        properties.add(new Property<Double>(fmok));
                        properties.add(new Property<Long>(concentration));
                        properties.add(new Property<Long>(gradient - 1));
                        properties.add(new Property<Long>(nHop + 1));
                        properties.add(new Property<Integer>(mode));
                        this.diffuseTrace(
                                new Trace("searchRequest", properties), target);
                    } else {
                        if (AgentCompartment.log.isInfoEnabled()) {
                            AgentCompartment.log
                                    .info("Compartment: "
                                            + nextCompartment
                                            + " disconnected, diffusing routing info update");
                        }
                        this.removeRoutingInfo(nextCompartment);
                        IMembrane nodeToBack = null;
                        for (final IMembrane m : neighbourhood) {
                            if (m.getRemoteCompartment()
                                    .getID()
                                    .equals(route.get((int) (route.size() - nHop)))) {
                                nodeToBack = m;
                                if (AgentCompartment.log.isDebugEnabled()) {
                                    AgentCompartment.log
                                            .debug("nodeToBack ---> "
                                                    + nodeToBack);
                                }
                            }
                        }
                        final List<IProperty<?>> properties = new ArrayList<IProperty<?>>();
                        properties.add(new Property<List<String>>(route));
                        properties.add(new Property<Long>(nHop - 1));
                        properties.add(new Property<String>(nextCompartment));
                        this.diffuseTrace(new Trace("missingHop", properties),
                                nodeToBack);
                    }
                }
            }
        }

    }

    @Override
    public void searchReply(final List<String> routes,
            final String compartmentName, final double fmok,
            final List<IMolecule> molecules, final long gradient) {
        if (AgentCompartment.log.isDebugEnabled()) {
            AgentCompartment.log.debug("Search reply received");
        }
        if (routes.size() > 0) {
            final List<IMembrane> neighbours = this.localCompartment
                    .getNeighborhood().getMembranes();
            IMembrane nodeToSend = null;
            for (final IMembrane m : neighbours) {
                if (m.getRemoteCompartment().getID()
                        .equals(routes.get(routes.size() - 1))) {
                    nodeToSend = m;
                }
            }
            if (nodeToSend != null) {
                if (AgentCompartment.log.isInfoEnabled()) {
                    AgentCompartment.log.info("Forwarding search reply to: "
                            + nodeToSend);
                }
                routes.remove(routes.size() - 1);
                // trace diffused for forwarding
                final List<IProperty<?>> properties = new ArrayList<IProperty<?>>();
                properties.add(new Property<List<String>>(routes));
                properties.add(new Property<Long>(gradient - 1));
                // response!
                this.diffuseSearchReply(new SearchReply(properties, molecules),
                        nodeToSend);
            }
        }
    }

    /**
     * 
     * @param compartmentID
     * @param similarity
     */
    public void setPreferences(final String compartmentID,
            final double similarity) {
        if (!this.preferences.containsKey(compartmentID)) {
            final List<Double> args = new ArrayList<Double>();
            args.add(similarity);
            this.preferences.put(compartmentID, args);
        } else {
            final List<Double> args = this.preferences.get(compartmentID);
            if (!args.contains(similarity)) {
                args.add(similarity);
                this.preferences.remove(compartmentID);
                this.preferences.put(compartmentID, args);
            }
        }
    }

    // TODO Magic numbers are ugly
    private void updateHit(final String compartmentID) {
        final List<IProperty<?>> p = this.routingInfoCache.get(compartmentID);
        final long hit = (Long) p.get(2).getValue() + 1;
        p.set(2, new Property<Long>(hit));
    }

    /**
     * 
     */
    public void verifyTimeStampAndHit() {
        for (final String s : this.routingInfoCache.keySet()) {
            List<IProperty<?>> p = this.routingInfoCache.get(s);
            final List<String> list = (List<String>) p.get(0).getValue();
            final long current = System.currentTimeMillis();
            final long timeStamp = (Long) p.get(1).getValue();
            final long hit = (Long) p.get(2).getValue();
            final long delta = current - timeStamp;
            if (delta > AgentCompartment.MAX_TIME_STAMP_AGE
                    && hit < AgentCompartment.MIN_CACHE_HITS) {
                if (AgentCompartment.log.isDebugEnabled()) {
                    AgentCompartment.log.debug("Removed compartment: " + s
                            + " from routing cache");
                }
                this.removeRoutingInfo(s);
            } else {
                p = new ArrayList<IProperty<?>>();
                p.add(new Property<List<String>>(list));
                p.add(new Property<Long>(System.currentTimeMillis()));
                p.add(new Property<Long>(0L));
                if (AgentCompartment.log.isDebugEnabled()) {
                    AgentCompartment.log.debug("Added compartment: " + s
                            + " to routing cache");
                }
                this.routingInfoCache.put(s, p);
            }
        }
    }

    /**
     * Metodo che ci dà il permesso di aggiornare o meno in maniera
     * probabilistica
     *
     * Usato in UPDATE_MODE_PROBABILISTIC per aggiornamenti all'interno del
     * compartment a seconda della probabilità Usato in UPDATE_MODE_RANDOM per
     * invio random della richiesta ai vicini
     *
     * Può essere modificato in qualcosa di più sofisticato
     *
     * @return true se il compartimento può aggiornare false se il compartimento
     *         non può aggiornare
     *
     */
    private static boolean shouldUpdate() {
        if (new Random().nextInt() % 100 >= AgentCompartment.PROBABILITY) {
            return true;
        }
        return false;
    }

    /**
     *
     * Metodo che cerca all'interno del compartimento se ci sono molecole simili
     * a quelle passate in ingresso grazie a qualche algoritmo e le aggiorna
     *
     * Method left intentionally blank
     *
     * @param molecule
     */
    private void checkUpdatesDue(final IMolecule molecule) {
        //
    }

}
