package interfaces;

import interfaces.model.IMolecule;
import interfaces.model.ITrace;
import interfaces.peer.components.ILocalCompartment;
import java.util.List;

/**
 * Extension to {@link ILocalCompartment} useful for developing new high-level
 * primitives. The new methods defined in this interfaces are taken from the
 * essay by the same authors, on APICe {@link http
 * ://apice.unibo.it/xwiki/bin/view/Courses/Sd1415ProjectsCommunication}.
 * 
 * @author Luca Santonastasi (luca.santonastasi@studio.unibo.it)
 * @author Matteo Fattori (matteo.fattori5@studio.unibo.it)
 *
 */
public interface IAgentCompartment extends ILocalCompartment {

    /**
     * Broadcast mode for {@link IAgentCompartment#searchRequest} primitive.
     */
    public final static int MODE_BROADCAST = 990;

    /**
     * Focused mode for {@link IAgentCompartment#searchRequest} primitive.
     */
    public final static int MODE_FOCUSED = 991;

    /**
     *
     * @param routingList
     *            -> [FORSE?] potrebbe essere più facile diffondere le tracce
     *            con le membrane
     * @param trace
     *            -> simulates action execution side-effects
     * @param compartmentName
     *            -> name of the compartment
     * @param fmok
     *            -> semantic similarity function between molecules
     * @param concentration
     *            -> concentration of the trace
     * @param gradient
     *            --> gradient --> See TOTA Middleware [Mamei and Zambonelli]
     */
    public void diffuseTrace(List<String> routingList, ITrace trace,
            String compartmentName, double fmok, long concentration,
            long gradient);

    /**
     *
     * @param uniqueID
     *            -> represents a specific update identifier. Useful to avoid
     *            flooding or duplicate updates
     * @param molecules
     *            -> list of molecules that have to be updated
     * @param concentration
     *            -> TODO Probably to remove since molecules already have their
     *            own concentration
     * @param mode
     *            -> may be MODE_BROADCAST or MODE_FOCUSED
     * @param gradient
     *            -> gradient --> See TOTA Middleware [Mamei and Zambonelli]
     */
    public void diffuseUpdates(String uniqueID, List<IMolecule> molecules,
            long concentration, int mode, long gradient);

    /**
     *
     * @param routingList
     *            -> list composed by every node that connects a sender to a
     *            specific receiver. This is useful for the direct reply.
     * @param compartmentName
     *            -> name of the compartment
     * @param fmok
     *            -> semantic similarity function between molecules
     * @param concentration
     *            -> molecules eventually found should have at least this
     *            concentration
     * @param gradient
     *            -> gradient --> See TOTA Middleware [Mamei and Zambonelli]
     * @param nHop
     *            -> it's the opposite of {@code gradient}. While
     *            {@code gradient} decreases, {@code nHop} increases. Useful for
     *            the reply in which this number will become the
     *            {@code gradient}.
     * @param mode
     *            -> may be MODE_BROADCAST or MODE_FOCUSED
     */
    public void searchRequest(List<String> routingList, String compartmentName,
            double fmok, long concentration, long gradient, long nHop, int mode);

    /**
     *
     * @param routingList
     *            -> list composed by every node that connects a sender to a
     *            specific receiver
     * @param compartmentName
     *            -> name of the compartment
     * @param fmok
     *            -> semantic similarity function between molecules
     * @param molecules
     *            -> represents the list of molecules found following a
     *            {@link IAgentCompartment#searchRequest}.
     * @param gradient
     *            -> gradient --> See TOTA Middleware [Mamei and Zambonelli]
     */
    public void searchReply(List<String> routingList, String compartmentName,
            double fmok, List<IMolecule> molecules, long gradient);

    /**
     * Returns all the molecules as a single String
     * 
     * @return the String representing all the molecules
     */
    List<IMolecule> getMolecules();

    /**
     * Adds a molecule
     * 
     * @param mol
     *            the molecule to add
     */
    void addMolecule(final IMolecule mol);

    /**
     * Gets the routing paths known by this compartment -- that is, lists of
     * hops
     * 
     * @return the list of routes, each of which represents the routing path
     *         toward a target compartment
     */
    List<List<String>> getRoutes();

}
