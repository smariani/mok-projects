# README

This is the repository of the projects related to the Molecules of Knowledge model, developed by students of the course in Distributed Systems 14/15 of the University of Bologna.

Home page is <http://apice.unibo.it/xwiki/bin/view/MoK/Projects>